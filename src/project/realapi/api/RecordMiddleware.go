package api

import (
    "fmt"
    "github.com/gocraft/web"
    "project/realapi/data/user"
    "project/realapi/share"
)

func (self *RootContext) RecordMiddleware(w web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
    defer func() {
        var request user.Request
        if self.userInfo != nil {
            request.UserId = self.userInfo.Id
        }
        request.Url = r.RequestURI
        request.Data = string(self.requestBody)
        request.Response = fmt.Sprintf("%v",self.okPacket)
        share.MonitorDB.Create(&request)
    }()

    next(w, r)
}
