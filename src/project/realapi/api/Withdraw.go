package api

import (
    "github.com/gocraft/web"
    "project/realapi/data/user"
    "project/realapi/runtime"
    "tantanbei.com/json"
)

const BASEURL = "http://static.hyrainbow.com/activity/dist/money.html?sessionkey="

func (self *Api) GetWithdrawPage(rw web.ResponseWriter, req *web.Request) {
    var data user.SessionAppid
    err := json.Unmarshal(self.requestBody, &data)
    if err != nil {
        runtime.CLog.Fatal("requestWithdrawPage", "unmarshal err %v", err)
    }

    self.okPacket.Code = 0
    self.okPacket.Data = map[string]interface{}{
        "url": BASEURL + data.Sessionkey + "&appKey=" + self.app.Ak + "&appSecret=" + self.app.As,
    }
}
