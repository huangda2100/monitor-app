package api

import (
    "fmt"
    "github.com/gocraft/web"
    "github.com/jinzhu/gorm"
    "io/ioutil"
    "project/realapi/data"
    "project/realapi/runtime"
    "project/realapi/service"
    "project/realapi/share"
    "strings"
    "tantanbei.com/json"
    "time"
)

func(self *Apn) CallbackReport(rw web.ResponseWriter, req *web.Request) {
    body, err := ioutil.ReadAll(req.Body)
    if err != nil {
        self.okPacket.Code = 0
        self.okPacket.Message = "读书数据失败"
        return
    }

    var appReq data.AppRequest
    err = json.Unmarshal(body, &appReq)
    if err != nil {
        self.okPacket.Code = 0
        self.okPacket.Message = "解析错误"
        return
    }

    realIp := appReq.Ip
    if appReq.Ip == "" {
        realIp = service.FromRequest(req)
    }
    //记录report info
    var callbackInfo data.CallbackInfo
    callbackInfo.Imei = appReq.Imei
    callbackInfo.Mac = appReq.Mac
    callbackInfo.Oaid = appReq.Oaid
    callbackInfo.Androidid = appReq.Androidid
    callbackInfo.Ip = realIp
    callbackInfo.Ua = appReq.Ua
    callbackInfo.From = appReq.From
    callbackInfo.EventType = appReq.EventType
    callbackInfo.EventTime = appReq.EventTime
    callbackInfo.PurchaseAmount = appReq.PurchaseAmount
    if err := share.MonitorDB.Save(&callbackInfo).Error; err != nil {
        runtime.CLog.Warn("CallbackReport", "save callbackinfo err: %v", err)
        self.okPacket.Code = 0
        self.okPacket.Message = "服务器内部错误"
        return
    }

    var clickInfo data.ClickInfo
    sqlStr := "select * from `click_infos` where"
    oneDayAgo := time.Now().AddDate(0,0, -1)
    fixedStr := fmt.Sprintf(" and is_called = 0 and created_at > '%s' order by id desc limit 1", oneDayAgo.String())
    if appReq.Imei != "" {
        sqlCond := ""
        sqlCond += " imei = '" + fmt.Sprintf("%s'", service.MD5(appReq.Imei)) + fixedStr

        queryStr := sqlStr + sqlCond
        if err := share.MonitorDB.Raw(queryStr).Scan(&clickInfo).Error; err != nil && err != gorm.ErrRecordNotFound{
            runtime.CLog.Warn("callbackReport", "db error %v, querystr is: %v", err, queryStr)
            self.okPacket.Code = 0
            self.okPacket.Message = "服务器内部错误"
            return
        }
    }

    if clickInfo.Id == 0 {
        if appReq.Mac != "" {
            sqlCond := ""
            mac := strings.ToUpper(strings.Replace(appReq.Mac,":","", -1))
            sqlCond += " mac = '" + fmt.Sprintf("%s'", service.MD5(mac))

            queryStr := sqlStr + sqlCond
            if err := share.MonitorDB.Raw(queryStr).Scan(&clickInfo).Error; err != nil && err != gorm.ErrRecordNotFound{
                runtime.CLog.Warn("callbackReport", "db error %v, querystr is: %v", err, queryStr)
                self.okPacket.Code = 0
                self.okPacket.Message = "服务器内部错误"
                return
            }
        }
    }

    if clickInfo.Id == 0 {
        if appReq.Oaid != "" {
            sqlCond := ""
            sqlCond += " oaid = '" + fmt.Sprintf("%s'", appReq.Oaid)

            queryStr := sqlStr + sqlCond
            if err := share.MonitorDB.Raw(queryStr).Scan(&clickInfo).Error; err != nil && err != gorm.ErrRecordNotFound{
                runtime.CLog.Warn("callbackReport", "db error %v, querystr is: %v", err, queryStr)
                self.okPacket.Code = 0
                self.okPacket.Message = "服务器内部错误"
                return
            }
        }
    }

    if clickInfo.Id == 0 {
        if appReq.Androidid != "" {
            sqlCond := ""
            sqlCond += " androidid = '" + fmt.Sprintf("%s'", service.MD5(appReq.Androidid))

            queryStr := sqlStr + sqlCond
            if err := share.MonitorDB.Raw(queryStr).Scan(&clickInfo).Error; err != nil && err != gorm.ErrRecordNotFound{
                runtime.CLog.Warn("callbackReport", "db error %v, querystr is: %v", err, queryStr)
                self.okPacket.Code = 0
                self.okPacket.Message = "服务器内部错误"
                return
            }
        }
    }


    if clickInfo.Id == 0 {
        runtime.CLog.Warn("callbackReport", "no record match: %v", appReq)
        self.okPacket.Code = 0
        self.okPacket.Message = "无点击数据匹配，无法上报"
        return
    }

    var status bool

    if clickInfo.Source == data.SOURCE_TOUTIAO {
        status = service.ReportToutiao(clickInfo.Callback, &appReq)
    } else if clickInfo.Source == data.SOURCE_KUAISHOU {
        status = service.ReportKuaishou(clickInfo.Callback, &appReq)
    }

    clickInfo.IsCalled = 1
    if status {
        clickInfo.CalledResult = 1
    } else {
        clickInfo.CalledResult = 2
    }

    if err := share.MonitorDB.Save(&clickInfo).Error; err != nil {
        runtime.CLog.Warn("CallbackReport", "save clickinfo err: %v", err)
        self.okPacket.Code = 0
        self.okPacket.Message = "服务器内部错误"
        return
    }

    self.okPacket.Code = 1
    self.okPacket.Message = "上报成功"
}
