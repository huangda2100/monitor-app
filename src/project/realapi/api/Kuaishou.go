package api

import (
    "github.com/gocraft/web"
    "project/realapi/data"
    "project/realapi/runtime"
    "project/realapi/service"
    "project/realapi/share"
)

func (self *Apn) MonitorKSIos(rw web.ResponseWriter, req *web.Request) {
    var iosStruct data.ClickInfo
    var err error

    iosStruct, err = service.MakeClickInfoStruct(data.SOURCE_KUAISHOU, iosStruct, req)
    if err != nil {
        runtime.CLog.Warn("kuai shou ios", "struct error %v", err)
        self.okPacket.Code = 0
        self.okPacket.Message = "服务器内部错误"
        return
    }

    if &iosStruct == nil {
        runtime.CLog.Warn("kuai shou ios", "iosStruct nil")
        self.okPacket.Code = 0
        self.okPacket.Message = "无参数"
        return
    }

    if err := share.MonitorDB.Create(&iosStruct).Error; err != nil {
        runtime.CLog.Warn("kuai shou ios", "db error %v", err)
        self.okPacket.Code = 0
        self.okPacket.Message = "服务器内部错误"
        return
    }

    self.okPacket.Code = 1
    self.okPacket.Message = "上报成功"
}

func (self *Apn) MonitorKSAndroid(rw web.ResponseWriter, req *web.Request) {
    var androidStruct data.ClickInfo
    var err error

    androidStruct, err = service.MakeClickInfoStruct(data.SOURCE_KUAISHOU, androidStruct, req)
    if err != nil {
        runtime.CLog.Warn("kuai shou android", "struct error %v", err)
        self.okPacket.Code = 0
        self.okPacket.Message = "服务器内部错误"
        return
    }

    if &androidStruct == nil {
        runtime.CLog.Warn("kuai shou ios", "androidStruct nil")
        self.okPacket.Code = 0
        self.okPacket.Message = "无参数"
        return
    }

    if err := share.MonitorDB.Create(&androidStruct).Error; err != nil {
        runtime.CLog.Warn("kuai shou android", "db error %v", err)
        self.okPacket.Code = 0
        self.okPacket.Message = "服务器内部错误"
        return
    }

    self.okPacket.Code = 1
    self.okPacket.Message = "上报成功"
}
