package api

import (
    "github.com/gocraft/web"
    "io/ioutil"
    "project/realapi/data/user"
    "project/realapi/runtime"
    "project/realapi/share"
    "tantanbei.com/json"
)

func (self *Api) ApiSessionMiddleware(w web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
    runtime.CLog.Debug("session error", " in session middleware url:", r.URL, ", r.Method:", r.Method, ", r.Header:", r.Header)
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        runtime.CLog.Warn("session error", "read request error", err.Error())
        FailWithErrCode(self)
        return
    }

    defer r.Body.Close()
    self.requestBody = body
    var sessionKeyStruct user.SessionKey
    err = json.Unmarshal(body, &sessionKeyStruct)

    if err != nil {
        runtime.CLog.Warn("session error", "Unmarshal error", err.Error())
        FailWithErrCode(self)
        return
    }

    value, err := share.Redis.GetInt64(sessionKeyStruct.Sessionkey)
    if err != nil {
        runtime.CLog.Warn("session error", "get string error", err.Error())
        FailWithErrCode(self)
        return
    }

    var userData user.User
    if err := share.MonitorDB.Where("id=?", value).First(&userData).Error; err != nil {
        runtime.CLog.Warn("session error", "user doesn't find")
        FailWithErrCode(self)
        return
    }

    self.userInfo = &userData

    if self.userInfo.Id == 0 {
        runtime.CLog.Warn("session error", "user doesn't exist")
        FailWithErrCode(self)
        return
    }

    next(w, r)
}
