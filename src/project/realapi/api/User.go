package api

import (
    "encoding/json"
    "github.com/gocraft/web"
    "project/realapi/data/attribute"
    "project/realapi/data/conf"
    "project/realapi/data/user"
    "project/realapi/runtime"
    "project/realapi/service"
)

func (self *Api) BindGuest(rw web.ResponseWriter, req *web.Request) {
    var userBind user.UserBind
    var userWechat user.Wechat

    var userMap = make(map[string]interface{})
    err := json.Unmarshal(self.requestBody, &userMap)
    if err != nil {
        runtime.CLog.Fatal(conf.TAG_USER, "BindGuest Unmarshal error", err.Error())
        self.okPacket.Code = -1
        self.okPacket.Message = "解析错误"
        return
    }

    userBind.Sessionkey = userMap["sessionkey"].(string)
    userBind.Overwrite = false

    err = json.Unmarshal([]byte(userMap["wechat"].(string)), &userWechat)
    if err != nil {
        runtime.CLog.Fatal(conf.TAG_USER, "BindGuest Unmarshal error", err.Error())
        self.okPacket.Code = -1
        self.okPacket.Message = "解析错误"
        return
    }

    userBind.Wechat = userWechat

    if self.userInfo.Openid != "" && !userBind.Overwrite {
        runtime.CLog.Debug(conf.TAG_USER,"BindGuest user already bind", "")
        self.okPacket.Code = -1
        self.okPacket.Message = "该用户已绑定"
        return
    }

    userInfo, err := service.BindGuest(&userBind.Wechat, self.userInfo)
    if err != nil {
        runtime.CLog.Fatal(conf.TAG_USER,"BindGuest bind error", err.Error())
        self.okPacket.Code = -1
        self.okPacket.Message = "绑定错误"
        return
    }

    newSessionKey, err := service.GenerateSessionKey(userInfo, false)
    if err != nil {
        runtime.CLog.Fatal(conf.TAG_USER,"BindGuest generate sessionkey error", err.Error())

        self.okPacket.Code = -1
        self.okPacket.Message = "获取新的session错误"
        return
    }

    self.okPacket.Code = 0
    self.okPacket.Data = map[string]interface{}{
        "sessionkey": newSessionKey,
    }
}

func (self *Api) GetUserInfoAndWithdrawConfig(rw web.ResponseWriter, req *web.Request) {
    runtime.CLog.Debug(conf.TAG_USER,"GetUserInfoAndWithdrawConfig withdraw config body:", string(self.requestBody))

    attrMap := service.GetUserAttribute(self.userInfo.Id)
    withdrawConfig := service.GetWithdrawConfig()
    isWithdraw := service.IsWithdraw(self.userInfo.Id)

    self.okPacket.Code = 0
    self.okPacket.Data = map[string]interface{}{
        "uid":             self.userInfo.Id,
        "username":        self.userInfo.Nickname,
        "name":            self.userInfo.Nickname,
        "coin":            attrMap[attribute.ATTRIBUTE_KEY_COIN],
        "last_login_time": self.userInfo.LastLoginTime,
        "withdraw_config": withdrawConfig,
        "isWithdraw"     : isWithdraw,
    }
}
