package api

import (
    "fmt"
    "github.com/gocraft/web"
    "io/ioutil"
    "project/realapi/data"
    "project/realapi/service"
    "project/realapi/share"
    "sort"
    "strconv"
    "tantanbei.com/json"
    "time"
)

func verify(body []byte, r *web.Request) ([]byte, *data.App, string, bool) {
    var reqData map[string]interface{}
    err := json.Unmarshal(body, &reqData)
    if err != nil {
        return nil, nil, "解码错误", false
    }

    ak := reqData["ak"].(string)
    sign := reqData["sign"].(string)

    var appData data.App
    share.MonitorDB.Where("ak = ?", ak).First(&appData)
    if appData.As == "" {
        return nil, nil, "不存在解密字符串", false
    }

    reqData["as"] = appData.As
    delete(reqData, "ak")
    delete(reqData, "sign")

    lst := []string{}
    for k,_ := range reqData {
        lst = append(lst,k)
    }

    signStr := ""
    sort.Strings(lst)
    for _,v := range lst {
        signStr += fmt.Sprintf("%s=%v&", v, reqData[v])
    }

    signStr = signStr[0:len(signStr) - 1]
    verifySign := service.MD5(signStr)

    // 验证过期时间
    timestamp := time.Now().Unix() + 3600
    tsInt, _ := strconv.ParseInt(reqData["ts"].(string), 10, 64)
    if tsInt > timestamp {
        return nil, nil, "请求过期", false
    }

    // 验证签名
    if sign == "" || sign != verifySign {
        errStr := fmt.Sprintf("签名数据错误,编码str: %s,接受到sign:%s, 服务端编码的sign:%s",signStr,sign,verifySign)
        return nil, nil, errStr, false
    }

    return body, &appData, "", true
}

func (self *Api) AuthFunc(w web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
    body, appData, msg, isAuth := verify(self.requestBody, r)
    if !isAuth {
        self.okPacket.Code = -1
        self.okPacket.Message = msg
        return
    }

    self.requestBody = body
    self.app = appData
    next(w, r)
}

func (self *Auth) AuthFunc(w web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
    rbody, err := ioutil.ReadAll(r.Body)
    if err != nil {
        self.okPacket.Code = -1
        self.okPacket.Message = "读取错误"
        return
    }
    body, appData, msg, isAuth := verify(rbody,r)
    if !isAuth {
        self.okPacket.Code = -1
        self.okPacket.Message = msg
        return
    }
    self.requestBody = body
    self.app = appData
    next(w, r)
}

