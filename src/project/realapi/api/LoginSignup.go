package api

import (
    "encoding/json"
    "github.com/gocraft/web"
    "project/realapi/data/conf"
    "project/realapi/data/user"
    "project/realapi/runtime"
    "project/realapi/service"
)

type SignInUp struct {
    Phone       string `json:"phone"`
    UserName    string `json:"user_name"`
    DisplayName string `json:"display_name"`
    Password    string `json:"password"`
    AccType     int    `json:"acc_type"` //0普通用户 1代理商 2管理员 3 代理子账户  4 充值管理员 5 审核管理员 6子管理账号
    Email       string `json:"email"`
}

//0:phone/password error
func (self *Auth) Login(rw web.ResponseWriter, req *web.Request) {
    var passport user.PassportStruct
    err := json.Unmarshal(self.requestBody, &passport)

    userInfo := service.GetUserInfoByPassport(passport.Passport)

    //更新用户最后登陆时间
    err = service.UpdateUserLastLoginTime(userInfo)
    if err != nil {
        runtime.CLog.Fatal(conf.TAG_LOGIN,"Login update userinfo error", err.Error())
        Fail(rw)
        return
    }

    isGuest := false
    if userInfo.Openid != "" {
        isGuest = true
    }

    sessionKey, err := service.GenerateSessionKey(userInfo, isGuest)

    if err != nil {
        self.okPacket.Code = -1
        self.okPacket.Message = err.Error()
        return
    }

    self.okPacket.Code = 0
    self.okPacket.Data = map[string]string{"sessionkey": sessionKey}
}

/**
 * 生成passport并记录用户基础信息
 */
func (self *Auth) Register(rw web.ResponseWriter, req *web.Request) {
    var registerInfo user.RegisterRequest

    err := json.Unmarshal(self.requestBody, &registerInfo)
    if err != nil {
        runtime.CLog.Warn(conf.TAG_LOGIN,"Register unmarshal error", err.Error())
        Fail(rw)
        return
    }

    if registerInfo.RegisterIp == "" {
        realIp := service.FromRequest(req)
        registerInfo.RegisterIp = realIp
    }
    registerInfo.Guest = true
    var key string
    if registerInfo.Guest {
        //游客游客登陆
        key, err = service.GuestRegister(&registerInfo)
    } else {
        //微信登陆
        key, err = service.WechatRegister(&registerInfo)
    }

    if err != nil {
        runtime.CLog.Warn(conf.TAG_LOGIN,"Register create error", err.Error())
        Fail(rw)
        return
    }

    self.okPacket.Code = 0
    self.okPacket.Data = map[string]interface{}{
        "passport": key,
    }
}
