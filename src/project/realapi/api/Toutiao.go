package api

import (
    "github.com/gocraft/web"
    "project/realapi/data"
    "project/realapi/runtime"
    "project/realapi/service"
    "project/realapi/share"
)

func (self *Apn) MonitorTTIos(rw web.ResponseWriter, req *web.Request) {
    var iosStruct data.ClickInfo
    var err error

    iosStruct, err = service.MakeClickInfoStruct(data.SOURCE_TOUTIAO, iosStruct, req)
    if err != nil {
        runtime.CLog.Warn("tou tiao ios", "struct error %v", err)
        self.okPacket.Code = 0
        self.okPacket.Message = "服务器内部错误"
        return
    }

    if &iosStruct == nil {
        runtime.CLog.Warn("tou tiao ios", "iosStruct nil")
        self.okPacket.Code = 0
        self.okPacket.Message = "无参数"
        return
    }

    share.MonitorDB.Create(&iosStruct)
    if share.MonitorDB.Error != nil {
        runtime.CLog.Warn("tou tiao ios", "db error %v", share.MonitorDB.Error)
        self.okPacket.Code = 0
        self.okPacket.Message = "服务器内部错误"
        return
    }

    self.okPacket.Code = 1
    self.okPacket.Message = "上报成功"
}

func (self *Apn) MonitorTTAndroid(rw web.ResponseWriter, req *web.Request) {
    var androidStruct data.ClickInfo
    var err error

    androidStruct, err = service.MakeClickInfoStruct(data.SOURCE_TOUTIAO, androidStruct, req)
    if err != nil {
        runtime.CLog.Warn("tou tiao android", "struct error %v", err)
        self.okPacket.Code = 0
        self.okPacket.Message = "服务器内部错误"
        return
    }

    if &androidStruct == nil {
        runtime.CLog.Warn("tou tiao ios", "androidStruct nil")
        self.okPacket.Code = 0
        self.okPacket.Message = "无参数"
        return
    }

    if err := share.MonitorDB.Create(&androidStruct).Error; err != nil {
        runtime.CLog.Fatal("tou tiao android", "db error %v", err)
        self.okPacket.Code = 0
        self.okPacket.Message = "服务器内部错误"
        return
    }

    self.okPacket.Code = 1
    self.okPacket.Message = "上报成功"
}
