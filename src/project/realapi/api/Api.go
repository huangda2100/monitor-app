package api

import (
    "project/realapi/data"
    "project/realapi/data/user"
    "project/realapi/packet"
)

type RootContext struct {
    okPacket *packet.OkPacket
    requestBody []byte
    app *data.App
    userInfo *user.User
    isFirstLoginToday bool
    //panic err message
    //DO NOT set it message manually
    err interface{}
}

type Apn struct {
    *RootContext
}

type Web struct {
    *RootContext
}

type Api struct {
    *RootContext

    UserId uint32
}

type Auth struct {
    *RootContext
}
