package api

import (
	"github.com/gocraft/web"
	"tantanbei.com/xjson"
	"project/realapi/packet"
)

func (self *RootContext) PrePackMiddleware(w web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	//TODO: Use this function to send response automatically
	defer func() {
		if self.err == nil {
			w.Write(xjson.Encode(self.okPacket))
		}
	}()

	self.okPacket = &packet.OkPacket{}
	next(w, r)
}
