package api

import "github.com/gocraft/web"

func (self *RootContext) Acao(w web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	next(w, r)
}
