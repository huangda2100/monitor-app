package api

import (
    "github.com/gocraft/web"
    "github.com/jinzhu/gorm"
    "project/realapi/data/attribute"
    "project/realapi/data/conf"
    "project/realapi/data/withdraw"
    "project/realapi/runtime"
    "project/realapi/service"
    "project/realapi/share"
    "tantanbei.com/json"
    "time"
)

func (self *Api) GetCoin(rw web.ResponseWriter, req *web.Request) {
    var coinAttr attribute.Attribute
    share.MonitorDB.Where("user_id=? AND type=?", self.userInfo.Id, attribute.ATTRIBUTE_TYPE_COIN).First(&coinAttr)

    if share.MonitorDB.Error != nil {
        runtime.CLog.Fatal(conf.TAG_CONFIG, "GetCoin db error", share.MonitorDB.Error)
        self.okPacket.Code = -1
        self.okPacket.Message = "获取金币错误"
        return
    }

    self.okPacket.Code = 0
    self.okPacket.Data = map[string]interface{}{
        "coin": coinAttr.Amount,
    }
}


func (self *Api) SaveCoin(rw web.ResponseWriter, req *web.Request) {
    var coinSave attribute.CoinSave
    err := json.Unmarshal(self.requestBody, &coinSave)

    if err != nil {
        runtime.CLog.Fatal(conf.TAG_CONFIG, "SaveCoin unmarshal error", err.Error())
        self.okPacket.Code = -1
        self.okPacket.Message = "解析错误"
        return
    }

    operateType, value, err := service.GetChangedValue(coinSave.Coinchanged)

    var coinAttr attribute.Attribute
    var coinLog attribute.AttributeLog
    share.MonitorDB.Where("user_id=? AND type=?", self.userInfo.Id, attribute.ATTRIBUTE_TYPE_COIN).First(&coinAttr)

    if share.MonitorDB.Error != nil {
        runtime.CLog.Fatal(conf.TAG_CONFIG, "SaveCoin db error", share.MonitorDB.Error)

        self.okPacket.Code = -1
        self.okPacket.Message = "获取金币错误"
        return
    }

    tx := share.MonitorDB.Begin()
    if coinAttr.ID <= 0 {
        coinAttr.Type = attribute.ATTRIBUTE_TYPE_COIN
        coinAttr.UserId = self.userInfo.Id
        coinAttr.Amount = value
        coinAttr.Appid = self.userInfo.Appid
        tx.Create(&coinAttr)
    } else {
        switch operateType {
        case service.OPERATE_TYPE_PLUS:
            coinAttr.Amount += value
        case service.OPERATE_TYPE_MINUS:
            coinAttr.Amount -= value
            if coinAttr.Amount < 0 {
                coinAttr.Amount = 0
            }
        }
        tx.Save(&coinAttr)
    }

    if tx.Error != nil {
        tx.Rollback()
        runtime.CLog.Fatal(conf.TAG_CONFIG, "SaveCoin update attribute error", tx.Error)
        self.okPacket.Code = -1
        self.okPacket.Message = "更新错误"
        return
    }

    coinLog.UserId = self.userInfo.Id
    coinLog.Type = attribute.ATTRIBUTE_TYPE_COIN
    coinLog.Ip = service.FromRequest(req)
    coinLog.Data = coinSave.Coinchanged

    tx.Create(&coinLog)
    if tx.Error != nil {
        tx.Rollback()
        runtime.CLog.Fatal(conf.TAG_CONFIG, "SaveCoin update attribute log error", tx.Error)
        self.okPacket.Code = -1
        self.okPacket.Message = "更新日志错误"
        return
    }

    tx.Commit()

    ip := service.RealIP(req)
    var attributeIncrease = map[string]int32{
        "coin_increase":   int32(value),
        "water_increase":  0,
        "sprite_increase": 0,
    }
    service.WriteNoticeLog(self.userInfo, req.Method, ip, 0, attributeIncrease)

    self.okPacket.Code = 0
    self.okPacket.Data = map[string]interface{}{
        "coinchanged": coinSave.Coinchanged,
        "coin":        coinAttr.Amount,
    }

}

/**
 * 用户提现
 */
func (self *Api) WithdrawMoney(rw web.ResponseWriter, req *web.Request) {
    runtime.CLog.Debug(conf.TAG_USER,"WithdrawMoney withdraw body:", string(self.requestBody))

    var withdrawRequestFromH5 withdraw.WithdrawFromH5
    err := json.Unmarshal(self.requestBody, &withdrawRequestFromH5)
    if err != nil {
        runtime.CLog.Warn(conf.TAG_USER,"WithdrawMoney unmarshal error", err.Error())
        self.okPacket.Code = -1
        self.okPacket.Message = "读取参数失败"
        return
    }

    currentDate := time.Now().Format("2006-01-02")
    withdrawCount := 0

    if self.userInfo.Openid == "" {
        runtime.CLog.Warn(conf.TAG_USER,"don't bind wechat, can't withdraw")
        self.okPacket.Code = -1
        self.okPacket.Message = "尚未绑定微信，无法提现"
        return
    }

    share.MonitorDB.Model(&withdraw.WithdrawLog{}).Where("openid=? and date=? and status=?", self.userInfo.Openid, currentDate, 1).Count(&withdrawCount)
    if withdrawCount >= 1 {
        self.okPacket.Code = -1
        self.okPacket.Message = "今日的提现机会已用完，请于明日提现"
        return
    }

    if err := share.MonitorDB.Model(&withdraw.WithdrawLog{}).Where("openid=? and amount=? and status=?", self.userInfo.Openid, 3000, 1).Count(&withdrawCount).Error; err != nil && err != gorm.ErrRecordNotFound {
        self.okPacket.Code = -1
        self.okPacket.Message = "获取提现次数失败"
        return
    }
    if withdrawCount >= 1 {
        self.okPacket.Code = -1
        self.okPacket.Message = "该微信号已经使用过新用户提现机会"
        return
    }

    var attr attribute.Attribute
    if err := share.MonitorDB.Where("user_id=? and type=?", self.userInfo.Id, attribute.ATTRIBUTE_TYPE_COIN).First(&attr).Error; err != nil {
        runtime.CLog.Warn(conf.TAG_USER,"invalid amount: %f, can withdraw amount: %f", err)
        self.okPacket.Code = -1
        self.okPacket.Message = "用户不存在金币"
        return
    }
    canWithdrawAmount := float32(attr.Amount) / float32(service.RATIO)
    if withdrawRequestFromH5.Amount > canWithdrawAmount {
        runtime.CLog.Warn(conf.TAG_USER,"invalid amount: %f, can withdraw amount: %f", withdrawRequestFromH5.Amount, canWithdrawAmount)
        self.okPacket.Code = -1
        self.okPacket.Message = "提现金额大于金币余额"
        return
    }

    //减掉用户账户内的提现金币
    attr.Amount -= int(withdrawRequestFromH5.Amount * float32(service.RATIO))
    if err := share.MonitorDB.Save(&attr).Error; err != nil {
        runtime.CLog.Warn(conf.TAG_USER,"save db error: %v", err)
        self.okPacket.Code = -1
        self.okPacket.Message = "用户保存失败"
        return
    }

    if self.userInfo.Openid == "" {
        runtime.CLog.Warn(conf.TAG_USER,"don't bind wechat 2, can't withdraw")
        self.okPacket.Code = -1
        self.okPacket.Message = "尚未绑定微信，无法提现"
        return
    }

    withdrawRequestFromH5.Openid = self.userInfo.Openid
    withdrawRequestFromH5.Uid = self.userInfo.Id
    ip := service.FromRequest(req)
    result1, result2, isSuccess := service.WithdrawMoney(&withdrawRequestFromH5, ip, self.app)

    var withdrawLog withdraw.WithdrawLog
    withdrawLog.UserId = self.userInfo.Id
    withdrawLog.Openid = self.userInfo.Openid
    withdrawLog.Status = withdraw.WITHDRAW_SUCCESS
    withdrawLog.Date = currentDate
    withdrawLog.Amount = int(withdrawRequestFromH5.Amount * float32(service.RATIO))
    withdrawLog.WechatResp = result1

    if !isSuccess {
        runtime.CLog.Warn(conf.TAG_USER,"WithdrawMoney withdraw error", result1, result2)
        self.okPacket.Code = -1
        self.okPacket.Message = "提现失败"

        //补偿账户金币
        attr.Amount += int(withdrawRequestFromH5.Amount * float32(service.RATIO))
        if err := share.MonitorDB.Save(&attr).Error; err != nil {
            runtime.CLog.Warn(conf.TAG_USER,"attr db error: %v", err)
            self.okPacket.Message = "提现失败，请联系客服补偿金币"
        }

        withdrawLog.Status = withdraw.WITHDRAW_FAIL
        if err := share.MonitorDB.Save(&withdrawLog).Error; err != nil {
            runtime.CLog.Warn(conf.TAG_USER,"compensate db error: %v", err)
        }

        return
    }

    if err := share.MonitorDB.Save(&withdrawLog).Error; err != nil {
        runtime.CLog.Warn(conf.TAG_USER,"withdrawLog db error: %v", err)
    }

    self.okPacket.Code = 0
    self.okPacket.Message = "提现成功"
}


