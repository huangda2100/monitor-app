package api

import (
    runtime2 "project/realapi/runtime"
    "runtime"

    "github.com/gocraft/web"
    "tantanbei.com/xjson"
)

func (self *RootContext) HandlePanicMiddleware(w web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	defer func() {
		if err := recover(); err != nil {
			logPanicMessage(r, err)

			self.err = err
			self.okPacket.Code = -2
			self.okPacket.Message = "panic"
			Success(w)
			w.Write(xjson.Encode(self.okPacket))
		}
	}()

	next(w, r)
}

func logPanicMessage(r *web.Request, err interface{}) {
	const size = 4096
	stack := make([]byte, size)
	stack = stack[:runtime.Stack(stack, false)]

	data := map[string]interface{}{
		"Error":  err,
		"Stack":  string(stack),
		"Params": r.URL.Query(),
		"Method": r.Method,
	}

	runtime2.CLog.Fatal("panic_middleware", "HandlePanicMiddleware err message:%v", data)
}
