package api

import (
    "github.com/gocraft/web"
    "github.com/jinzhu/gorm"
    "project/realapi/data"
    "project/realapi/data/conf"
    "project/realapi/runtime"
    "project/realapi/share"
    "tantanbei.com/json"
)

func(self *Api) GetAdslotInfo(rw web.ResponseWriter, req *web.Request) {
    var adslotReq data.AdslotReq
    err := json.Unmarshal(self.requestBody, &adslotReq)
    if err != nil {
        runtime.CLog.Warn(conf.TAG_LOGIN,"Register unmarshal error", err.Error())
        Fail(rw)
        return
    }

    var queryInfo data.AdslotConf
    if err := share.MonitorDB.Where("appid = ? and channel_id = ?", adslotReq.Appid, adslotReq.ChannelId).Preload("Adslots").First(&queryInfo).Error; err != nil && err != gorm.ErrRecordNotFound{
        runtime.CLog.Warn(conf.TAG_ADSLOT, "query error %v", err)
        self.okPacket.Code = -1
        self.okPacket.Message = "查询失败"
    }

    var adslotInfo []string
    for _, v := range queryInfo.Adslots {
        adslotInfo = append(adslotInfo, v.Adslotid)
    }

    self.okPacket.Code = 0
    self.okPacket.Data = adslotInfo
}
