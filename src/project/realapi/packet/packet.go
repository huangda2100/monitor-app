package packet

type OkPacket struct {
    Code    int32       `json:"error_code"`
    Data    interface{} `json:"data,omitempty"`
    Message string      `json:"error_msg,omitempty"`
}
