package service

import (
    "crypto/md5"
    "encoding/hex"
    "errors"
    "fmt"
    "io/ioutil"
    "net/http"
    "project/realapi/data/conf"
    "project/realapi/data/user"
    "project/realapi/runtime"
    "project/realapi/share"
    "strconv"
    "time"

    "github.com/satori/go.uuid"
    "tantanbei.com/json"
)

func GuestRegister(registerInfo *user.RegisterRequest) (string, error) {
    var storeUser user.User
    storeUser.Passport = GeneratePassport()
    storeUser.LastLoginTime = time.Now()
    storeUser.Phone = registerInfo.Tel
    storeUser.QQ = registerInfo.QQ
    storeUser.RegisterIp = registerInfo.RegisterIp
    storeUser.Appid = registerInfo.Appid
    storeUser.Imei = registerInfo.Imei
    storeUser.Mac = registerInfo.Mac
    storeUser.Oaid = registerInfo.Oaid
    storeUser.Androidid = registerInfo.Androidid

    if err := share.MonitorDB.Create(&storeUser).Error; err != nil {
        return "", err
    }

    return storeUser.Passport, nil
    //return GenerateSessionKey(&storeUser, true)
}

func GetWechatResponse(requestUrl string) ([]byte, error) {
    resp, err := http.Get(requestUrl)
    if err != nil {
        return nil, err
    }

    defer resp.Body.Close()
    return ioutil.ReadAll(resp.Body)
}

func WechatRegister(registerInfo *user.RegisterRequest) (string, error) {
    resp, err := GetWechatResponse(registerInfo.Wechat)
    if err != nil {
        return "", err
    }

    var wechatResponse user.WechatResponse
    err = json.Unmarshal(resp, &wechatResponse)
    if err != nil {
        return "", err
    }

    var storeUser user.User
    storeUser.Passport = GeneratePassport()
    storeUser.Openid = wechatResponse.Credential.RawData.Openid
    storeUser.Unionid = wechatResponse.Credential.RawData.Unionid
    storeUser.Nickname = wechatResponse.Nickname
    storeUser.Gender = wechatResponse.Gender
    storeUser.Icon = wechatResponse.RawData.Headimgurl
    storeUser.City = wechatResponse.RawData.City
    storeUser.Province = wechatResponse.RawData.Province
    storeUser.Country = wechatResponse.RawData.Country
    storeUser.RegisterIp = registerInfo.RegisterIp
    storeUser.Appid = registerInfo.Appid

    if err := share.MonitorDB.Create(&storeUser).Error; err != nil {
        return "", err
    }

    return storeUser.Passport, nil
    //return GenerateSessionKey(&storeUser, false)
}

//生成passport
func GeneratePassport() string {
    uid, err := uuid.NewV4()
    if err != nil {
        return ""
    }

    return uid.String()
}

//生成session key
func GenerateSessionKey(userInfo *user.User, isGuest bool) (string, error) {
    key := ""
    if userInfo.Id != 0 {
        if isGuest {
            key = "guest:userId:" + fmt.Sprintf("%d:%d", userInfo.Id, time.Now().Unix())
        } else {
            key = "user:userId" + fmt.Sprintf("%d:%d", userInfo.Id, time.Now().Unix())
        }
    } else {
        runtime.CLog.Debug(conf.TAG_USER, "GenerateSessionKey", userInfo)
        return "", errors.New("用户id不存在")
    }

    md5Key := GetMd5(key)
    value := strconv.Itoa(userInfo.Id)
    err := share.Redis.Set(md5Key, []byte(value), 86400*2)
    if err != nil {
        return "", err
    }

    return md5Key, nil
}

func GetUserInfoByPassport(passport string) *user.User {
    var userInfo user.User
    share.MonitorDB.Where("passport=?", passport).First(&userInfo)

    return &userInfo
}

func UpdateUserLastLoginTime(userInfo *user.User) error {
    userInfo.LastLoginTime = time.Now()
    err := share.MonitorDB.Save(&userInfo).Error

    return err
}

func BindGuest(wechat *user.Wechat, userInfo *user.User) (*user.User, error) {
    userInfo.Openid = wechat.Openid
    userInfo.Unionid = wechat.Unionid
    userInfo.Nickname = wechat.Nickname
    userInfo.Gender = wechat.Sex
    userInfo.Icon = wechat.Headimgurl
    userInfo.City = wechat.City
    userInfo.Province = wechat.Province
    userInfo.Country = wechat.Country
    userInfo.UserTags = wechat.UserTags
    privilege, _ := json.Marshal(wechat.Privilege)
    userInfo.Privilege = string(privilege)

    err := share.MonitorDB.Save(&userInfo).Error

    return userInfo, err
}

var Grade = []float32{0.3, 1, 10, 30, 50, 100}

const RATIO = 10000

func GetWithdrawConfig() map[string]interface{} {
    config := map[string]interface{}{
        "grade": Grade,
        "ratio": RATIO,
    }

    return config
}

func GetMd5(str string) string {
    h := md5.New()
    h.Write([]byte(str))
    return hex.EncodeToString(h.Sum(nil))
}
