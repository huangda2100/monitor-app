package service

import (
    "crypto/md5"
    "encoding/hex"
    "errors"
    "fmt"
    "github.com/gocraft/web"
    "net/url"
    "sort"
    "strconv"
    "time"
)

const APPSIGNEXPIRE = 10

var ApiAuthConfig = map[string]string{
    //appkey---appsecret
    "123": "321",
}

var AppSecret string
var ok bool

func VerifySign(req *web.Request) error {
    ak := req.URL.Query().Get("ak")
    sign := req.URL.Query().Get("sign")
    ts := req.URL.Query().Get("ts")

    // 验证来源
    AppSecret, ok = ApiAuthConfig[ak]
    if !ok {
        return errors.New("ak Error")
    }

    // 验证过期时间
    timestamp := time.Now().Unix()
    tsInt, _ := strconv.ParseInt(ts, 10, 64)
    if tsInt > timestamp || timestamp-tsInt >= APPSIGNEXPIRE {
        return errors.New("ts Error")
    }

    // 验证签名
    if sign == "" || sign != createSign(req.Form) {
        return errors.New("sign Error")
    }

    return nil
}

func MD5(str string) string {
    s := md5.New()
    s.Write([]byte(str))
    return hex.EncodeToString(s.Sum(nil))
}

func createSign(params url.Values) string {
    // 自定义 MD5 组合
    return MD5(AppSecret + createEncryptStr(params) + AppSecret)
}

func createEncryptStr(params url.Values) string {
    var key []string
    var str = ""
    for k := range params {
        if k != "sn" {
            key = append(key, k)
        }
    }
    sort.Strings(key)
    for i := 0; i < len(key); i++ {
        if i == 0 {
            str = fmt.Sprintf("%v=%v", key[i], params.Get(key[i]))
        } else {
            str = str + fmt.Sprintf("&%v=%v", key[i], params.Get(key[i]))
        }
    }
    return str
}
