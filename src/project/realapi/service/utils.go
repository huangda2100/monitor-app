package service

import (
    "bytes"
    "crypto/tls"
    "encoding/pem"
    "fmt"
    "golang.org/x/crypto/pkcs12"
    "io/ioutil"
    "log"
    "math/rand"
    "net/http"
    "project/realapi/data"
    "project/realapi/runtime"
    "sort"
    "strconv"
    "strings"
    "time"
    "unsafe"
)

const (
    WECHAT_WITHDRAW_API = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers"
    MCH_APPID           = "wx95e1e8e49da0597e"
    MCHID               = "1567709141"
    KEY                 = "pa13YmNV27SEGvcyS0rl8PZPflGjQwrm"
)

//ca证书的位置
var (
    wechatCertPath = "cert/apiclient_cert.p12"
)

func Pkcs12ToPem(p12 []byte, password string) tls.Certificate {
    blocks, err := pkcs12.ToPEM(p12, password)

    // 从恐慌恢复
    defer func() {
        if x := recover(); x != nil {
            log.Print(x)
        }
    }()

    if err != nil {
        panic(err)
    }

    var pemData []byte
    for _, b := range blocks {
        pemData = append(pemData, pem.EncodeToMemory(b)...)
    }

    cert, err := tls.X509KeyPair(pemData, pemData)
    if err != nil {
        panic(err)
    }
    return cert
}

// 设置证书
func SetCertData(certPath string) []byte {
    certData, err := ioutil.ReadFile(certPath)
    if err != nil {
        runtime.CLog.Fatal("SetCertData", "read cert fail")
        return nil
    }

    return certData
}

//携带ca证书的安全请求
func SecurePost(url string, xmlContent string, app *data.App) (*http.Response, error) {
    certData := SetCertData(wechatCertPath)
    certificate := Pkcs12ToPem(certData, app.Mchid)

    config := &tls.Config{
        Certificates: []tls.Certificate{certificate},
    }
    transport := &http.Transport{
        TLSClientConfig:    config,
        DisableCompression: true,
    }
    client := &http.Client{Transport: transport}
    return client.Post(
        url,
        "application/xml; charset=utf-8",
        strings.NewReader(xmlContent))
}

func GetWithdrawNum(uid int) string {
    //hyfruit + uid + unixnano
    return "hyfruit" + fmt.Sprintf("%d%d", uid, time.Now().UnixNano())
}

const letterBytes = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
    letterIdxBits = 6                    // 6 bits to represent a letter index
    letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
    letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var src = rand.NewSource(time.Now().UnixNano())

func GetNonceStr(n int) string {
    b := make([]byte, n)
    // A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
    for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
        if remain == 0 {
            cache, remain = src.Int63(), letterIdxMax
        }
        if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
            b[i] = letterBytes[idx]
            i--
        }
        cache >>= letterIdxBits
        remain--
    }

    return *(*string)(unsafe.Pointer(&b))
}

//md5签名
func Md5WithdrawOrder(order map[string]interface{}, app *data.App) string {
    keys := make([]string, 0)
    for k := range order {
        keys = append(keys, k)
    }

    sort.Strings(keys)
    var buf bytes.Buffer
    for _, key := range keys {
        switch order[key].(type) {
        case string:
            buf.WriteString(key)
            buf.WriteString(`=`)
            buf.WriteString(order[key].(string))
            buf.WriteString(`&`)
        case int:
            str := strconv.Itoa(order[key].(int))
            buf.WriteString(key)
            buf.WriteString(`=`)
            buf.WriteString(str)
            buf.WriteString(`&`)
        }
    }

    buf.WriteString(`key=`)
    buf.WriteString(app.ApiKey)
    str := GetMd5(buf.String())

    return strings.ToUpper(str)
}

func MapToXml(params map[string]interface{}) string {
    var buf bytes.Buffer
    buf.WriteString(`<xml>`)
    for k, v := range params {
        buf.WriteString(`<`)
        buf.WriteString(k)
        buf.WriteString(`>`)

        switch v.(type) {
        case string:
            buf.WriteString(v.(string))
        case int:
            str := strconv.Itoa(v.(int))
            buf.WriteString(str)
        }

        buf.WriteString(`</`)
        buf.WriteString(k)
        buf.WriteString(`>`)
    }
    buf.WriteString(`</xml>`)

    return buf.String()
}
