package service

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "net/http"
    "net/url"
    "project/realapi/data"
    "project/realapi/runtime"
)

const REPORTURL_TOUTIAO = "https://ad.oceanengine.com/track/activate/"

//头条、快手事件对应map
var toutiaoMap = map[int]int{
    1:0,//激活
    2:1,//注册
    3:2,//付费
}

var kuaishouMap = map[int]int{
    1:1,  //激活
    2:2,  //注册
    3:3,  //付费
}

func ReportToutiao(callback string, appReq *data.AppRequest) bool {
    //构造请求url
    //req, err := http.NewRequest(http.MethodGet, callback, nil)
    req, err := http.NewRequest(http.MethodGet, REPORTURL_TOUTIAO, nil)
    if err != nil {
       runtime.CLog.Debug("ReportToutiao", "new request err: %v", err)
       return false
    }

    parsedUrl, err := url.Parse(callback)
    if err != nil {
        panic(err)
    }
    queryStr, _ := url.ParseQuery(parsedUrl.RawQuery)
    realCallback := queryStr["callback"][0]
    q := req.URL.Query()
    q.Add("callback", realCallback)
    q.Add("imei", appReq.Imei)
    eventType, ok := toutiaoMap[appReq.EventType]
    if ok {
       q.Add("event_type", fmt.Sprintf("%d", eventType))
    } else  {
       runtime.CLog.Debug("ReportToutiao", "get eventType from map err")
       return false
    }

    q.Add("conv_time", fmt.Sprintf("%d", appReq.EventTime))
    req.URL.RawQuery = q.Encode()

    //上报头条
    client := http.Client{}
    runtime.CLog.Warn("ReportToutiao", "new request url: %v", req)
    resp, err := client.Do(req)

    if err != nil {
        runtime.CLog.Warn("ReportToutiao", "new request err: %v", err)
        return false
    }

    type ToutiaoResp struct {
        Code int    `json:"code"`
        Ret  int    `json:"ret"`
        Msg  string `json:"msg"`
    }

    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        runtime.CLog.Warn("ReportToutiao", "new request err: %v", err)
        return false
    }

    var respStruct ToutiaoResp
    err = json.Unmarshal(body, &respStruct)
    runtime.CLog.Warn("ReportToutiao", "new request resp: %v", respStruct)
    if err != nil {
        runtime.CLog.Warn("ReportToutiao", "new request err: %v", err)
        return false
    }

    if respStruct.Code != 0 {
        runtime.CLog.Warn("ReportToutiao", "response err: %s", respStruct.Msg)
        return false
    }

    return true
}

func ReportKuaishou(callback string, appReq *data.AppRequest) bool {
    //构造请求url
    var needPurchaseAmountEventType = []int{3, 12, 13, 14, 15}
    var params string

    eventType, ok := kuaishouMap[appReq.EventType]
    if ok {
        params = "&event_type=" + fmt.Sprintf("%d", eventType)
    } else  {
        runtime.CLog.Debug("ReportKuaishou", "get eventType from map err")
        return false
    }

    for _, v := range needPurchaseAmountEventType {
        if appReq.EventType == v {
            eventAmount := float32(appReq.PurchaseAmount) / 100
            params += "&purchase_amount=" + fmt.Sprintf("%f", eventAmount)
            break
        }
    }

    params += "&event_time=" + fmt.Sprintf("%d", appReq.EventTime)

    parseUrl, err := url.Parse(callback)
    if err != nil {
        runtime.CLog.Debug("ReportKuaishou", "new request err: %v", err)
        return false
    }

    reqUrl := parseUrl.Scheme + "://" + parseUrl.Host + parseUrl.Path + parseUrl.RawQuery + params

    //上报快手
    resp, err := http.Get(reqUrl)
    if err != nil {
        runtime.CLog.Debug("ReportKuaishou", "new request err: %v", err)
        return false
    }

    defer resp.Body.Close()

    type KuaishouResp struct {
        Result int `json:"result"`
    }

    var respStruct KuaishouResp

    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        runtime.CLog.Debug("ReportKuaishou", "new request err: %v", err)
        return false
    }

    err = json.Unmarshal(body, &respStruct)
    if err != nil {
        runtime.CLog.Debug("ReportKuaishou", "new request err: %v", err)
        return false
    }

    if respStruct.Result != 1 {
        runtime.CLog.Debug("ReportKuaishou", "new request err: %v", err)
        return false
    }

    return true
}
