package service

import (
    "encoding/base64"
    "project/realapi/data/attribute"
    "project/realapi/data/conf"
    "project/realapi/data/user"
    "project/realapi/runtime"
    "project/realapi/share"
    "time"

    "bitbucket.org/tantanbei/protocol/fruitlog"
    "github.com/golang/protobuf/proto"
)

func WriteNoticeLog(user *user.User, apiName string, ip string, withdrawAmount float32, attributeIncrease map[string]int32) {
    var fruitLog fruitlog.FruitLog

    var attributeInfo attribute.Attribute
    share.MonitorDB.Where("user_id=? and type=?", user.Id, attribute.ATTRIBUTE_TYPE_COIN).First(&attributeInfo)

    fruitLog.UserId = int32(user.Id)
    fruitLog.Ip = ip
    fruitLog.ApiName = apiName
    fruitLog.CoinIncrease = attributeIncrease["coin_increase"]
    fruitLog.WaterIncrease = attributeIncrease["water_increase"]
    fruitLog.SpriteIncrease = attributeIncrease["sprite_increase"]
    fruitLog.CurrentCoin = int32(attributeInfo.Amount)
    fruitLog.WithdrawAmount = withdrawAmount
    fruitLog.Timestamp = int32(time.Now().Unix())
    fruitLog.Openid = user.Openid
    fruitLog.Unionid = user.Unionid

    var body []byte
    body, err := proto.Marshal(&fruitLog)
    if err != nil {
        return
    }
    logStr := base64.StdEncoding.EncodeToString(body)
    runtime.CLog.Notice(conf.TAG_SENSETIVE, logStr)
    return
}
