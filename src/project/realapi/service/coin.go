package service

import (
    "errors"
    "project/realapi/data/attribute"
    "project/realapi/share"
    "strconv"
)


const OPERATE_TYPE_PLUS = 1
const OPERATE_TYPE_MINUS = 2

var operateMap = map[string]int{
    "+": OPERATE_TYPE_PLUS,
    "-": OPERATE_TYPE_MINUS,
}

func GetChangedValue(raw string) (int, int, error) {
    rawType := raw[:1]
    value := raw[1:]

    amount, _ := strconv.Atoi(value)
    operateType, ok := operateMap[rawType]

    if !ok {
        return 0, 0, errors.New("没有相应操作方式 ")
    }

    return operateType, amount, nil
}

func GetUserAttribute(userId int) map[string]int {
    var attrs []attribute.Attribute
    share.MonitorDB.Where("user_id=?", userId).Find(&attrs)

    attrMap := make(map[string]int, 0)
    for _, val := range attrs {
        switch val.Type {
        case attribute.ATTRIBUTE_TYPE_WATER:
            attrMap[attribute.ATTRIBUTE_KEY_WATER] = val.Amount
        case attribute.ATTRIBUTE_TYPE_COIN:
            attrMap[attribute.ATTRIBUTE_KEY_COIN] = val.Amount
        case attribute.ATTRIBUTE_TYPE_SPIRIT:
            attrMap[attribute.ATTRIBUTE_KEY_SPRITE] = val.Amount
        }
    }

    return attrMap
}
