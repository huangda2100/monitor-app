package service

import (
    "encoding/xml"
    "io/ioutil"
    "project/realapi/data"
    "project/realapi/data/conf"
    "project/realapi/data/withdraw"
    "project/realapi/runtime"
    "project/realapi/share"
)

//付款订单
type WithdrawOrder struct {
    XMLName        xml.Name `xml:"xml"`
    MchAppid       string   `xml:"mch_appid"`
    Mchid          string   `xml:"mchid"`
    DeviceInfo     string   `xml:"device_info"`
    NonceStr       string   `xml:"nonce_str"`
    Sign           string   `xml:"sign"`
    PartnerTradeNo string   `xml:"partner_trade_no"`
    Openid         string   `xml:"openid"`
    CheckName      string   `xml:"check_name"`
    Amount         int      `xml:"amount"`
    Desc           string   `xml:"desc"`
    SpbillCreateIp string   `xml:"spbill_create_ip"`
}

//付款订单结果
type WithdrawResult struct {
    ReturnCode     string `xml:"return_code"`
    ReturnMsg      string `xml:"return_msg"`
    ResultCode     string `xml:"result_code"`
    ErrCodeDes     string `xml:"err_code_des"`
    PaymentNo      string `xml:"payment_no"`
    PartnerTradeNo string `xml:"partner_trade_no"`
}

//付款，成功返回自定义订单号，微信订单号，true，失败返回错误信息，false
func WithdrawMoney(req *withdraw.WithdrawFromH5, ip string, app *data.App) (string, string, bool) {
    orderStruct := make(map[string]interface{})

    orderStruct["mch_appid"] = app.MchAppid
    orderStruct["mchid"] = app.Mchid
    orderStruct["device_info"] = "WEB"
    orderStruct["nonce_str"] = GetNonceStr(32)
    orderStruct["partner_trade_no"] = GetWithdrawNum(req.Uid)
    orderStruct["openid"] = req.Openid
    orderStruct["amount"] = int(req.Amount * 100)
    orderStruct["check_name"] = "NO_CHECK" //NO_CHECK：不校验真实姓名 FORCE_CHECK：强校验真实姓名
    orderStruct["desc"] = app.PrizeDesc
    orderStruct["spbill_create_ip"] = ip
    orderStruct["sign"] = Md5WithdrawOrder(orderStruct, app)

    xmlBody := MapToXml(orderStruct)
    resp, err := SecurePost(WECHAT_WITHDRAW_API, xmlBody, app)
    if err != nil {
        return err.Error(), "", false
    }
    defer resp.Body.Close()

    bodyByte, _ := ioutil.ReadAll(resp.Body)

    var res WithdrawResult
    xmlErr := xml.Unmarshal(bodyByte, &res)

    if xmlErr != nil {
        return xmlErr.Error(), "", false
    }

    if res.ReturnCode == "SUCCESS" && res.ResultCode == "SUCCESS" {
        return res.PartnerTradeNo, res.PaymentNo, true
    }

    runtime.CLog.Warn(conf.TAG_USER, "original withdraw err: %v, the request is %v: ", res, orderStruct)
    return res.ReturnMsg, res.ErrCodeDes, false
}

func IsWithdraw(userId int) bool {
    var count = 0
    share.MonitorDB.Model(&withdraw.WithdrawLog{}).Where("user_id = ? and status = ?", userId, withdraw.WITHDRAW_SUCCESS).Count(&count)

    if count > 0 {
        return true
    } else {
        return false
    }
}
