package service

import (
    "github.com/gocraft/web"
    "project/realapi/data"
    "strconv"
)

func MakeClickInfoStruct(source int, cStruct data.ClickInfo, req *web.Request) (data.ClickInfo, error) {
    params := req.URL.Query()
    if len(params) == 0 {
        return cStruct, nil
    }
    adid := req.URL.Query().Get("adid")
    cStruct.Adid = adid
    cid := req.URL.Query().Get("cid")
    cStruct.Cid = cid
    imei := req.URL.Query().Get("imei")
    cStruct.Imei = imei
    ip := req.URL.Query().Get("ip")
    cStruct.Ip = ip
    mac := req.URL.Query().Get("mac")
    cStruct.Mac = mac
    oaid := req.URL.Query().Get("oaid")
    cStruct.Oaid = oaid
    androidid := req.URL.Query().Get("androidid")
    cStruct.Androidid = androidid
    idfa := req.URL.Query().Get("idfa")
    cStruct.Idfa = idfa
    os := req.URL.Query().Get("os")
    cStruct.Os = os
    ua := req.URL.Query().Get("ua")
    cStruct.Ua = ua
    csite := req.URL.Query().Get("csite")
    cStruct.Csite = csite

    ctypeRaw := req.URL.Query().Get("ctype")
    if ctypeRaw == "" {
        cStruct.Ctype = 0
    } else {
        ctype, _ := strconv.Atoi(ctypeRaw)
        cStruct.Ctype = ctype
    }

    tsRaw := req.URL.Query().Get("ts")
    if tsRaw == "" {
        cStruct.Ts = 0
    } else {
        ts, _ := strconv.Atoi(tsRaw)
        cStruct.Ts = ts
    }

    cStruct.Agent = req.URL.Query().Get("agent")
    cStruct.Callback = req.URL.Query().Get("callback_url")
    cStruct.Source = source
    cStruct.Appid = req.URL.Query().Get("appid")
    cStruct.AidName = req.URL.Query().Get("aid_name")
    cStruct.CidName = req.URL.Query().Get("cid_name")
    cStruct.CampaignId = req.URL.Query().Get("campaign_id")
    cStruct.CampaignName = req.URL.Query().Get("campaign_name")
    cStruct.ConvertId = req.URL.Query().Get("convert_id")
    cStruct.Geo = req.URL.Query().Get("geo")
    cStruct.RequestId = req.URL.Query().Get("request_id")
    cStruct.Sl = req.URL.Query().Get("sl")
    cStruct.CallbackParam = req.URL.Query().Get("callback_param")

    return cStruct, nil
}
