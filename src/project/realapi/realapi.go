package main

import (
    "bitbucket.org/hyrainbow/clog"
    "flag"
    "fmt"
    "github.com/gocraft/web"
    "gopkg.in/gcfg.v1"
    "log"
    "net/http"
    "project/realapi/api"
    "project/realapi/data"
    "project/realapi/runtime"
    "project/realapi/share"
    "time"
)

var GlobalContext data.GlobalContext

func init() {
    configFile := flag.String("c", "conf/prod.conf", "config file path")

    if err := gcfg.ReadFileInto(&GlobalContext, *configFile); err != nil {
        log.Fatalf("Failed to read global conf, err[%s]", err.Error())
        return
    }

    share.InitDB(&GlobalContext)
    share.InitRedis(&GlobalContext)

    clogConf := &clog.Conf{
        Name:    GlobalContext.Log.Name,
        LogPath: GlobalContext.Log.LogPath,
        BufSize: map[clog.LogLevel]int{
            clog.NoticeLevel:  GlobalContext.Log.BufSize,
            clog.FatalLevel:   GlobalContext.Log.BufSize,
            clog.WarningLevel: GlobalContext.Log.BufSize,
            clog.DebugLevel:   GlobalContext.Log.BufSize},
        MaxEnableLevel: clog.LogLevel(GlobalContext.Log.MaxEnableLevel),
        NeedEmail:      GlobalContext.Log.NeedEmail,
        EmailConf: &clog.EmailConf{
            Server:    GlobalContext.Email.Server,
            Sender:    GlobalContext.Email.Sender,
            Password:  GlobalContext.Email.Password,
            Receivers: GlobalContext.Email.Receivers,
            Subject:   GlobalContext.Email.Subject,
            HostName:  "",
        },
    }

    var err error
    runtime.CLog, err = clog.NewClog(clogConf)
    if err != nil {
        fmt.Println(err)
    }
}

func main() {

    rootRouter := web.New(api.RootContext{}).
        Middleware((*api.RootContext).PrePackMiddleware).
        Middleware((*api.RootContext).HandlePanicMiddleware).
        Middleware(web.StaticMiddleware("template", web.StaticOption{Prefix: "/template/"})).
        Middleware((*api.RootContext).Acao).
        Middleware((*api.RootContext).RecordMiddleware)
    //Middleware(web.LoggerMiddleware).
    //Middleware(web.ShowErrorsMiddleware).

    //***********
    //**apn******
    //***********
    apnRouter := rootRouter.Subrouter(api.Apn{}, "")
    //add route
    apnRouter.Get("/tt/monitorIos", (*api.Apn).MonitorTTIos)
    apnRouter.Get("/tt/monitorAndroid", (*api.Apn).MonitorTTAndroid)
    apnRouter.Get("/ks/monitorIos", (*api.Apn).MonitorKSIos)
    apnRouter.Get("/ks/monitorAndroid", (*api.Apn).MonitorKSAndroid)
    apnRouter.Post("/report", (*api.Apn).CallbackReport)

    authRouter := rootRouter.Subrouter(api.Auth{},"")
    authRouter.Middleware((*api.Auth).AuthFunc)
    authRouter.Post("/login", (*api.Auth).Login)
    authRouter.Post("/register", (*api.Auth).Register)

    //***********
    //**api******
    //***********
    apiRouter := rootRouter.Subrouter(api.Api{}, "/api")
    apiRouter.Middleware((*api.Api).ApiSessionMiddleware)
    apiRouter.Middleware((*api.Api).AuthFunc)
    apiRouter.Post("/savecoin", (*api.Api).SaveCoin)
    apiRouter.Post("/readcoin", (*api.Api).GetCoin)
    apiRouter.Post("/bindwechat", (*api.Api).BindGuest)
    apiRouter.Post("/withdrawpage", (*api.Api).GetWithdrawPage)
    apiRouter.Post("/withdraw", (*api.Api).WithdrawMoney)
    apiRouter.Post("/basicinfo", (*api.Api).GetUserInfoAndWithdrawConfig)
    apiRouter.Post("/adslotinfo", (*api.Api).GetAdslotInfo)

    ch := make(chan error, 1)

    go func() {
        //http1 server
        ch <- http.ListenAndServe(GlobalContext.Server.Port, rootRouter)
    }()

    err := <-ch
    runtime.CLog.Debug("realapi err:", err.Error())

    <-time.After(10 * time.Second)
}
