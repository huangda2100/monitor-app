package share

import (
    "fmt"
    "project/realapi/data"
    "tantanbei.com/redis"
    "time"
)

var Redis *redis.Redis
var err error

func InitRedis(context *data.GlobalContext)  {
    Redis, err = redis.NewRedis(
       context.Redis.Address,
       context.Redis.Password,
       context.Redis.BufferCapacity,
       context.Redis.ConnTimeout*time.Second,
       context.Redis.WriteTimeout*time.Second,
       context.Redis.ReadTimeout*time.Second)
    if err != nil {
       panic(fmt.Sprint("create redis error:", err))
    }
}
