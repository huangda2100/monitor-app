package share

import (
    _ "github.com/go-sql-driver/mysql"
    "github.com/jinzhu/gorm"
    "project/realapi/data"
)

var MonitorDB *gorm.DB

func InitDB(context *data.GlobalContext){
    var err error

    MonitorDB, err = gorm.Open("mysql", context.DB.Address)
    if err != nil {
        panic(err)
    }
    MonitorDB.DB().SetMaxOpenConns(context.DB.MaxConn)
}
