package user

import "time"

//注册接口数据
type RegisterRequest struct {
    Wechat     string `json:"wechat"`
    Tel        string `json:"tel"`
    QQ         string `json:"qq"`
    Guest      bool   `json:"guest"`
    Appid      string `json:"appid"`
    Imei       string `json:"imei"`
    Mac        string `json:"mac"`
    Oaid       string `json:"oaid"`
    Androidid  string `json:"androidid"`
    RegisterIp string `json:"register_ip"`
}

//微信回掉接口返回数据
type WechatResponse struct {
    Credential   Credential `json:"credential"`
    Gender       int        `json:"gender"`
    Icon         string     `json:"icon"`
    Nickname     string     `json:"nickname"`
    PlatformType int        `json:"platform_type"`
    RawData      RawData    `json:"raw_data"`
    Uid          string     `json:"uid"`
}

type Credential struct {
    Expired int64             `json:"expired"`
    RawData CredentialRawData `json:"raw_data"`
    Token   string            `json:"token"`
    Type    int               `json:"type"`
    Uid     string            `json:"uid"`
}

type RawData struct {
    City       string `json:"city"`
    Country    string `json:"country"`
    Headimgurl string `json:"headimgurl"`
    Language   string `json:"language"`
    Nickname   string `json:"nickname"`
    Openid     string `json:"openid"`
    Province   string `json:"province"`
    Sex        int    `json:"sex"`
    Unionid    string `json:"unionid"`
}

type CredentialRawData struct {
    AccessToken  string `json:"access_token"`
    ExpiresIn    int    `json:"expires_in"`
    Openid       string `json:"openid"`
    RefreshToken string `json:"refresh_token"`
    Scope        string `json:"scope"`
    Unionid      string `json:"unionid"`
}

type User struct {
    Id            int       `gorm:"column:id;primary_key;AUTO_INCREMENT"`
    Passport      string    `json:"passport"`
    Openid        string    `json:"openid"`
    Unionid       string    `json:"unionid"`
    Nickname      string    `json:"nickname"`
    Gender        int       `json:"gender"`
    Icon          string    `json:"icon"`
    City          string    `json:"city"`
    Province      string    `json:"province"`
    Country       string    `json:"country"`
    Language      string    `json:"language"`
    Phone         string    `json:"phone"`
    QQ            string    `json:"qq"`
    LastLoginTime time.Time `json:"last_login_time"`
    RegisterIp    string    `json:"register_ip"`
    UserTags      string    `json:"user_tags"`
    Privilege     string    `json:"privilege"`
    Num           int       `json:"num"`
    Level         int       `json:"level"`
    Appid         string    `json:"appid"`
    Imei          string    `json:"imei"`
    Mac           string    `json:"mac"`
    Oaid          string    `json:"oaid"`
    Androidid     string    `json:"androidid"`
    CreatedAt     time.Time
    UpdatedAt     time.Time
}

type SessionKey struct {
    Sessionkey string `json:"sessionkey"`
}

type SessionAppid struct {
    Sessionkey string `json:"sessionkey"`
    Appid      string `json:"appid"`
}

type PassportStruct struct {
    Passport string `json:"passport"`
}

type UserBind struct {
    Wechat     Wechat `json:"wechat"`
    Overwrite  bool   `json:"overwrite"`
    Sessionkey string `json:"sessionkey"`
}

type Wechat struct {
    Country    string `json:"country"`
    Unionid    string `json:"unionid"`
    Province   string `json:"province"`
    City       string `json:"city"`
    Openid     string `json:"openid"`
    Sex        int    `json:"sex"`
    Nickname   string `json:"nickname"`
    Headimgurl string `json:"headimgurl"`
    UserTags   string `json:"userTags"`
    Language   string `json:"language"`
    Privilege  string `json:"privilege"`
}

type VipReq struct {
    Num int `json:"num"`
}

type Request struct {
    Id        int    `gorm:"column:id;primary_key;AUTO_INCREMENT"`
    UserId    int    `json:"user_id"`
    Url       string `json:"url"`
    Data      string `json:"data"`
    Response  string `json:"response"`
    CreatedAt time.Time
    UpdatedAt time.Time
}

type OpenReport struct {
    Id        int    `gorm:"column:id;primary_key;AUTO_INCREMENT"`
    UserId    int    `json:"user_id"`
    Url       string `json:"url"`
    Data      string `json:"data"`
    CreatedAt time.Time
    UpdatedAt time.Time
}
