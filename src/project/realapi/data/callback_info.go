package data

import "time"

type CallbackInfo struct {
    Id             int    `gorm:"column:id;primary_key;AUTO_INCREMENT"`
    Imei           string `json:"imei"`
    Mac            string `json:"mac"`
    Oaid           string `json:"oaid"`
    Androidid      string `json:"androidid"`
    Ip             string `json:"ip"`
    Ua             string `json:"ua"`
    From           string `json:"from"`
    EventType      int    `json:"event_type"`
    EventTime      int    `json:"event_time"`
    PurchaseAmount int    `json:"purchase_amount"`
    CreatedAt      time.Time
    UpdatedAt      time.Time
}

type AppRequest struct {
    Imei           string `json:"imei,omitempty"`
    Mac            string `json:"mac,omitempty"`
    Oaid           string `json:"oaid,omitempty"`
    Androidid      string `json:"androidid,omitempty"`
    Ua             string `json:"ua,omitempty"`
    From           string `json:"from"`
    EventType      int    `json:"event_type,omitempty"`
    EventTime      int    `json:"event_time,omitempty"`
    PurchaseAmount int    `json:"purchase_amount,omitempty"`
    Passport       string `json:"passport"`
    Ip             string `json:"ip"`
}
