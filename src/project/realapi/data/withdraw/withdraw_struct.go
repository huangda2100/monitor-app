package withdraw

import "time"

type WithdrawFromH5 struct {
    Amount float32 `json:"amount"`
    Openid string  `json:"openid"`
    Uid    int     `json:"uid"`
    Ip     string  `json:"ip"`
}

type WithdrawLog struct {
    Id         int    `gorm:"column:id;primary_key;AUTO_INCREMENT"`
    UserId     int    `json:"user_id"`
    Openid     string `json:"openid"`
    Date       string `json:"date"`
    Status     int    `json:"status"`
    Amount     int    `json:"amount"`
    WechatResp string `json:"wechat_resp"`
    CreatedAt  time.Time
    UpdatedAt  time.Time
}

const WITHDRAW_SUCCESS = 1
const WITHDRAW_FAIL = 0
