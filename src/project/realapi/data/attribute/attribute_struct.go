package attribute

import "time"

const ATTRIBUTE_TYPE_WATER = 1
const ATTRIBUTE_TYPE_COIN = 2
const ATTRIBUTE_TYPE_SPIRIT = 3

const ATTRIBUTE_KEY_WATER = "water"
const ATTRIBUTE_KEY_COIN = "coin"
const ATTRIBUTE_KEY_SPRITE = "sprite"

type Attribute struct {
    ID        int    `gorm:"column:id;primary_key;AUTO_INCREMENT"`
    Type      int    `json:"type"`
    UserId    int    `json:"user_id"`
    Amount    int    `json:"amount"`
    Appid     string `json:"appid"`
    CreatedAt time.Time
    UpdatedAt time.Time
}

type AttributeLog struct {
    ID        int    `gorm:"column:id;primary_key;AUTO_INCREMENT"`
    Type      int    `json:"type"`
    UserId    int    `json:"user_id"`
    Ip        string `json:"ip"`
    Data      string `json:"data"`
    CreatedAt time.Time
    UpdatedAt time.Time
}

type CoinSave struct {
    Sessionkey  string `json:"sessionkey"`
    Coinchanged string `json:"coinchanged"`
    Verifycode  string `json:"verifycode"`
}
