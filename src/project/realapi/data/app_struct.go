package data

import "time"

type App struct {
    Id        int    `gorm:"column:id;primary_key;AUTO_INCREMENT"`
    Appid     string `json:"appid"`
    Ak        string `json:"ak"`
    As        string `json:"as"`
    MchAppid  string `json:"mch_appid"`
    Mchid     string `json:"mchid"`
    ApiKey    string `json:"api_key"`
    PrizeDesc string `json:"prize_desc"`
    CreatedAt time.Time
    UpdatedAt time.Time
}
