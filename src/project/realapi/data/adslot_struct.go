package data

import "time"

type AdslotReq struct {
    Appid     string `json:"appid"`
    ChannelId string `json:"channel_id"`
}

type AdslotConf struct {
    Id        int      `gorm:"column:id;primary_key;AUTO_INCREMENT"`
    Appid     string   `json:"appid"`
    ChannelId string   `json:"channel_id"`
    Adslots   []Adslot `gorm:"ForeignKey:ConfId"`
    CreatedAt time.Time
    UpdatedAt time.Time
}

type Adslot struct {
    Id        int    `gorm:"column:id;primary_key;AUTO_INCREMENT"`
    ConfId    int    `json:"conf_id"`
    Adslotid  string `json:"adslotid"`
    CreatedAt time.Time
    UpdatedAt time.Time
}
