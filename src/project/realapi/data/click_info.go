package data

import "time"

const SOURCE_TOUTIAO = 1
const SOURCE_KUAISHOU = 2

type ClickInfo struct {
    Id           int    `gorm:"column:id;primary_key;AUTO_INCREMENT"`
    Adid         string `json:"adid,omitempty"`
    Cid          string `json:"cid,omitempty"`
    Imei         string `json:"imei,omitempty"`
    Ip           string `json:"ip,omitempty"`
    Mac          string `json:"mac,omitempty"`
    Oaid         string `json:"oaid,omitempty"`
    Androidid    string `json:"androidid,omitempty"`
    Idfa         string `json:"idfa,omitempty"`
    Os           string `json:"os,omitempty"`
    Ua           string `json:"ua,omitempty"`
    Csite        string `json:"csite,omitempty"`
    Ctype        int    `json:"ctype,omitempty"`
    Ts           int    `json:"ts,omitempty"`
    Agent        string `json:"agent,omitempty"`
    Callback     string `json:"callback,omitempty"`
    IsCalled     int    `json:"is_called,omitempty"`
    CalledResult int    `json:"called_result,omitempty"`
    Source       int    `json:"source"`
    Appid        string `json:"appid"`
    AidName      string `json:"aid_name"`
    CidName      string `json:"cid_name"`
    CampaignId   string `json:"campaign_id"`
    CampaignName string `json:"campaign_name"`
    ConvertId    string `json:"convert_id"`
    Geo          string `json:"geo"`
    RequestId    string `json:"request_id"`
    Sl           string `json:"sl"`
    CallbackParam string `json:"callback_param"
`
    CreatedAt time.Time
    UpdatedAt time.Time
}
