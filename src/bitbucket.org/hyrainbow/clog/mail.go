package clog

import (
	"bytes"
	"fmt"
	"net/smtp"
	"strconv"
	"strings"
	"time"
)

type emailClient struct {
	Server    string
	Sender    string
	Password  string
	Receivers []string
	Subject   string

	HostName string // 本机hostname

	FatalMessage chan string
	//有些fatal，不频繁可以不报出来
	FatalMessage2 chan string
}

func newEmailClient(conf *EmailConf) (ec *emailClient, err error) {
	ec = &emailClient{}
	ec.Server = conf.Server
	ec.Sender = conf.Sender
	ec.Password = conf.Password
	ec.Receivers = conf.Receivers
	ec.Subject = conf.Subject
	ec.HostName = conf.HostName
	ec.FatalMessage = make(chan string)
	ec.FatalMessage2 = make(chan string)
	go ec.SendMailLoop()
	go ec.SendMailLoop2()
	return
}

func (ec *emailClient) SendMsgToChannel(msg string) {
	ec.FatalMessage <- msg
	return
}

func (ec *emailClient) SendMsgToChannel2(msg string) {
	ec.FatalMessage2 <- msg
}

func (ec *emailClient) SendMailLoop() {
	lasttime := time.Now()
	data := make([]string, 0)
	var tmpstr string
	var flag int // 用于select出口标记
	for {
		select {
		// 5分钟check下有没有需要发送的fatal日志
		case <-time.After(time.Minute * 5):
			flag = 1
		case tmpstr = <-ec.FatalMessage:
			flag = 2
		}
		if flag == 2 {
			data = append(data, tmpstr)
		}
		if len(data) > 99 {
			go ec.SendMail("FATAL_1: 5分钟内fatel日志超过100条\r\n" + strings.Join(data, "\r\n"))
			data = make([]string, 0)
			continue
		}
		nowtime := time.Now()
		if lasttime.Add(time.Minute * 5).Before(nowtime) {
			lasttime = nowtime
			if len(data) > 0 {
				go ec.SendMail("FATAL_1: 5分钟后存在fatel日志\r\n" + strings.Join(data, "\r\n"))
				data = make([]string, 0)
			}
		}
	}
}

func (ec *emailClient) SendMailLoop2() {
	lasttime := time.Now()
	data := make([]string, 0)
	var tmpstr string
	var flag int // 用于select出口标记
	for {
		select {
		// 5分钟check下有没有需要发送的fatal日志
		case <-time.After(time.Minute * 5):
			flag = 1
		case tmpstr = <-ec.FatalMessage2:
			flag = 2
		}
		if flag == 2 {
			data = append(data, tmpstr)
		}
		if len(data) > 2500 {
			go ec.SendMail("FATAL_2: 5分钟内fatel日志超过2500条\r\n总条数(" + strconv.Itoa(len(data)) + ")\r\n" + strings.Join(data[:50], "\r\n") +
				"...\r\n" + strings.Join(data[len(data)/2-25:len(data)/2+25], "\r\n") +
				"...\r\n" + strings.Join(data[len(data)-50:], "\r\n"))
			data = make([]string, 0)
			continue
		}
		nowtime := time.Now()
		if lasttime.Add(time.Minute * 5).Before(nowtime) {
			lasttime = nowtime
			if len(data) > 250 {
				go ec.SendMail("FATAL_2: 5分钟后fatel日志存在超过250条\r\n总条数(" + strconv.Itoa(len(data)) + ")\r\n" + strings.Join(data[:100], "\r\n") +
					"...\r\n" + strings.Join(data[len(data)-100:], "\r\n"))
			}
			data = make([]string, 0)
		}
	}
}

func (ec *emailClient) SendMail(content string) (err error) {
	var data bytes.Buffer
	fmt.Fprintf(&data, "To: %s\r\n", strings.Join(ec.Receivers, ","))
	fmt.Fprintf(&data, "From: %s\r\n", ec.Sender)
	fmt.Fprintf(&data, "Subject: %s\r\n", ec.Subject)
	fmt.Fprintf(&data, "Content-Type: text/plain; charset=UTF-8\r\n")
	fmt.Fprintf(&data, "\r\n%s", ec.HostName)
	fmt.Fprintf(&data, "\r\n%s", content)

	hp := strings.Split(ec.Server, ":")
	auth := smtp.PlainAuth("", ec.Sender, ec.Password, hp[0])

	err = smtp.SendMail(ec.Server, auth, ec.Sender, ec.Receivers, data.Bytes())
	fmt.Println("SendMail", err, ec)
	if err != nil {
		return
	}
	return
}
