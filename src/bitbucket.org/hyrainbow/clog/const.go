package clog

import (
	"path"
	"runtime"
	"strconv"
	"time"
)

type LogLevel uint32

const (
	NoticeLevel  LogLevel = 1
	FatalLevel   LogLevel = 2
	WarningLevel LogLevel = 3
	DebugLevel   LogLevel = 4
)

var LogLevelList = []LogLevel{NoticeLevel, FatalLevel, WarningLevel, DebugLevel}

func (ll LogLevel) GetLogFileSuffix() string {
	switch ll {
	case NoticeLevel:
		return ".log"
	case FatalLevel:
		return ".fatal"
	case WarningLevel:
		return ".warn"
	case DebugLevel:
		return ".dg"
	}
	return ".unknown"
}

func (ll LogLevel) GetLogPrefix() string {
	prefix := "UNKNOWN: "
	switch ll {
	case NoticeLevel:
		prefix = "NOTICE: "
	case FatalLevel:
		prefix = "FATAL: "
	case WarningLevel:
		prefix = "WARNING: "
	case DebugLevel:
		prefix = "DEBUG: "
	}

	if ll == NoticeLevel {
		prefix += " " + time.Now().Format("2006-01-02 15:04:05") + " * "

	} else {
		_, file, line, ok := runtime.Caller(3)
		if !ok {
			file = "???"
			line = 0
		} else {
			file = path.Base(file)
		}
		prefix += " " + time.Now().Format("2006-01-02 15:04:05") + " [" + file + ":" + strconv.Itoa(line) + "] * "
	}

	return prefix
}

const (
	DefaultBufSize int = 1
)
