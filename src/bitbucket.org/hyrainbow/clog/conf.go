package clog

type Conf struct {
	Name           string
	LogPath        string
	BufSize        map[LogLevel]int
	MaxEnableLevel LogLevel
	NeedEmail      bool
	EmailConf      *EmailConf
}

type EmailConf struct {
	Server    string
	Sender    string
	Password  string
	Receivers []string
	Subject   string

	HostName string // 本机hostname
}
