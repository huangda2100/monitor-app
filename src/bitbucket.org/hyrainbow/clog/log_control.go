package clog

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sync"
	"time"
)

type LogControl struct {
	TimeGap     int64  //间隔时间，单位秒
	FileName    string //日志文件名
	FilePath    string //日志路径
	FileOut     *os.File
	BufferedOut bufio.Writer
	FileMutex   sync.Mutex //日志锁
	LogLevel    LogLevel   //当前日志级别
	BufSize     int
	ec          *emailClient
}

// 传入timegap 单位为分钟
func (this *LogControl) Init(timegap int64, filename string, filepath string, loglevel LogLevel, logBufferSize int) (err error) {
	// 内部转化为秒
	this.TimeGap = timegap * 60
	this.FileName = filename
	this.FilePath = filepath
	this.LogLevel = loglevel

	if logBufferSize > 0 {
		this.BufSize = logBufferSize
	} else {
		this.BufSize = DefaultBufSize
	}

	err = this.open_file()
	if err != nil {
		return
	}
	go this.LogCut()

	return
}

func (this *LogControl) Write(format string, args ...interface{}) (err error) {
	if this == nil {
		fmt.Printf("FATAL:[log module not init] "+format+"\n", args...)
		return
	}

	this.FileMutex.Lock()
	defer this.FileMutex.Unlock()

	err = this.checkValid()
	if err != nil {
		return err
	}

	prefix := this.LogLevel.GetLogPrefix()

	var body string
	if args != nil {
		body = fmt.Sprintf(format, args...)
	} else {
		body = format
	}

	logData := prefix + body + "\n"
	_, err = this.BufferedOut.Write([]byte(logData))

	if this.ec != nil {
		go this.ec.SendMsgToChannel(logData)
	}
	return
}

// write2 功能，有些少量fatal日志，可以容忍，可以不需要邮件报出来
// 专门给Fatal 使用
func (this *LogControl) Write2(format string, args ...interface{}) (err error) {
	if this == nil {
		fmt.Printf("FATAL:[log module not init] "+format+"\n", args...)
		return
	}

	this.FileMutex.Lock()
	defer this.FileMutex.Unlock()
	err = this.checkValid()
	if err != nil {
		return err
	}
	prefix := this.LogLevel.GetLogPrefix()

	var body string
	if args != nil {
		body = fmt.Sprintf(format, args...)
	} else {
		body = format
	}
	logData := prefix + body + "\n"

	_, err = this.FileOut.Write([]byte(logData))
	if this.ec != nil {
		go this.ec.SendMsgToChannel2(logData)
	}
	return
}

func (this *LogControl) open_file() (err error) {
	this.FileOut, err = os.OpenFile(this.FilePath+"/"+this.FileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return
	}
	this.BufferedOut = *bufio.NewWriterSize(this.FileOut, this.BufSize)
	return
}

func (this *LogControl) checkValid() (err error) {
	//这部分代码有待商榷。。。时间开销
	_, err = os.Stat(this.FilePath + "/" + this.FileName)
	if err != nil {
		err = this.open_file()
		if err != nil {
			return
		}
	}
	return
}

func (this *LogControl) LogCut() {
	var err error
	for {
		nowtime := time.Now().Unix()
		nexttime := int64(nowtime/this.TimeGap+1) * this.TimeGap
		var delta time.Duration
		delta = time.Duration(nexttime - nowtime)
		time.Sleep(time.Second * delta)
		this.FileMutex.Lock()
		//		date_format := time.Now().Truncate(time.Duration(this.TimeGap * time.Second)).Format("200601021504")
		date_now := time.Now().Unix() - this.TimeGap
		date_now = (int64(float64(date_now)+0.5*float64(this.TimeGap)) / this.TimeGap) * this.TimeGap
		date_format := time.Unix(date_now, 0).Format("200601021504")
		cutfile := this.FilePath + this.FileName + "." + date_format + "00"
		_, err = os.Stat(cutfile)
		if err != nil { // 如果文件不存在,则切割日志. 如果文件存在，则不覆盖
			err = this.checkValid()
			if err != nil {
				log.Printf("check log file fail. err[%s]", err.Error())
				os.Exit(-1)
			}
			this.BufferedOut.Flush()
			this.FileOut.Close()
			os.Rename(this.FilePath+this.FileName, cutfile)
			err = this.open_file()
			if err != nil {
				os.Exit(-1)
			}
		}
		this.FileMutex.Unlock()
	}
	return
}
