package clog

type CLog struct {
	logControl map[LogLevel]*LogControl
	ec         *emailClient
	conf       *Conf
}

func NewClog(conf *Conf) (*CLog, error) {
	cLog := &CLog{
		conf:       conf,
		logControl: make(map[LogLevel]*LogControl, 4),
	}

	if conf.NeedEmail {
		var err error
		cLog.ec, err = newEmailClient(conf.EmailConf)
		if err != nil {
			return nil, err
		}
	}

	for _, l := range LogLevelList[:conf.MaxEnableLevel] {
		logControl := &LogControl{}
		if l == FatalLevel {
			logControl.ec = cLog.ec
		}

		err := logControl.Init(60, conf.Name+l.GetLogFileSuffix(), conf.LogPath, l, conf.BufSize[l])
		if err != nil {
			return nil, err
		}

		cLog.logControl[l] = logControl
	}

	return cLog, nil
}

func (cl *CLog) Debug(tag, format string, a ...interface{}) {
	if cl.conf.MaxEnableLevel < DebugLevel {
		return
	}
	cl.logControl[DebugLevel].Write(format, a...)
}

func (cl *CLog) Warn(tag, format string, a ...interface{}) {
	if cl.conf.MaxEnableLevel < WarningLevel {
		return
	}
	cl.logControl[WarningLevel].Write(format, a...)
}

func (cl *CLog) Fatal(tag, format string, a ...interface{}) {
	if cl.conf.MaxEnableLevel < FatalLevel {
		return
	}
	cl.logControl[FatalLevel].Write(format, a...)
}

func (cl *CLog) Notice(tag, format string, a ...interface{}) {
	if cl.conf.MaxEnableLevel < NoticeLevel {
		return
	}
	cl.logControl[NoticeLevel].Write(format, a...)
}
