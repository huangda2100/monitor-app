// Code generated by protoc-gen-go. DO NOT EDIT.
// source: log.proto

/*
Package log is a generated protocol buffer package.

It is generated from these files:
	log.proto

It has these top-level messages:
	TTSKLog
*/
package log

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// 日志类型
type LogType int32

const (
	LogType_LOGIN        LogType = 0
	LogType_ADD_COIN     LogType = 1
	LogType_SUB_COIN     LogType = 2
	LogType_LEVEL_UP     LogType = 3
	LogType_EXCHANGE_RMB LogType = 4
	LogType_ADVERT       LogType = 5
)

var LogType_name = map[int32]string{
	0: "LOGIN",
	1: "ADD_COIN",
	2: "SUB_COIN",
	3: "LEVEL_UP",
	4: "EXCHANGE_RMB",
	5: "ADVERT",
}
var LogType_value = map[string]int32{
	"LOGIN":        0,
	"ADD_COIN":     1,
	"SUB_COIN":     2,
	"LEVEL_UP":     3,
	"EXCHANGE_RMB": 4,
	"ADVERT":       5,
}

func (x LogType) String() string {
	return proto.EnumName(LogType_name, int32(x))
}
func (LogType) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

// 金币操作类型
type CoinOpType int32

const (
	CoinOpType_OP_NONE       CoinOpType = 0
	CoinOpType_ADVERT_BONUS  CoinOpType = 1
	CoinOpType_LV_UP         CoinOpType = 2
	CoinOpType_LOTTERY_BONUS CoinOpType = 3
	CoinOpType_LOTTERY_CONST CoinOpType = 4
	CoinOpType_LUCKY_BONUS   CoinOpType = 5
	CoinOpType_ACCELERATE    CoinOpType = 6
	CoinOpType_BUY_PET       CoinOpType = 7
	CoinOpType_FINISH_TASK   CoinOpType = 8
	CoinOpType_DAILY_SIGN    CoinOpType = 9
	CoinOpType_EXCHANGE      CoinOpType = 10
	CoinOpType_INVITEE       CoinOpType = 11
	CoinOpType_INVITER       CoinOpType = 12
	CoinOpType_ROLL_BACK     CoinOpType = 13
)

var CoinOpType_name = map[int32]string{
	0:  "OP_NONE",
	1:  "ADVERT_BONUS",
	2:  "LV_UP",
	3:  "LOTTERY_BONUS",
	4:  "LOTTERY_CONST",
	5:  "LUCKY_BONUS",
	6:  "ACCELERATE",
	7:  "BUY_PET",
	8:  "FINISH_TASK",
	9:  "DAILY_SIGN",
	10: "EXCHANGE",
	11: "INVITEE",
	12: "INVITER",
	13: "ROLL_BACK",
}
var CoinOpType_value = map[string]int32{
	"OP_NONE":       0,
	"ADVERT_BONUS":  1,
	"LV_UP":         2,
	"LOTTERY_BONUS": 3,
	"LOTTERY_CONST": 4,
	"LUCKY_BONUS":   5,
	"ACCELERATE":    6,
	"BUY_PET":       7,
	"FINISH_TASK":   8,
	"DAILY_SIGN":    9,
	"EXCHANGE":      10,
	"INVITEE":       11,
	"INVITER":       12,
	"ROLL_BACK":     13,
}

func (x CoinOpType) String() string {
	return proto.EnumName(CoinOpType_name, int32(x))
}
func (CoinOpType) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

// 日志格式
type TTSKLog struct {
	Type   LogType    `protobuf:"varint,1,opt,name=type,enum=LogType" json:"type,omitempty"`
	Openid string     `protobuf:"bytes,2,opt,name=openid" json:"openid,omitempty"`
	Lv     int32      `protobuf:"varint,3,opt,name=lv" json:"lv,omitempty"`
	Coin   int32      `protobuf:"varint,4,opt,name=coin" json:"coin,omitempty"`
	Op     CoinOpType `protobuf:"varint,5,opt,name=op,enum=CoinOpType" json:"op,omitempty"`
	Data   int32      `protobuf:"varint,6,opt,name=data" json:"data,omitempty"`
	Extra  string     `protobuf:"bytes,7,opt,name=extra" json:"extra,omitempty"`
	Imei   string     `protobuf:"bytes,8,opt,name=imei" json:"imei,omitempty"`
}

func (m *TTSKLog) Reset()                    { *m = TTSKLog{} }
func (m *TTSKLog) String() string            { return proto.CompactTextString(m) }
func (*TTSKLog) ProtoMessage()               {}
func (*TTSKLog) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *TTSKLog) GetType() LogType {
	if m != nil {
		return m.Type
	}
	return LogType_LOGIN
}

func (m *TTSKLog) GetOpenid() string {
	if m != nil {
		return m.Openid
	}
	return ""
}

func (m *TTSKLog) GetLv() int32 {
	if m != nil {
		return m.Lv
	}
	return 0
}

func (m *TTSKLog) GetCoin() int32 {
	if m != nil {
		return m.Coin
	}
	return 0
}

func (m *TTSKLog) GetOp() CoinOpType {
	if m != nil {
		return m.Op
	}
	return CoinOpType_OP_NONE
}

func (m *TTSKLog) GetData() int32 {
	if m != nil {
		return m.Data
	}
	return 0
}

func (m *TTSKLog) GetExtra() string {
	if m != nil {
		return m.Extra
	}
	return ""
}

func (m *TTSKLog) GetImei() string {
	if m != nil {
		return m.Imei
	}
	return ""
}

func init() {
	proto.RegisterType((*TTSKLog)(nil), "TTSKLog")
	proto.RegisterEnum("LogType", LogType_name, LogType_value)
	proto.RegisterEnum("CoinOpType", CoinOpType_name, CoinOpType_value)
}

func init() { proto.RegisterFile("log.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 396 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x4c, 0x92, 0xcb, 0x8a, 0xdb, 0x30,
	0x14, 0x86, 0xc7, 0x8e, 0x2f, 0xf1, 0xc9, 0xa5, 0xa7, 0xa2, 0x14, 0x43, 0xbb, 0x08, 0x5d, 0x85,
	0x59, 0x64, 0xd1, 0x3e, 0x81, 0xed, 0xa8, 0x19, 0x11, 0x55, 0x0a, 0xb2, 0x1c, 0x1a, 0x28, 0x88,
	0xb4, 0x63, 0x82, 0x21, 0x8d, 0xcc, 0x10, 0x86, 0xce, 0xcb, 0xf5, 0x61, 0xfa, 0x24, 0x45, 0x4a,
	0x86, 0x99, 0xdd, 0xf9, 0x7e, 0xfd, 0xff, 0xb9, 0x80, 0x20, 0x3b, 0xda, 0xc3, 0xa2, 0x7f, 0xb0,
	0x67, 0xfb, 0xe9, 0x6f, 0x00, 0xa9, 0xd6, 0xf5, 0x9a, 0xdb, 0x03, 0xf9, 0x08, 0xd1, 0xf9, 0xa9,
	0x6f, 0xf3, 0x60, 0x16, 0xcc, 0xa7, 0x9f, 0x87, 0x0b, 0x6e, 0x0f, 0xfa, 0xa9, 0x6f, 0x95, 0x57,
	0xc9, 0x7b, 0x48, 0x6c, 0xdf, 0x9e, 0xba, 0xfb, 0x3c, 0x9c, 0x05, 0xf3, 0x4c, 0x5d, 0x89, 0x4c,
	0x21, 0x3c, 0x3e, 0xe6, 0x83, 0x59, 0x30, 0x8f, 0x55, 0x78, 0x7c, 0x24, 0x04, 0xa2, 0x5f, 0xb6,
	0x3b, 0xe5, 0x91, 0x57, 0x7c, 0x4d, 0x3e, 0x40, 0x68, 0xfb, 0x3c, 0xf6, 0x7d, 0x47, 0x8b, 0xca,
	0x76, 0x27, 0xd9, 0xfb, 0xd6, 0xa1, 0xed, 0x5d, 0xe0, 0x7e, 0x7f, 0xde, 0xe7, 0xc9, 0x25, 0xe0,
	0x6a, 0xf2, 0x0e, 0xe2, 0xf6, 0xcf, 0xf9, 0x61, 0x9f, 0xa7, 0x7e, 0xd6, 0x05, 0x9c, 0xb3, 0xfb,
	0xdd, 0x76, 0xf9, 0xd0, 0x8b, 0xbe, 0xbe, 0xfd, 0x01, 0xe9, 0x75, 0x4f, 0x92, 0x41, 0xcc, 0xe5,
	0x8a, 0x09, 0xbc, 0x21, 0x63, 0x18, 0x16, 0xcb, 0xa5, 0xa9, 0x24, 0x13, 0x18, 0x38, 0xaa, 0x9b,
	0xf2, 0x42, 0xa1, 0x23, 0x4e, 0xb7, 0x94, 0x9b, 0x66, 0x83, 0x03, 0x82, 0x30, 0xa6, 0xdf, 0xab,
	0xbb, 0x42, 0xac, 0xa8, 0x51, 0xdf, 0x4a, 0x8c, 0x08, 0x40, 0x52, 0x2c, 0xb7, 0x54, 0x69, 0x8c,
	0x6f, 0xff, 0x05, 0x00, 0x2f, 0xeb, 0x92, 0x11, 0xa4, 0x72, 0x63, 0x84, 0x14, 0x14, 0x6f, 0x5c,
	0xf2, 0xe2, 0x33, 0xa5, 0x14, 0x4d, 0x8d, 0x81, 0x5f, 0x60, 0xeb, 0xda, 0x86, 0xe4, 0x2d, 0x4c,
	0xb8, 0xd4, 0x9a, 0xaa, 0xdd, 0xf5, 0x75, 0xf0, 0x5a, 0xaa, 0xa4, 0xa8, 0x35, 0x46, 0xe4, 0x0d,
	0x8c, 0x78, 0x53, 0xad, 0x9f, 0x3d, 0x31, 0x99, 0x02, 0x14, 0x55, 0x45, 0x39, 0x55, 0x85, 0xa6,
	0x98, 0xb8, 0x81, 0x65, 0xb3, 0x33, 0x1b, 0xaa, 0x31, 0x75, 0xee, 0xaf, 0x4c, 0xb0, 0xfa, 0xce,
	0xe8, 0xa2, 0x5e, 0xe3, 0xd0, 0xb9, 0x97, 0x05, 0xe3, 0x3b, 0x53, 0xb3, 0x95, 0xc0, 0xcc, 0x5d,
	0xf6, 0x7c, 0x0b, 0x82, 0xcb, 0x32, 0xb1, 0x65, 0x9a, 0x52, 0x1c, 0xbd, 0x80, 0xc2, 0x31, 0x99,
	0x40, 0xa6, 0x24, 0xe7, 0xa6, 0x2c, 0xaa, 0x35, 0x4e, 0x7e, 0x26, 0xfe, 0x2b, 0x7c, 0xf9, 0x1f,
	0x00, 0x00, 0xff, 0xff, 0x8a, 0x08, 0xe7, 0x11, 0x17, 0x02, 0x00, 0x00,
}
