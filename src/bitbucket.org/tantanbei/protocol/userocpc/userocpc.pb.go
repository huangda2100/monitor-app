// Code generated by protoc-gen-go. DO NOT EDIT.
// source: userocpc.proto

/*
Package userocpc is a generated protocol buffer package.

It is generated from these files:
	userocpc.proto

It has these top-level messages:
	SingleUser
	UserOcpc
	BidAdjustmentConfig
*/
package userocpc

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type SingleUser struct {
	Ideaid        string  `protobuf:"bytes,1,opt,name=ideaid" json:"ideaid,omitempty"`
	Userid        string  `protobuf:"bytes,2,opt,name=userid" json:"userid,omitempty"`
	Cost          string  `protobuf:"bytes,3,opt,name=cost" json:"cost,omitempty"`
	Ctrcnt        string  `protobuf:"bytes,4,opt,name=ctrcnt" json:"ctrcnt,omitempty"`
	Cvrcnt        string  `protobuf:"bytes,5,opt,name=cvrcnt" json:"cvrcnt,omitempty"`
	Adclass       string  `protobuf:"bytes,6,opt,name=adclass" json:"adclass,omitempty"`
	AdclassCost   string  `protobuf:"bytes,7,opt,name=adclassCost" json:"adclassCost,omitempty"`
	AdclassCtrcnt string  `protobuf:"bytes,8,opt,name=adclassCtrcnt" json:"adclassCtrcnt,omitempty"`
	AdclassCvrcnt string  `protobuf:"bytes,9,opt,name=adclassCvrcnt" json:"adclassCvrcnt,omitempty"`
	Kvalue        string  `protobuf:"bytes,10,opt,name=kvalue" json:"kvalue,omitempty"`
	Hpcvr         float64 `protobuf:"fixed64,11,opt,name=hpcvr" json:"hpcvr,omitempty"`
	Calibration   float64 `protobuf:"fixed64,12,opt,name=calibration" json:"calibration,omitempty"`
	Cvr3Cali      float64 `protobuf:"fixed64,13,opt,name=cvr3Cali" json:"cvr3Cali,omitempty"`
	Cvr3Cnt       int64   `protobuf:"varint,14,opt,name=cvr3Cnt" json:"cvr3Cnt,omitempty"`
	Kvalue1       float64 `protobuf:"fixed64,15,opt,name=kvalue1" json:"kvalue1,omitempty"`
	Kvalue2       float64 `protobuf:"fixed64,16,opt,name=kvalue2" json:"kvalue2,omitempty"`
	CpaSugest     float64 `protobuf:"fixed64,18,opt,name=cpaSugest" json:"cpaSugest,omitempty"`
	T             float64 `protobuf:"fixed64,19,opt,name=t" json:"t,omitempty"`
	CpcBid        float64 `protobuf:"fixed64,20,opt,name=cpcBid" json:"cpcBid,omitempty"`
}

func (m *SingleUser) Reset()                    { *m = SingleUser{} }
func (m *SingleUser) String() string            { return proto.CompactTextString(m) }
func (*SingleUser) ProtoMessage()               {}
func (*SingleUser) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *SingleUser) GetIdeaid() string {
	if m != nil {
		return m.Ideaid
	}
	return ""
}

func (m *SingleUser) GetUserid() string {
	if m != nil {
		return m.Userid
	}
	return ""
}

func (m *SingleUser) GetCost() string {
	if m != nil {
		return m.Cost
	}
	return ""
}

func (m *SingleUser) GetCtrcnt() string {
	if m != nil {
		return m.Ctrcnt
	}
	return ""
}

func (m *SingleUser) GetCvrcnt() string {
	if m != nil {
		return m.Cvrcnt
	}
	return ""
}

func (m *SingleUser) GetAdclass() string {
	if m != nil {
		return m.Adclass
	}
	return ""
}

func (m *SingleUser) GetAdclassCost() string {
	if m != nil {
		return m.AdclassCost
	}
	return ""
}

func (m *SingleUser) GetAdclassCtrcnt() string {
	if m != nil {
		return m.AdclassCtrcnt
	}
	return ""
}

func (m *SingleUser) GetAdclassCvrcnt() string {
	if m != nil {
		return m.AdclassCvrcnt
	}
	return ""
}

func (m *SingleUser) GetKvalue() string {
	if m != nil {
		return m.Kvalue
	}
	return ""
}

func (m *SingleUser) GetHpcvr() float64 {
	if m != nil {
		return m.Hpcvr
	}
	return 0
}

func (m *SingleUser) GetCalibration() float64 {
	if m != nil {
		return m.Calibration
	}
	return 0
}

func (m *SingleUser) GetCvr3Cali() float64 {
	if m != nil {
		return m.Cvr3Cali
	}
	return 0
}

func (m *SingleUser) GetCvr3Cnt() int64 {
	if m != nil {
		return m.Cvr3Cnt
	}
	return 0
}

func (m *SingleUser) GetKvalue1() float64 {
	if m != nil {
		return m.Kvalue1
	}
	return 0
}

func (m *SingleUser) GetKvalue2() float64 {
	if m != nil {
		return m.Kvalue2
	}
	return 0
}

func (m *SingleUser) GetCpaSugest() float64 {
	if m != nil {
		return m.CpaSugest
	}
	return 0
}

func (m *SingleUser) GetT() float64 {
	if m != nil {
		return m.T
	}
	return 0
}

func (m *SingleUser) GetCpcBid() float64 {
	if m != nil {
		return m.CpcBid
	}
	return 0
}

type UserOcpc struct {
	User []*SingleUser `protobuf:"bytes,1,rep,name=user" json:"user,omitempty"`
}

func (m *UserOcpc) Reset()                    { *m = UserOcpc{} }
func (m *UserOcpc) String() string            { return proto.CompactTextString(m) }
func (*UserOcpc) ProtoMessage()               {}
func (*UserOcpc) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *UserOcpc) GetUser() []*SingleUser {
	if m != nil {
		return m.User
	}
	return nil
}

type BidAdjustmentConfig struct {
	IdeaRate     map[int32]float64  `protobuf:"bytes,1,rep,name=ideaRate" json:"ideaRate,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"fixed64,2,opt,name=value"`
	AdslotidRate map[string]float64 `protobuf:"bytes,2,rep,name=adslotidRate" json:"adslotidRate,omitempty" protobuf_key:"bytes,1,opt,name=key" protobuf_val:"fixed64,2,opt,name=value"`
	GlobalRate   float64            `protobuf:"fixed64,3,opt,name=globalRate" json:"globalRate,omitempty"`
}

func (m *BidAdjustmentConfig) Reset()                    { *m = BidAdjustmentConfig{} }
func (m *BidAdjustmentConfig) String() string            { return proto.CompactTextString(m) }
func (*BidAdjustmentConfig) ProtoMessage()               {}
func (*BidAdjustmentConfig) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *BidAdjustmentConfig) GetIdeaRate() map[int32]float64 {
	if m != nil {
		return m.IdeaRate
	}
	return nil
}

func (m *BidAdjustmentConfig) GetAdslotidRate() map[string]float64 {
	if m != nil {
		return m.AdslotidRate
	}
	return nil
}

func (m *BidAdjustmentConfig) GetGlobalRate() float64 {
	if m != nil {
		return m.GlobalRate
	}
	return 0
}

func init() {
	proto.RegisterType((*SingleUser)(nil), "userocpc.SingleUser")
	proto.RegisterType((*UserOcpc)(nil), "userocpc.UserOcpc")
	proto.RegisterType((*BidAdjustmentConfig)(nil), "userocpc.BidAdjustmentConfig")
}

func init() { proto.RegisterFile("userocpc.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 461 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x84, 0x53, 0xd1, 0x8a, 0xd3, 0x40,
	0x14, 0x65, 0x92, 0xb6, 0x9b, 0xde, 0xb6, 0xeb, 0x3a, 0x5b, 0xe4, 0x52, 0x44, 0x42, 0xf1, 0x21,
	0x20, 0x54, 0xec, 0xfa, 0x20, 0xfa, 0x20, 0xbb, 0x45, 0xc4, 0x27, 0x21, 0xc5, 0x0f, 0x98, 0xce,
	0x8c, 0x75, 0xdc, 0x98, 0x84, 0x64, 0x1a, 0xd8, 0x1f, 0xf0, 0xff, 0xfc, 0x23, 0x99, 0x3b, 0x49,
	0x9b, 0xa2, 0xe8, 0xdb, 0x3d, 0xe7, 0xdc, 0x7b, 0xee, 0x6d, 0x73, 0x06, 0x2e, 0x0f, 0xb5, 0xae,
	0x0a, 0x59, 0xca, 0x55, 0x59, 0x15, 0xb6, 0xe0, 0x51, 0x87, 0x97, 0x3f, 0x07, 0x00, 0x5b, 0x93,
	0xef, 0x33, 0xfd, 0xa5, 0xd6, 0x15, 0x7f, 0x02, 0x23, 0xa3, 0xb4, 0x30, 0x0a, 0x59, 0xcc, 0x92,
	0x71, 0xda, 0x22, 0xc7, 0xbb, 0x11, 0xa3, 0x30, 0xf0, 0xbc, 0x47, 0x9c, 0xc3, 0x40, 0x16, 0xb5,
	0xc5, 0x90, 0x58, 0xaa, 0x5d, 0xaf, 0xb4, 0x95, 0xcc, 0x2d, 0x0e, 0x7c, 0xaf, 0x47, 0xc4, 0x37,
	0xc4, 0x0f, 0x5b, 0x9e, 0x10, 0x47, 0xb8, 0x10, 0x4a, 0x66, 0xa2, 0xae, 0x71, 0x44, 0x42, 0x07,
	0x79, 0x0c, 0x93, 0xb6, 0xdc, 0xb8, 0x25, 0x17, 0xa4, 0xf6, 0x29, 0xfe, 0x1c, 0x66, 0x1d, 0xf4,
	0x2b, 0x23, 0xea, 0x39, 0x27, 0xfb, 0x5d, 0xfe, 0x80, 0xf1, 0x79, 0x57, 0xd3, 0xdd, 0x77, 0xdf,
	0x88, 0xec, 0xa0, 0x11, 0xfc, 0x7d, 0x1e, 0xf1, 0x39, 0x0c, 0xbf, 0x95, 0xb2, 0xa9, 0x70, 0x12,
	0xb3, 0x84, 0xa5, 0x1e, 0xb8, 0xdb, 0xa4, 0xc8, 0xcc, 0xae, 0x12, 0xd6, 0x14, 0x39, 0x4e, 0x49,
	0xeb, 0x53, 0x7c, 0x01, 0x91, 0x6c, 0xaa, 0x9b, 0x8d, 0xc8, 0x0c, 0xce, 0x48, 0x3e, 0x62, 0xf7,
	0x9b, 0xa9, 0xce, 0x2d, 0x5e, 0xc6, 0x2c, 0x09, 0xd3, 0x0e, 0x3a, 0xc5, 0xef, 0x7d, 0x85, 0x8f,
	0x68, 0xa8, 0x83, 0x27, 0x65, 0x8d, 0x57, 0x7d, 0x65, 0xcd, 0x9f, 0xc2, 0x58, 0x96, 0x62, 0x7b,
	0xd8, 0xeb, 0xda, 0x22, 0x27, 0xed, 0x44, 0xf0, 0x29, 0x30, 0x8b, 0xd7, 0xc4, 0x32, 0xff, 0x15,
	0x4a, 0x79, 0x67, 0x14, 0xce, 0x89, 0x6a, 0xd1, 0xf2, 0x35, 0x44, 0x2e, 0x01, 0x9f, 0x65, 0x29,
	0x79, 0x02, 0x03, 0xf7, 0x7d, 0x91, 0xc5, 0x61, 0x32, 0x59, 0xcf, 0x57, 0xc7, 0xf4, 0x9c, 0x92,
	0x92, 0x52, 0xc7, 0xf2, 0x57, 0x00, 0xd7, 0x77, 0x46, 0xdd, 0xaa, 0xef, 0x87, 0xda, 0xfe, 0xd0,
	0xb9, 0xdd, 0x14, 0xf9, 0x57, 0xb3, 0xe7, 0x1f, 0x21, 0x72, 0xc9, 0x49, 0x85, 0xd5, 0xad, 0xcb,
	0x8b, 0x93, 0xcb, 0x5f, 0x06, 0x56, 0x9f, 0xda, 0xee, 0x0f, 0xb9, 0xad, 0x1e, 0xd2, 0xe3, 0x30,
	0xdf, 0xc2, 0x54, 0xa8, 0x3a, 0x2b, 0xac, 0x51, 0x64, 0x16, 0x90, 0xd9, 0xcb, 0x7f, 0x9b, 0xdd,
	0xf6, 0x26, 0xbc, 0xe1, 0x99, 0x09, 0x7f, 0x06, 0xb0, 0xcf, 0x8a, 0x9d, 0xc8, 0xc8, 0x32, 0xa4,
	0xff, 0xa1, 0xc7, 0x2c, 0xde, 0xc1, 0xec, 0xec, 0x1e, 0x7e, 0x05, 0xe1, 0xbd, 0x7e, 0xa0, 0x37,
	0x31, 0x4c, 0x5d, 0xe9, 0x42, 0xe1, 0xb3, 0x12, 0xf8, 0x50, 0x10, 0x78, 0x1b, 0xbc, 0x61, 0x8b,
	0xf7, 0xf0, 0xf8, 0x8f, 0xfd, 0x7d, 0x83, 0xf1, 0x7f, 0x0c, 0x76, 0x23, 0x7a, 0xa3, 0x37, 0xbf,
	0x03, 0x00, 0x00, 0xff, 0xff, 0x2d, 0x03, 0xee, 0x6d, 0xb5, 0x03, 0x00, 0x00,
}
