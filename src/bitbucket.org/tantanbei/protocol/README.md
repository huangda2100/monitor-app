## cpc 项目所有内部通讯协议

### 使用说明
这里仅提供golang项目的说明，其他语言请咨询相关同事。  
假如你要做一个需求，需要更改proto文件，该proto会被项目A引用，测试与上线的操作步骤如下：

#### 测试
1. 安装好protoc命令
2. 如A使用govendor
  + 来到protocol项目下，更改proto，编译得到新版的xx.proto.go，xx.py文件
  + 手动拷贝到项目A的依赖中，打包测试
3. 如A使用go mod
  + 更新protocol项目，编译，push到自己的分支。
  + 来到A目录下，运行命令`go get -u bitbucket.org/tantanbei/protocol@{分支名}`。如：`go get -u bitbucket.org/tantanbei/protocol@xialu-branch`，go.mod文件和go.sum文件就会更新为相应版本了。

#### 上线
1. 更新protocol项目，这包括：
  + proto文件
  + 编译生成的xx.proto.go，xx.py文件
2. 向protocol项目发起mr以更新相应内容到master
3. 更新A的protocol版本，根据不同包管理方式，更新方式如下
  + go mod：protocol mr merge后，来到A的根目录下，执行命令`go get -u bitbucket.org/tantanbei/protocol@master`  
  你可以使用`go get -u bitbucket.org/tantanbei/protocol@{commitId}`更新依赖到`commitId`对应的版本
  + govendor：来到A的根目录下，执行`govendor fetch bitbucket.org/tantanbei/protocol`
4. 向A项目发起mr

#### 安装protoc命令：
1. qdc-techcenter-test-cpc-01:10.0.2.97 机器已安装了相应的环境
2. 使用命令 `bash compile.sh init` 自动安装
3. 手动安装
    1. [安装protoc命令](https://github.com/protocolbuffers/protobuf/releases/tag/v3.5.1)，版本3.5.1即可
    2. [安装protoc-gen-go](https://github.com/golang/protobuf)，版本必须为`v1.1.0`  
       + 使用go get安装只能得到最新版，此时可以进入相关包路径下，切换其版本，如： `cd ~/.go/src/github.com/golang/protobuf && git checkout v1.1.0`
       + 运行  `make`  命令安装（注意运行命令时的工作目录应为`protobuf`，而不是`protoc-gen-go`）

#### 编译方法：
在protocol项目根目录下，执行命令`bash compile.sh ${文件名}`，如：`bash compile.sh idxinterface/idx.proto`  
   > 说明：这等同于执行`protoc --go_out=plugins=grpc,paths=source_relative:. --python_out=plugins=grpc:. ${文件名}`，其中 `plugins=grpc` 的作用是调用grpc插件编译相应代码，如果proto文件中没有使用grpc，可以除去。--python_out的作用是同时生成.py文件。
