// Code generated by protoc-gen-go. DO NOT EDIT.
// source: cvrModelParams.proto

/*
Package cvrModelParams is a generated protocol buffer package.

It is generated from these files:
	cvrModelParams.proto

It has these top-level messages:
	CvrModelFactor
	CvrModelFactorList
*/
package cvrModelParams

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type CvrModelFactor struct {
	CvrModelName string  `protobuf:"bytes,1,opt,name=cvr_model_name,json=cvrModelName" json:"cvr_model_name,omitempty"`
	CvrFatcor    float64 `protobuf:"fixed64,2,opt,name=cvr_fatcor,json=cvrFatcor" json:"cvr_fatcor,omitempty"`
	CvrType      string  `protobuf:"bytes,3,opt,name=cvr_type,json=cvrType" json:"cvr_type,omitempty"`
}

func (m *CvrModelFactor) Reset()                    { *m = CvrModelFactor{} }
func (m *CvrModelFactor) String() string            { return proto.CompactTextString(m) }
func (*CvrModelFactor) ProtoMessage()               {}
func (*CvrModelFactor) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *CvrModelFactor) GetCvrModelName() string {
	if m != nil {
		return m.CvrModelName
	}
	return ""
}

func (m *CvrModelFactor) GetCvrFatcor() float64 {
	if m != nil {
		return m.CvrFatcor
	}
	return 0
}

func (m *CvrModelFactor) GetCvrType() string {
	if m != nil {
		return m.CvrType
	}
	return ""
}

type CvrModelFactorList struct {
	Records []*CvrModelFactor `protobuf:"bytes,1,rep,name=records" json:"records,omitempty"`
}

func (m *CvrModelFactorList) Reset()                    { *m = CvrModelFactorList{} }
func (m *CvrModelFactorList) String() string            { return proto.CompactTextString(m) }
func (*CvrModelFactorList) ProtoMessage()               {}
func (*CvrModelFactorList) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *CvrModelFactorList) GetRecords() []*CvrModelFactor {
	if m != nil {
		return m.Records
	}
	return nil
}

func init() {
	proto.RegisterType((*CvrModelFactor)(nil), "cvrModelParams.CvrModelFactor")
	proto.RegisterType((*CvrModelFactorList)(nil), "cvrModelParams.CvrModelFactorList")
}

func init() { proto.RegisterFile("cvrModelParams.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 182 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x49, 0x2e, 0x2b, 0xf2,
	0xcd, 0x4f, 0x49, 0xcd, 0x09, 0x48, 0x2c, 0x4a, 0xcc, 0x2d, 0xd6, 0x2b, 0x28, 0xca, 0x2f, 0xc9,
	0x17, 0xe2, 0x43, 0x15, 0x55, 0x2a, 0xe0, 0xe2, 0x73, 0x86, 0x8a, 0xb8, 0x25, 0x26, 0x97, 0xe4,
	0x17, 0x09, 0xa9, 0x70, 0x81, 0xd4, 0xc4, 0xe7, 0x82, 0x84, 0xe2, 0xf3, 0x12, 0x73, 0x53, 0x25,
	0x18, 0x15, 0x18, 0x35, 0x38, 0x83, 0x78, 0x60, 0x3a, 0xfd, 0x12, 0x73, 0x53, 0x85, 0x64, 0xb9,
	0xb8, 0x40, 0xaa, 0xd2, 0x12, 0x4b, 0x92, 0xf3, 0x8b, 0x24, 0x98, 0x14, 0x18, 0x35, 0x18, 0x83,
	0x38, 0x93, 0xcb, 0x8a, 0xdc, 0xc0, 0x02, 0x42, 0x92, 0x5c, 0x1c, 0x20, 0xe9, 0x92, 0xca, 0x82,
	0x54, 0x09, 0x66, 0xb0, 0x76, 0xf6, 0xe4, 0xb2, 0xa2, 0x90, 0xca, 0x82, 0x54, 0x25, 0x3f, 0x2e,
	0x21, 0x54, 0x1b, 0x7d, 0x32, 0x8b, 0x4b, 0x84, 0x2c, 0xb8, 0xd8, 0x8b, 0x52, 0x93, 0xf3, 0x8b,
	0x52, 0x8a, 0x25, 0x18, 0x15, 0x98, 0x35, 0xb8, 0x8d, 0xe4, 0xf4, 0xd0, 0xdc, 0x8f, 0xaa, 0x29,
	0x08, 0xa6, 0x3c, 0x89, 0x0d, 0xec, 0x31, 0x63, 0x40, 0x00, 0x00, 0x00, 0xff, 0xff, 0x04, 0x6c,
	0x9d, 0xac, 0xf0, 0x00, 0x00, 0x00,
}
