// Code generated by protoc-gen-go. DO NOT EDIT.
// source: ocpx_common.proto

/*
Package ocpx is a generated protocol buffer package.

It is generated from these files:
	ocpx_common.proto

It has these top-level messages:
	OcpxLog
	OcpmItemInfoValue
	OcpmItemExtraInfoValue
	IsotonicParamsValue
	IsotonicParams
	OcpcItemInfo
	OcpmItemInfo
*/
package ocpx

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// 写入 adx log 中 ocpx log 的字段
type OcpxLog struct {
	OcpmFinalCtcvr int64   `protobuf:"varint,1,opt,name=ocpm_final_ctcvr,json=ocpmFinalCtcvr" json:"ocpm_final_ctcvr,omitempty"`
	PrevCtcvrMean  int64   `protobuf:"varint,2,opt,name=prev_ctcvr_mean,json=prevCtcvrMean" json:"prev_ctcvr_mean,omitempty"`
	PostCtcvrMean  int64   `protobuf:"varint,3,opt,name=post_ctcvr_mean,json=postCtcvrMean" json:"post_ctcvr_mean,omitempty"`
	CtrlFactor     float64 `protobuf:"fixed64,4,opt,name=ctrl_factor,json=ctrlFactor" json:"ctrl_factor,omitempty"`
}

func (m *OcpxLog) Reset()                    { *m = OcpxLog{} }
func (m *OcpxLog) String() string            { return proto.CompactTextString(m) }
func (*OcpxLog) ProtoMessage()               {}
func (*OcpxLog) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *OcpxLog) GetOcpmFinalCtcvr() int64 {
	if m != nil {
		return m.OcpmFinalCtcvr
	}
	return 0
}

func (m *OcpxLog) GetPrevCtcvrMean() int64 {
	if m != nil {
		return m.PrevCtcvrMean
	}
	return 0
}

func (m *OcpxLog) GetPostCtcvrMean() int64 {
	if m != nil {
		return m.PostCtcvrMean
	}
	return 0
}

func (m *OcpxLog) GetCtrlFactor() float64 {
	if m != nil {
		return m.CtrlFactor
	}
	return 0
}

// 每个ocpm 单元的信息
type OcpmItemInfoValue struct {
	CpaBid       int64 `protobuf:"varint,1,opt,name=cpa_bid,json=cpaBid" json:"cpa_bid,omitempty"`
	IsOcpmHidden bool  `protobuf:"varint,2,opt,name=is_ocpm_hidden,json=isOcpmHidden" json:"is_ocpm_hidden,omitempty"`
}

func (m *OcpmItemInfoValue) Reset()                    { *m = OcpmItemInfoValue{} }
func (m *OcpmItemInfoValue) String() string            { return proto.CompactTextString(m) }
func (*OcpmItemInfoValue) ProtoMessage()               {}
func (*OcpmItemInfoValue) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *OcpmItemInfoValue) GetCpaBid() int64 {
	if m != nil {
		return m.CpaBid
	}
	return 0
}

func (m *OcpmItemInfoValue) GetIsOcpmHidden() bool {
	if m != nil {
		return m.IsOcpmHidden
	}
	return false
}

// 每个ocpc单元额外的信息（可能在单元外有额外维度）
type OcpmItemExtraInfoValue struct {
	PrevCtcvrMean int64   `protobuf:"varint,1,opt,name=prev_ctcvr_mean,json=prevCtcvrMean" json:"prev_ctcvr_mean,omitempty"`
	PostCtcvrMean int64   `protobuf:"varint,2,opt,name=post_ctcvr_mean,json=postCtcvrMean" json:"post_ctcvr_mean,omitempty"`
	CtrlFactor    float64 `protobuf:"fixed64,3,opt,name=ctrl_factor,json=ctrlFactor" json:"ctrl_factor,omitempty"`
}

func (m *OcpmItemExtraInfoValue) Reset()                    { *m = OcpmItemExtraInfoValue{} }
func (m *OcpmItemExtraInfoValue) String() string            { return proto.CompactTextString(m) }
func (*OcpmItemExtraInfoValue) ProtoMessage()               {}
func (*OcpmItemExtraInfoValue) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *OcpmItemExtraInfoValue) GetPrevCtcvrMean() int64 {
	if m != nil {
		return m.PrevCtcvrMean
	}
	return 0
}

func (m *OcpmItemExtraInfoValue) GetPostCtcvrMean() int64 {
	if m != nil {
		return m.PostCtcvrMean
	}
	return 0
}

func (m *OcpmItemExtraInfoValue) GetCtrlFactor() float64 {
	if m != nil {
		return m.CtrlFactor
	}
	return 0
}

// 保序回归参数信息
type IsotonicParamsValue struct {
	Boundaries []int64 `protobuf:"varint,1,rep,packed,name=boundaries" json:"boundaries,omitempty"`
	Values     []int32 `protobuf:"varint,2,rep,packed,name=values" json:"values,omitempty"`
}

func (m *IsotonicParamsValue) Reset()                    { *m = IsotonicParamsValue{} }
func (m *IsotonicParamsValue) String() string            { return proto.CompactTextString(m) }
func (*IsotonicParamsValue) ProtoMessage()               {}
func (*IsotonicParamsValue) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{3} }

func (m *IsotonicParamsValue) GetBoundaries() []int64 {
	if m != nil {
		return m.Boundaries
	}
	return nil
}

func (m *IsotonicParamsValue) GetValues() []int32 {
	if m != nil {
		return m.Values
	}
	return nil
}

// 保序回归分单元信息
type IsotonicParams struct {
	ParamInfos map[string]*IsotonicParamsValue `protobuf:"bytes,1,rep,name=param_infos,json=paramInfos" json:"param_infos,omitempty" protobuf_key:"bytes,1,opt,name=key" protobuf_val:"bytes,2,opt,name=value"`
}

func (m *IsotonicParams) Reset()                    { *m = IsotonicParams{} }
func (m *IsotonicParams) String() string            { return proto.CompactTextString(m) }
func (*IsotonicParams) ProtoMessage()               {}
func (*IsotonicParams) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{4} }

func (m *IsotonicParams) GetParamInfos() map[string]*IsotonicParamsValue {
	if m != nil {
		return m.ParamInfos
	}
	return nil
}

// 用于ocpc 字典外层包装
type OcpcItemInfo struct {
	IsotonicParams      *IsotonicParams `protobuf:"bytes,1,opt,name=isotonic_params,json=isotonicParams" json:"isotonic_params,omitempty"`
	ModelIsotonicParams *IsotonicParams `protobuf:"bytes,2,opt,name=model_isotonic_params,json=modelIsotonicParams" json:"model_isotonic_params,omitempty"`
}

func (m *OcpcItemInfo) Reset()                    { *m = OcpcItemInfo{} }
func (m *OcpcItemInfo) String() string            { return proto.CompactTextString(m) }
func (*OcpcItemInfo) ProtoMessage()               {}
func (*OcpcItemInfo) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{5} }

func (m *OcpcItemInfo) GetIsotonicParams() *IsotonicParams {
	if m != nil {
		return m.IsotonicParams
	}
	return nil
}

func (m *OcpcItemInfo) GetModelIsotonicParams() *IsotonicParams {
	if m != nil {
		return m.ModelIsotonicParams
	}
	return nil
}

// 字典包装类
type OcpmItemInfo struct {
	// map<unit_id, OcpxItemInfoValue>
	ItemInfos map[string]*OcpmItemInfoValue `protobuf:"bytes,1,rep,name=item_infos,json=itemInfos" json:"item_infos,omitempty" protobuf_key:"bytes,1,opt,name=key" protobuf_val:"bytes,2,opt,name=value"`
	// map<ctr_model_name & cvr_model_name & slot_type & unit_id, OcpxItemExtraInfoValue>
	ItemExtraInfos map[string]*OcpmItemExtraInfoValue `protobuf:"bytes,2,rep,name=item_extra_infos,json=itemExtraInfos" json:"item_extra_infos,omitempty" protobuf_key:"bytes,1,opt,name=key" protobuf_val:"bytes,2,opt,name=value"`
}

func (m *OcpmItemInfo) Reset()                    { *m = OcpmItemInfo{} }
func (m *OcpmItemInfo) String() string            { return proto.CompactTextString(m) }
func (*OcpmItemInfo) ProtoMessage()               {}
func (*OcpmItemInfo) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{6} }

func (m *OcpmItemInfo) GetItemInfos() map[string]*OcpmItemInfoValue {
	if m != nil {
		return m.ItemInfos
	}
	return nil
}

func (m *OcpmItemInfo) GetItemExtraInfos() map[string]*OcpmItemExtraInfoValue {
	if m != nil {
		return m.ItemExtraInfos
	}
	return nil
}

func init() {
	proto.RegisterType((*OcpxLog)(nil), "ocpx.OcpxLog")
	proto.RegisterType((*OcpmItemInfoValue)(nil), "ocpx.OcpmItemInfoValue")
	proto.RegisterType((*OcpmItemExtraInfoValue)(nil), "ocpx.OcpmItemExtraInfoValue")
	proto.RegisterType((*IsotonicParamsValue)(nil), "ocpx.IsotonicParamsValue")
	proto.RegisterType((*IsotonicParams)(nil), "ocpx.IsotonicParams")
	proto.RegisterType((*OcpcItemInfo)(nil), "ocpx.OcpcItemInfo")
	proto.RegisterType((*OcpmItemInfo)(nil), "ocpx.OcpmItemInfo")
}

func init() { proto.RegisterFile("ocpx_common.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 525 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x94, 0x5f, 0x6e, 0xd3, 0x30,
	0x1c, 0xc7, 0xe5, 0x64, 0xeb, 0xd8, 0xaf, 0x23, 0xed, 0x5c, 0xd8, 0x4a, 0x85, 0xa0, 0x44, 0xd3,
	0xd4, 0x17, 0x82, 0x54, 0x5e, 0x10, 0x12, 0x12, 0x02, 0x75, 0x5a, 0x25, 0xa6, 0x4e, 0x91, 0x40,
	0xbc, 0x59, 0xae, 0xe3, 0x82, 0x45, 0x13, 0x5b, 0x89, 0x57, 0x75, 0x47, 0xe0, 0x04, 0x5c, 0x80,
	0x0b, 0x70, 0x1d, 0x4e, 0x83, 0xec, 0xa4, 0x5d, 0xdd, 0x85, 0x69, 0x4f, 0xb5, 0xbf, 0xfd, 0xfa,
	0xf3, 0xfb, 0xdb, 0xc2, 0xa1, 0x64, 0x6a, 0x49, 0x98, 0x4c, 0x53, 0x99, 0x45, 0x2a, 0x97, 0x5a,
	0xe2, 0x1d, 0x23, 0x85, 0xbf, 0x11, 0xec, 0x4d, 0x98, 0x5a, 0x7e, 0x92, 0xdf, 0xf0, 0x00, 0xda,
	0x92, 0xa9, 0x94, 0xcc, 0x44, 0x46, 0xe7, 0x84, 0x69, 0xb6, 0xc8, 0xbb, 0xa8, 0x8f, 0x06, 0x7e,
	0x1c, 0x18, 0xfd, 0xcc, 0xc8, 0x1f, 0x8d, 0x8a, 0x4f, 0xa1, 0xa5, 0x72, 0xbe, 0x28, 0x3d, 0x24,
	0xe5, 0x34, 0xeb, 0x7a, 0xd6, 0xf8, 0xd0, 0xc8, 0xd6, 0x73, 0xc1, 0x69, 0x66, 0x7d, 0xb2, 0xd0,
	0x9b, 0x3e, 0xbf, 0xf2, 0xc9, 0x42, 0xdf, 0xf8, 0x9e, 0x43, 0x93, 0xe9, 0x7c, 0x4e, 0x66, 0x94,
	0x69, 0x99, 0x77, 0x77, 0xfa, 0x68, 0x80, 0x62, 0x30, 0xd2, 0x99, 0x55, 0xc2, 0x18, 0x0e, 0x27,
	0x4c, 0xa5, 0x63, 0xcd, 0xd3, 0x71, 0x36, 0x93, 0x5f, 0xe8, 0xfc, 0x8a, 0xe3, 0x63, 0xd8, 0x63,
	0x8a, 0x92, 0xa9, 0x48, 0xaa, 0x34, 0x1b, 0x4c, 0xd1, 0x0f, 0x22, 0xc1, 0x27, 0x10, 0x88, 0x82,
	0xd8, 0x5a, 0xbe, 0x8b, 0x24, 0xe1, 0x65, 0x76, 0x0f, 0xe2, 0x03, 0x51, 0x18, 0xca, 0xb9, 0xd5,
	0xc2, 0x9f, 0x08, 0x8e, 0x56, 0xd0, 0xd1, 0x52, 0xe7, 0xf4, 0x86, 0x5c, 0x53, 0x1f, 0xba, 0x67,
	0x7d, 0xde, 0x3d, 0xea, 0xf3, 0x6f, 0xd5, 0x77, 0x01, 0x9d, 0x71, 0x21, 0xb5, 0xcc, 0x04, 0xbb,
	0xa4, 0x39, 0x4d, 0x8b, 0x32, 0x8f, 0x67, 0x00, 0x53, 0x79, 0x95, 0x25, 0x34, 0x17, 0xbc, 0xe8,
	0xa2, 0xbe, 0x3f, 0xf0, 0xe3, 0x0d, 0x05, 0x1f, 0x41, 0x63, 0x61, 0x8c, 0x45, 0xd7, 0xeb, 0xfb,
	0x83, 0xdd, 0xb8, 0xba, 0x85, 0x7f, 0x10, 0x04, 0x2e, 0x0f, 0x8f, 0xa0, 0xa9, 0xcc, 0x89, 0x88,
	0x6c, 0x26, 0x4b, 0x56, 0x73, 0x78, 0x12, 0x99, 0x25, 0x88, 0x5c, 0x6b, 0x64, 0x3f, 0x4c, 0x33,
	0x8a, 0x51, 0xa6, 0xf3, 0xeb, 0x18, 0xd4, 0x5a, 0xe8, 0x7d, 0x85, 0xd6, 0xd6, 0xd7, 0xb8, 0x0d,
	0xfe, 0x0f, 0x7e, 0x6d, 0x1b, 0xb4, 0x1f, 0x9b, 0x23, 0x7e, 0x05, 0xbb, 0x36, 0x11, 0xdb, 0x8c,
	0xe6, 0xf0, 0x49, 0x5d, 0x14, 0x5b, 0x60, 0x5c, 0xfa, 0xde, 0x7a, 0x6f, 0x50, 0xf8, 0x0b, 0xc1,
	0xc1, 0x84, 0x29, 0xb6, 0x9a, 0x31, 0x7e, 0x07, 0x2d, 0x51, 0x3d, 0x21, 0x36, 0x83, 0xc2, 0xc6,
	0x68, 0x0e, 0x1f, 0xd5, 0xf1, 0xe2, 0x40, 0xb8, 0x05, 0x9f, 0xc3, 0xe3, 0x54, 0x26, 0x7c, 0x4e,
	0xb6, 0x21, 0xde, 0x1d, 0x90, 0x8e, 0x7d, 0xe2, 0x8a, 0xe1, 0x5f, 0xcf, 0x66, 0xb6, 0xde, 0x3e,
	0xfc, 0x1e, 0x40, 0x68, 0xee, 0xb6, 0xf2, 0x45, 0xc9, 0xdb, 0xf4, 0x45, 0xab, 0x43, 0xd5, 0xc7,
	0x7d, 0xb1, 0xba, 0xe3, 0x4b, 0x68, 0x5b, 0x02, 0x37, 0x7b, 0x57, 0x71, 0x3c, 0xcb, 0x39, 0xfd,
	0x0f, 0x67, 0xbd, 0xa1, 0x15, 0x2c, 0x10, 0x8e, 0xd8, 0xfb, 0x0c, 0x81, 0x1b, 0xae, 0x66, 0x2e,
	0x2f, 0xdd, 0xb9, 0x1c, 0xdf, 0x0e, 0xb5, 0x3d, 0x95, 0x1e, 0x81, 0x4e, 0x4d, 0xf4, 0x1a, 0xf6,
	0xd0, 0x65, 0x3f, 0x75, 0xd9, 0xee, 0xef, 0x6b, 0x23, 0xc0, 0xb4, 0x61, 0xff, 0x8d, 0x5e, 0xff,
	0x0b, 0x00, 0x00, 0xff, 0xff, 0x16, 0x92, 0x7b, 0xb3, 0xa2, 0x04, 0x00, 0x00,
}
