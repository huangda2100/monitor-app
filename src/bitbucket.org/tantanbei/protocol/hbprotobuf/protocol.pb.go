// Code generated by protoc-gen-go.
// source: protocol.proto
// DO NOT EDIT!

/*
Package pbprotobuf is a generated protocol buffer package.

It is generated from these files:
	protocol.proto

It has these top-level messages:
	BidRequest
	BidResponse
*/
package pbprotobuf

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type ConnectionType int32

const (
	ConnectionType_CONNECTION_UNKNOWN ConnectionType = 0
	ConnectionType_WIFI               ConnectionType = 2
	ConnectionType_CELL_2G            ConnectionType = 4
	ConnectionType_CELL_3G            ConnectionType = 5
	ConnectionType_CELL_4G            ConnectionType = 6
)

var ConnectionType_name = map[int32]string{
	0: "CONNECTION_UNKNOWN",
	2: "WIFI",
	4: "CELL_2G",
	5: "CELL_3G",
	6: "CELL_4G",
}
var ConnectionType_value = map[string]int32{
	"CONNECTION_UNKNOWN": 0,
	"WIFI":               2,
	"CELL_2G":            4,
	"CELL_3G":            5,
	"CELL_4G":            6,
}

func (x ConnectionType) Enum() *ConnectionType {
	p := new(ConnectionType)
	*p = x
	return p
}
func (x ConnectionType) String() string {
	return proto.EnumName(ConnectionType_name, int32(x))
}
func (x *ConnectionType) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(ConnectionType_value, data, "ConnectionType")
	if err != nil {
		return err
	}
	*x = ConnectionType(value)
	return nil
}
func (ConnectionType) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

type DeviceType int32

const (
	// Phone.
	DeviceType_HIGHEND_PHONE DeviceType = 4
	// Tablet.
	DeviceType_TABLET DeviceType = 5
	// Connected device.
	DeviceType_CONNECTED_DEVICE DeviceType = 6
	// Set top box.
	DeviceType_SET_TOP_BOX DeviceType = 7
)

var DeviceType_name = map[int32]string{
	4: "HIGHEND_PHONE",
	5: "TABLET",
	6: "CONNECTED_DEVICE",
	7: "SET_TOP_BOX",
}
var DeviceType_value = map[string]int32{
	"HIGHEND_PHONE":    4,
	"TABLET":           5,
	"CONNECTED_DEVICE": 6,
	"SET_TOP_BOX":      7,
}

func (x DeviceType) Enum() *DeviceType {
	p := new(DeviceType)
	*p = x
	return p
}
func (x DeviceType) String() string {
	return proto.EnumName(DeviceType_name, int32(x))
}
func (x *DeviceType) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(DeviceType_value, data, "DeviceType")
	if err != nil {
		return err
	}
	*x = DeviceType(value)
	return nil
}
func (DeviceType) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

type AdPosition int32

const (
	AdPosition_UNKNOWN        AdPosition = 0
	AdPosition_ABOVE_THE_FOLD AdPosition = 1
	AdPosition_BELOW_THE_FOLD AdPosition = 3
)

var AdPosition_name = map[int32]string{
	0: "UNKNOWN",
	1: "ABOVE_THE_FOLD",
	3: "BELOW_THE_FOLD",
}
var AdPosition_value = map[string]int32{
	"UNKNOWN":        0,
	"ABOVE_THE_FOLD": 1,
	"BELOW_THE_FOLD": 3,
}

func (x AdPosition) Enum() *AdPosition {
	p := new(AdPosition)
	*p = x
	return p
}
func (x AdPosition) String() string {
	return proto.EnumName(AdPosition_name, int32(x))
}
func (x *AdPosition) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(AdPosition_value, data, "AdPosition")
	if err != nil {
		return err
	}
	*x = AdPosition(value)
	return nil
}
func (AdPosition) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

type BidRequest struct {
	Id               *string            `protobuf:"bytes,1,req,name=id" json:"id,omitempty"`
	Imp              []*BidRequest_Imp  `protobuf:"bytes,2,rep,name=imp" json:"imp,omitempty"`
	Device           *BidRequest_Device `protobuf:"bytes,3,opt,name=device" json:"device,omitempty"`
	Site             *BidRequest_Site   `protobuf:"bytes,4,opt,name=site" json:"site,omitempty"`
	App              *BidRequest_App    `protobuf:"bytes,5,opt,name=app" json:"app,omitempty"`
	XXX_unrecognized []byte             `json:"-"`
}

func (m *BidRequest) Reset()                    { *m = BidRequest{} }
func (m *BidRequest) String() string            { return proto.CompactTextString(m) }
func (*BidRequest) ProtoMessage()               {}
func (*BidRequest) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *BidRequest) GetId() string {
	if m != nil && m.Id != nil {
		return *m.Id
	}
	return ""
}

func (m *BidRequest) GetImp() []*BidRequest_Imp {
	if m != nil {
		return m.Imp
	}
	return nil
}

func (m *BidRequest) GetDevice() *BidRequest_Device {
	if m != nil {
		return m.Device
	}
	return nil
}

func (m *BidRequest) GetSite() *BidRequest_Site {
	if m != nil {
		return m.Site
	}
	return nil
}

func (m *BidRequest) GetApp() *BidRequest_App {
	if m != nil {
		return m.App
	}
	return nil
}

type BidRequest_Imp struct {
	Id               *string                `protobuf:"bytes,1,req,name=id" json:"id,omitempty"`
	Banner           *BidRequest_Imp_Banner `protobuf:"bytes,2,opt,name=banner" json:"banner,omitempty"`
	Bidfloor         *float64               `protobuf:"fixed64,8,opt,name=bidfloor,def=0" json:"bidfloor,omitempty"`
	Native           *BidRequest_Imp_Native `protobuf:"bytes,13,opt,name=native" json:"native,omitempty"`
	Tagid            *string                `protobuf:"bytes,7,opt,name=tagid" json:"tagid,omitempty"`
	Pmp              *BidRequest_Pmp        `protobuf:"bytes,11,opt,name=pmp" json:"pmp,omitempty"`
	XXX_unrecognized []byte                 `json:"-"`
}

func (m *BidRequest_Imp) Reset()                    { *m = BidRequest_Imp{} }
func (m *BidRequest_Imp) String() string            { return proto.CompactTextString(m) }
func (*BidRequest_Imp) ProtoMessage()               {}
func (*BidRequest_Imp) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 0} }

const Default_BidRequest_Imp_Bidfloor float64 = 0

func (m *BidRequest_Imp) GetId() string {
	if m != nil && m.Id != nil {
		return *m.Id
	}
	return ""
}

func (m *BidRequest_Imp) GetBanner() *BidRequest_Imp_Banner {
	if m != nil {
		return m.Banner
	}
	return nil
}

func (m *BidRequest_Imp) GetBidfloor() float64 {
	if m != nil && m.Bidfloor != nil {
		return *m.Bidfloor
	}
	return Default_BidRequest_Imp_Bidfloor
}

func (m *BidRequest_Imp) GetNative() *BidRequest_Imp_Native {
	if m != nil {
		return m.Native
	}
	return nil
}

func (m *BidRequest_Imp) GetTagid() string {
	if m != nil && m.Tagid != nil {
		return *m.Tagid
	}
	return ""
}

func (m *BidRequest_Imp) GetPmp() *BidRequest_Pmp {
	if m != nil {
		return m.Pmp
	}
	return nil
}

type BidRequest_Imp_Banner struct {
	W                *int32      `protobuf:"varint,1,opt,name=w" json:"w,omitempty"`
	H                *int32      `protobuf:"varint,2,opt,name=h" json:"h,omitempty"`
	Pos              *AdPosition `protobuf:"varint,4,opt,name=pos,enum=pbprotobuf.AdPosition" json:"pos,omitempty"`
	Mimes            []string    `protobuf:"bytes,7,rep,name=mimes" json:"mimes,omitempty"`
	XXX_unrecognized []byte      `json:"-"`
}

func (m *BidRequest_Imp_Banner) Reset()                    { *m = BidRequest_Imp_Banner{} }
func (m *BidRequest_Imp_Banner) String() string            { return proto.CompactTextString(m) }
func (*BidRequest_Imp_Banner) ProtoMessage()               {}
func (*BidRequest_Imp_Banner) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 0, 0} }

func (m *BidRequest_Imp_Banner) GetW() int32 {
	if m != nil && m.W != nil {
		return *m.W
	}
	return 0
}

func (m *BidRequest_Imp_Banner) GetH() int32 {
	if m != nil && m.H != nil {
		return *m.H
	}
	return 0
}

func (m *BidRequest_Imp_Banner) GetPos() AdPosition {
	if m != nil && m.Pos != nil {
		return *m.Pos
	}
	return AdPosition_UNKNOWN
}

func (m *BidRequest_Imp_Banner) GetMimes() []string {
	if m != nil {
		return m.Mimes
	}
	return nil
}

type BidRequest_Imp_Native struct {
	Request          *string `protobuf:"bytes,1,req,name=request" json:"request,omitempty"`
	Ver              *string `protobuf:"bytes,2,opt,name=ver" json:"ver,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *BidRequest_Imp_Native) Reset()                    { *m = BidRequest_Imp_Native{} }
func (m *BidRequest_Imp_Native) String() string            { return proto.CompactTextString(m) }
func (*BidRequest_Imp_Native) ProtoMessage()               {}
func (*BidRequest_Imp_Native) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 0, 1} }

func (m *BidRequest_Imp_Native) GetRequest() string {
	if m != nil && m.Request != nil {
		return *m.Request
	}
	return ""
}

func (m *BidRequest_Imp_Native) GetVer() string {
	if m != nil && m.Ver != nil {
		return *m.Ver
	}
	return ""
}

type BidRequest_Site struct {
	Name             *string `protobuf:"bytes,2,opt,name=name" json:"name,omitempty"`
	Page             *string `protobuf:"bytes,7,opt,name=page" json:"page,omitempty"`
	Ref              *string `protobuf:"bytes,9,opt,name=ref" json:"ref,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *BidRequest_Site) Reset()                    { *m = BidRequest_Site{} }
func (m *BidRequest_Site) String() string            { return proto.CompactTextString(m) }
func (*BidRequest_Site) ProtoMessage()               {}
func (*BidRequest_Site) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 1} }

func (m *BidRequest_Site) GetName() string {
	if m != nil && m.Name != nil {
		return *m.Name
	}
	return ""
}

func (m *BidRequest_Site) GetPage() string {
	if m != nil && m.Page != nil {
		return *m.Page
	}
	return ""
}

func (m *BidRequest_Site) GetRef() string {
	if m != nil && m.Ref != nil {
		return *m.Ref
	}
	return ""
}

type BidRequest_App struct {
	Name             *string `protobuf:"bytes,2,opt,name=name" json:"name,omitempty"`
	Ver              *string `protobuf:"bytes,7,opt,name=ver" json:"ver,omitempty"`
	Bundle           *string `protobuf:"bytes,8,opt,name=bundle" json:"bundle,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *BidRequest_App) Reset()                    { *m = BidRequest_App{} }
func (m *BidRequest_App) String() string            { return proto.CompactTextString(m) }
func (*BidRequest_App) ProtoMessage()               {}
func (*BidRequest_App) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 2} }

func (m *BidRequest_App) GetName() string {
	if m != nil && m.Name != nil {
		return *m.Name
	}
	return ""
}

func (m *BidRequest_App) GetVer() string {
	if m != nil && m.Ver != nil {
		return *m.Ver
	}
	return ""
}

func (m *BidRequest_App) GetBundle() string {
	if m != nil && m.Bundle != nil {
		return *m.Bundle
	}
	return ""
}

type BidRequest_Device struct {
	Ua               *string                `protobuf:"bytes,2,opt,name=ua" json:"ua,omitempty"`
	Ip               *string                `protobuf:"bytes,3,opt,name=ip" json:"ip,omitempty"`
	Geo              *BidRequest_Device_Geo `protobuf:"bytes,4,opt,name=geo" json:"geo,omitempty"`
	Didmd5           *string                `protobuf:"bytes,6,opt,name=didmd5" json:"didmd5,omitempty"`
	Dpidmd5          *string                `protobuf:"bytes,8,opt,name=dpidmd5" json:"dpidmd5,omitempty"`
	Ipv6             *string                `protobuf:"bytes,9,opt,name=ipv6" json:"ipv6,omitempty"`
	Carrier          *string                `protobuf:"bytes,10,opt,name=carrier" json:"carrier,omitempty"`
	Language         *string                `protobuf:"bytes,11,opt,name=language" json:"language,omitempty"`
	Make             *string                `protobuf:"bytes,12,opt,name=make" json:"make,omitempty"`
	Model            *string                `protobuf:"bytes,13,opt,name=model" json:"model,omitempty"`
	Os               *string                `protobuf:"bytes,14,opt,name=os" json:"os,omitempty"`
	Osv              *string                `protobuf:"bytes,15,opt,name=osv" json:"osv,omitempty"`
	Hwv              *string                `protobuf:"bytes,24,opt,name=hwv" json:"hwv,omitempty"`
	W                *int32                 `protobuf:"varint,25,opt,name=w" json:"w,omitempty"`
	H                *int32                 `protobuf:"varint,26,opt,name=h" json:"h,omitempty"`
	Ppi              *int32                 `protobuf:"varint,27,opt,name=ppi" json:"ppi,omitempty"`
	Connectiontype   *ConnectionType        `protobuf:"varint,17,opt,name=connectiontype,enum=pbprotobuf.ConnectionType" json:"connectiontype,omitempty"`
	Devicetype       *DeviceType            `protobuf:"varint,18,opt,name=devicetype,enum=pbprotobuf.DeviceType" json:"devicetype,omitempty"`
	Ifa              *string                `protobuf:"bytes,20,opt,name=ifa" json:"ifa,omitempty"`
	Macmd5           *string                `protobuf:"bytes,22,opt,name=macmd5" json:"macmd5,omitempty"`
	Ext              *BidRequest_Device_Ext `protobuf:"bytes,28,opt,name=ext" json:"ext,omitempty"`
	XXX_unrecognized []byte                 `json:"-"`
}

func (m *BidRequest_Device) Reset()                    { *m = BidRequest_Device{} }
func (m *BidRequest_Device) String() string            { return proto.CompactTextString(m) }
func (*BidRequest_Device) ProtoMessage()               {}
func (*BidRequest_Device) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 3} }

func (m *BidRequest_Device) GetUa() string {
	if m != nil && m.Ua != nil {
		return *m.Ua
	}
	return ""
}

func (m *BidRequest_Device) GetIp() string {
	if m != nil && m.Ip != nil {
		return *m.Ip
	}
	return ""
}

func (m *BidRequest_Device) GetGeo() *BidRequest_Device_Geo {
	if m != nil {
		return m.Geo
	}
	return nil
}

func (m *BidRequest_Device) GetDidmd5() string {
	if m != nil && m.Didmd5 != nil {
		return *m.Didmd5
	}
	return ""
}

func (m *BidRequest_Device) GetDpidmd5() string {
	if m != nil && m.Dpidmd5 != nil {
		return *m.Dpidmd5
	}
	return ""
}

func (m *BidRequest_Device) GetIpv6() string {
	if m != nil && m.Ipv6 != nil {
		return *m.Ipv6
	}
	return ""
}

func (m *BidRequest_Device) GetCarrier() string {
	if m != nil && m.Carrier != nil {
		return *m.Carrier
	}
	return ""
}

func (m *BidRequest_Device) GetLanguage() string {
	if m != nil && m.Language != nil {
		return *m.Language
	}
	return ""
}

func (m *BidRequest_Device) GetMake() string {
	if m != nil && m.Make != nil {
		return *m.Make
	}
	return ""
}

func (m *BidRequest_Device) GetModel() string {
	if m != nil && m.Model != nil {
		return *m.Model
	}
	return ""
}

func (m *BidRequest_Device) GetOs() string {
	if m != nil && m.Os != nil {
		return *m.Os
	}
	return ""
}

func (m *BidRequest_Device) GetOsv() string {
	if m != nil && m.Osv != nil {
		return *m.Osv
	}
	return ""
}

func (m *BidRequest_Device) GetHwv() string {
	if m != nil && m.Hwv != nil {
		return *m.Hwv
	}
	return ""
}

func (m *BidRequest_Device) GetW() int32 {
	if m != nil && m.W != nil {
		return *m.W
	}
	return 0
}

func (m *BidRequest_Device) GetH() int32 {
	if m != nil && m.H != nil {
		return *m.H
	}
	return 0
}

func (m *BidRequest_Device) GetPpi() int32 {
	if m != nil && m.Ppi != nil {
		return *m.Ppi
	}
	return 0
}

func (m *BidRequest_Device) GetConnectiontype() ConnectionType {
	if m != nil && m.Connectiontype != nil {
		return *m.Connectiontype
	}
	return ConnectionType_CONNECTION_UNKNOWN
}

func (m *BidRequest_Device) GetDevicetype() DeviceType {
	if m != nil && m.Devicetype != nil {
		return *m.Devicetype
	}
	return DeviceType_HIGHEND_PHONE
}

func (m *BidRequest_Device) GetIfa() string {
	if m != nil && m.Ifa != nil {
		return *m.Ifa
	}
	return ""
}

func (m *BidRequest_Device) GetMacmd5() string {
	if m != nil && m.Macmd5 != nil {
		return *m.Macmd5
	}
	return ""
}

func (m *BidRequest_Device) GetExt() *BidRequest_Device_Ext {
	if m != nil {
		return m.Ext
	}
	return nil
}

type BidRequest_Device_Geo struct {
	Lat              *float64                   `protobuf:"fixed64,1,opt,name=lat" json:"lat,omitempty"`
	Lon              *float64                   `protobuf:"fixed64,2,opt,name=lon" json:"lon,omitempty"`
	Ext              *BidRequest_Device_Geo_Ext `protobuf:"bytes,3,opt,name=ext" json:"ext,omitempty"`
	XXX_unrecognized []byte                     `json:"-"`
}

func (m *BidRequest_Device_Geo) Reset()                    { *m = BidRequest_Device_Geo{} }
func (m *BidRequest_Device_Geo) String() string            { return proto.CompactTextString(m) }
func (*BidRequest_Device_Geo) ProtoMessage()               {}
func (*BidRequest_Device_Geo) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 3, 0} }

func (m *BidRequest_Device_Geo) GetLat() float64 {
	if m != nil && m.Lat != nil {
		return *m.Lat
	}
	return 0
}

func (m *BidRequest_Device_Geo) GetLon() float64 {
	if m != nil && m.Lon != nil {
		return *m.Lon
	}
	return 0
}

func (m *BidRequest_Device_Geo) GetExt() *BidRequest_Device_Geo_Ext {
	if m != nil {
		return m.Ext
	}
	return nil
}

type BidRequest_Device_Geo_Ext struct {
	Timestamp        *int32 `protobuf:"varint,1,opt,name=timestamp" json:"timestamp,omitempty"`
	XXX_unrecognized []byte `json:"-"`
}

func (m *BidRequest_Device_Geo_Ext) Reset()         { *m = BidRequest_Device_Geo_Ext{} }
func (m *BidRequest_Device_Geo_Ext) String() string { return proto.CompactTextString(m) }
func (*BidRequest_Device_Geo_Ext) ProtoMessage()    {}
func (*BidRequest_Device_Geo_Ext) Descriptor() ([]byte, []int) {
	return fileDescriptor0, []int{0, 3, 0, 0}
}

func (m *BidRequest_Device_Geo_Ext) GetTimestamp() int32 {
	if m != nil && m.Timestamp != nil {
		return *m.Timestamp
	}
	return 0
}

// Extensions.
type BidRequest_Device_Ext struct {
	Aid              *string `protobuf:"bytes,1,opt,name=aid" json:"aid,omitempty"`
	Aaid             *string `protobuf:"bytes,2,opt,name=aaid" json:"aaid,omitempty"`
	Mac              *string `protobuf:"bytes,3,opt,name=mac" json:"mac,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *BidRequest_Device_Ext) Reset()                    { *m = BidRequest_Device_Ext{} }
func (m *BidRequest_Device_Ext) String() string            { return proto.CompactTextString(m) }
func (*BidRequest_Device_Ext) ProtoMessage()               {}
func (*BidRequest_Device_Ext) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 3, 1} }

func (m *BidRequest_Device_Ext) GetAid() string {
	if m != nil && m.Aid != nil {
		return *m.Aid
	}
	return ""
}

func (m *BidRequest_Device_Ext) GetAaid() string {
	if m != nil && m.Aaid != nil {
		return *m.Aaid
	}
	return ""
}

func (m *BidRequest_Device_Ext) GetMac() string {
	if m != nil && m.Mac != nil {
		return *m.Mac
	}
	return ""
}

type BidRequest_Pmp struct {
	Deals            []*BidRequest_Pmp_Deal `protobuf:"bytes,2,rep,name=deals" json:"deals,omitempty"`
	XXX_unrecognized []byte                 `json:"-"`
}

func (m *BidRequest_Pmp) Reset()                    { *m = BidRequest_Pmp{} }
func (m *BidRequest_Pmp) String() string            { return proto.CompactTextString(m) }
func (*BidRequest_Pmp) ProtoMessage()               {}
func (*BidRequest_Pmp) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 4} }

func (m *BidRequest_Pmp) GetDeals() []*BidRequest_Pmp_Deal {
	if m != nil {
		return m.Deals
	}
	return nil
}

type BidRequest_Pmp_Deal struct {
	Id               *string `protobuf:"bytes,1,req,name=id" json:"id,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *BidRequest_Pmp_Deal) Reset()                    { *m = BidRequest_Pmp_Deal{} }
func (m *BidRequest_Pmp_Deal) String() string            { return proto.CompactTextString(m) }
func (*BidRequest_Pmp_Deal) ProtoMessage()               {}
func (*BidRequest_Pmp_Deal) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 4, 0} }

func (m *BidRequest_Pmp_Deal) GetId() string {
	if m != nil && m.Id != nil {
		return *m.Id
	}
	return ""
}

type BidResponse struct {
	Id               *string                `protobuf:"bytes,1,req,name=id" json:"id,omitempty"`
	Seatbid          []*BidResponse_SeatBid `protobuf:"bytes,2,rep,name=seatbid" json:"seatbid,omitempty"`
	Bidid            *string                `protobuf:"bytes,3,opt,name=bidid" json:"bidid,omitempty"`
	Cur              *string                `protobuf:"bytes,4,opt,name=cur" json:"cur,omitempty"`
	XXX_unrecognized []byte                 `json:"-"`
}

func (m *BidResponse) Reset()                    { *m = BidResponse{} }
func (m *BidResponse) String() string            { return proto.CompactTextString(m) }
func (*BidResponse) ProtoMessage()               {}
func (*BidResponse) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *BidResponse) GetId() string {
	if m != nil && m.Id != nil {
		return *m.Id
	}
	return ""
}

func (m *BidResponse) GetSeatbid() []*BidResponse_SeatBid {
	if m != nil {
		return m.Seatbid
	}
	return nil
}

func (m *BidResponse) GetBidid() string {
	if m != nil && m.Bidid != nil {
		return *m.Bidid
	}
	return ""
}

func (m *BidResponse) GetCur() string {
	if m != nil && m.Cur != nil {
		return *m.Cur
	}
	return ""
}

type BidResponse_SeatBid struct {
	Bid              []*BidResponse_SeatBid_Bid `protobuf:"bytes,1,rep,name=bid" json:"bid,omitempty"`
	XXX_unrecognized []byte                     `json:"-"`
}

func (m *BidResponse_SeatBid) Reset()                    { *m = BidResponse_SeatBid{} }
func (m *BidResponse_SeatBid) String() string            { return proto.CompactTextString(m) }
func (*BidResponse_SeatBid) ProtoMessage()               {}
func (*BidResponse_SeatBid) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1, 0} }

func (m *BidResponse_SeatBid) GetBid() []*BidResponse_SeatBid_Bid {
	if m != nil {
		return m.Bid
	}
	return nil
}

type BidResponse_SeatBid_Bid struct {
	Id               *string  `protobuf:"bytes,1,req,name=id" json:"id,omitempty"`
	Impid            *string  `protobuf:"bytes,2,req,name=impid" json:"impid,omitempty"`
	Price            *float64 `protobuf:"fixed64,3,req,name=price" json:"price,omitempty"`
	Adid             *string  `protobuf:"bytes,4,opt,name=adid" json:"adid,omitempty"`
	Nurl             *string  `protobuf:"bytes,5,opt,name=nurl" json:"nurl,omitempty"`
	Adm              *string  `protobuf:"bytes,6,req,name=adm" json:"adm,omitempty"`
	Iurl             *string  `protobuf:"bytes,8,opt,name=iurl" json:"iurl,omitempty"`
	W                *int32   `protobuf:"varint,16,opt,name=w" json:"w,omitempty"`
	H                *int32   `protobuf:"varint,17,opt,name=h" json:"h,omitempty"`
	Crid             *string  `protobuf:"bytes,10,opt,name=crid" json:"crid,omitempty"`
	Dealid           *string  `protobuf:"bytes,13,opt,name=dealid" json:"dealid,omitempty"`
	XXX_unrecognized []byte   `json:"-"`
}

func (m *BidResponse_SeatBid_Bid) Reset()                    { *m = BidResponse_SeatBid_Bid{} }
func (m *BidResponse_SeatBid_Bid) String() string            { return proto.CompactTextString(m) }
func (*BidResponse_SeatBid_Bid) ProtoMessage()               {}
func (*BidResponse_SeatBid_Bid) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1, 0, 0} }

func (m *BidResponse_SeatBid_Bid) GetId() string {
	if m != nil && m.Id != nil {
		return *m.Id
	}
	return ""
}

func (m *BidResponse_SeatBid_Bid) GetImpid() string {
	if m != nil && m.Impid != nil {
		return *m.Impid
	}
	return ""
}

func (m *BidResponse_SeatBid_Bid) GetPrice() float64 {
	if m != nil && m.Price != nil {
		return *m.Price
	}
	return 0
}

func (m *BidResponse_SeatBid_Bid) GetAdid() string {
	if m != nil && m.Adid != nil {
		return *m.Adid
	}
	return ""
}

func (m *BidResponse_SeatBid_Bid) GetNurl() string {
	if m != nil && m.Nurl != nil {
		return *m.Nurl
	}
	return ""
}

func (m *BidResponse_SeatBid_Bid) GetAdm() string {
	if m != nil && m.Adm != nil {
		return *m.Adm
	}
	return ""
}

func (m *BidResponse_SeatBid_Bid) GetIurl() string {
	if m != nil && m.Iurl != nil {
		return *m.Iurl
	}
	return ""
}

func (m *BidResponse_SeatBid_Bid) GetW() int32 {
	if m != nil && m.W != nil {
		return *m.W
	}
	return 0
}

func (m *BidResponse_SeatBid_Bid) GetH() int32 {
	if m != nil && m.H != nil {
		return *m.H
	}
	return 0
}

func (m *BidResponse_SeatBid_Bid) GetCrid() string {
	if m != nil && m.Crid != nil {
		return *m.Crid
	}
	return ""
}

func (m *BidResponse_SeatBid_Bid) GetDealid() string {
	if m != nil && m.Dealid != nil {
		return *m.Dealid
	}
	return ""
}

func init() {
	proto.RegisterType((*BidRequest)(nil), "pbprotobuf.BidRequest")
	proto.RegisterType((*BidRequest_Imp)(nil), "pbprotobuf.BidRequest.Imp")
	proto.RegisterType((*BidRequest_Imp_Banner)(nil), "pbprotobuf.BidRequest.Imp.Banner")
	proto.RegisterType((*BidRequest_Imp_Native)(nil), "pbprotobuf.BidRequest.Imp.Native")
	proto.RegisterType((*BidRequest_Site)(nil), "pbprotobuf.BidRequest.Site")
	proto.RegisterType((*BidRequest_App)(nil), "pbprotobuf.BidRequest.App")
	proto.RegisterType((*BidRequest_Device)(nil), "pbprotobuf.BidRequest.Device")
	proto.RegisterType((*BidRequest_Device_Geo)(nil), "pbprotobuf.BidRequest.Device.Geo")
	proto.RegisterType((*BidRequest_Device_Geo_Ext)(nil), "pbprotobuf.BidRequest.Device.Geo.Ext")
	proto.RegisterType((*BidRequest_Device_Ext)(nil), "pbprotobuf.BidRequest.Device.Ext")
	proto.RegisterType((*BidRequest_Pmp)(nil), "pbprotobuf.BidRequest.Pmp")
	proto.RegisterType((*BidRequest_Pmp_Deal)(nil), "pbprotobuf.BidRequest.Pmp.Deal")
	proto.RegisterType((*BidResponse)(nil), "pbprotobuf.BidResponse")
	proto.RegisterType((*BidResponse_SeatBid)(nil), "pbprotobuf.BidResponse.SeatBid")
	proto.RegisterType((*BidResponse_SeatBid_Bid)(nil), "pbprotobuf.BidResponse.SeatBid.Bid")
	proto.RegisterEnum("pbprotobuf.ConnectionType", ConnectionType_name, ConnectionType_value)
	proto.RegisterEnum("pbprotobuf.DeviceType", DeviceType_name, DeviceType_value)
	proto.RegisterEnum("pbprotobuf.AdPosition", AdPosition_name, AdPosition_value)
}

func init() { proto.RegisterFile("protocol.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 1089 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x55, 0xdb, 0x6e, 0xdb, 0x46,
	0x13, 0xfe, 0x29, 0x4a, 0x94, 0x35, 0x4a, 0x14, 0x66, 0x61, 0x18, 0xfb, 0x33, 0x09, 0xaa, 0x24,
	0x28, 0x20, 0x04, 0x85, 0x5a, 0x38, 0x87, 0xa2, 0x05, 0x0a, 0x54, 0x07, 0x46, 0x16, 0x6a, 0x48,
	0xc6, 0x5a, 0x8d, 0xdb, 0x2b, 0x61, 0x25, 0xae, 0xec, 0x45, 0x45, 0x72, 0x4b, 0x52, 0xb2, 0xf3,
	0x04, 0x45, 0xfb, 0x08, 0x05, 0xfa, 0x5a, 0xed, 0xeb, 0x14, 0xb3, 0xbb, 0x96, 0x62, 0xc7, 0x8e,
	0x7b, 0x37, 0x33, 0xfc, 0xe6, 0xb4, 0xf3, 0xcd, 0x10, 0x1a, 0x2a, 0x4b, 0x8b, 0x74, 0x9e, 0x2e,
	0xdb, 0x5a, 0x20, 0xa0, 0x66, 0x5a, 0x98, 0xad, 0x16, 0xcf, 0xfe, 0xbe, 0x07, 0xd0, 0x95, 0x11,
	0x13, 0xbf, 0xae, 0x44, 0x5e, 0x90, 0x06, 0x94, 0x64, 0x44, 0x9d, 0x66, 0xa9, 0x55, 0x63, 0x25,
	0x19, 0x91, 0x2f, 0xc0, 0x95, 0xb1, 0xa2, 0xa5, 0xa6, 0xdb, 0xaa, 0xef, 0x07, 0xed, 0xad, 0x63,
	0x7b, 0xeb, 0xd4, 0x1e, 0xc6, 0x8a, 0x21, 0x8c, 0xbc, 0x06, 0x2f, 0x12, 0x6b, 0x39, 0x17, 0xd4,
	0x6d, 0x3a, 0xad, 0xfa, 0xfe, 0x93, 0x5b, 0x1c, 0xfa, 0x1a, 0xc4, 0x2c, 0x98, 0x7c, 0x09, 0xe5,
	0x5c, 0x16, 0x82, 0x96, 0xb5, 0xd3, 0xa3, 0x5b, 0x9c, 0x8e, 0x65, 0x21, 0x98, 0x06, 0x62, 0x55,
	0x5c, 0x29, 0x5a, 0xd1, 0xf8, 0xdb, 0xaa, 0xea, 0x28, 0xc5, 0x10, 0x16, 0xfc, 0xe1, 0x82, 0x3b,
	0x8c, 0xd5, 0x47, 0xbd, 0x7d, 0x03, 0xde, 0x8c, 0x27, 0x89, 0xc8, 0x68, 0x49, 0x07, 0x7a, 0x7a,
	0x7b, 0x7b, 0xed, 0xae, 0x06, 0x32, 0xeb, 0x40, 0x9e, 0xc0, 0xce, 0x4c, 0x46, 0x8b, 0x65, 0x9a,
	0x66, 0x74, 0xa7, 0xe9, 0xb4, 0x9c, 0x6f, 0x9d, 0xaf, 0xd8, 0xc6, 0x84, 0x91, 0x13, 0x5e, 0xc8,
	0xb5, 0xa0, 0xf7, 0xef, 0x8c, 0x3c, 0xd2, 0x40, 0x66, 0x1d, 0xc8, 0x2e, 0x54, 0x0a, 0x7e, 0x2a,
	0x23, 0x5a, 0x6d, 0x3a, 0xad, 0x1a, 0x33, 0x0a, 0x36, 0xac, 0x62, 0x45, 0xeb, 0x9f, 0x6c, 0xf8,
	0x08, 0xc7, 0xa0, 0x62, 0x15, 0x44, 0xe0, 0x99, 0x7a, 0xc9, 0x3d, 0x70, 0xce, 0xa9, 0xd3, 0x74,
	0x5a, 0x15, 0xe6, 0x9c, 0xa3, 0x76, 0xa6, 0x7b, 0xad, 0x30, 0xe7, 0x8c, 0xb4, 0xc0, 0x55, 0x69,
	0xae, 0x1f, 0xbd, 0xb1, 0xbf, 0xf7, 0x61, 0xcc, 0x4e, 0x74, 0x94, 0xe6, 0xb2, 0x90, 0x69, 0xc2,
	0x10, 0x82, 0x35, 0xc5, 0x32, 0x16, 0x39, 0xad, 0x36, 0x5d, 0xac, 0x49, 0x2b, 0xc1, 0x2b, 0xf0,
	0x4c, 0xed, 0x84, 0x42, 0x35, 0x33, 0x35, 0xd8, 0xd7, 0xbd, 0x54, 0x89, 0x0f, 0xee, 0xda, 0xbe,
	0x6f, 0x8d, 0xa1, 0x18, 0x7c, 0x0f, 0x65, 0x1c, 0x24, 0x21, 0x50, 0x4e, 0x78, 0x2c, 0xec, 0x27,
	0x2d, 0xa3, 0x4d, 0xf1, 0x53, 0x61, 0x5b, 0xd7, 0x32, 0x46, 0xc8, 0xc4, 0x82, 0xd6, 0x4c, 0x84,
	0x4c, 0x2c, 0x82, 0x1e, 0xb8, 0x1d, 0xa5, 0x6e, 0x0c, 0x60, 0xd3, 0x55, 0x37, 0xe9, 0xc8, 0x1e,
	0x78, 0xb3, 0x55, 0x12, 0x2d, 0x85, 0x1e, 0x53, 0x8d, 0x59, 0x2d, 0xf8, 0xdd, 0x03, 0xcf, 0xb0,
	0x10, 0x69, 0xb1, 0xe2, 0x36, 0x4c, 0x69, 0xc5, 0x35, 0x4d, 0x94, 0x26, 0x30, 0xd2, 0x44, 0x91,
	0x97, 0xe0, 0x9e, 0x8a, 0xd4, 0x92, 0xf3, 0xe9, 0x27, 0x19, 0xdd, 0x1e, 0x88, 0x94, 0x21, 0x1a,
	0xf3, 0x46, 0x32, 0x8a, 0xa3, 0xd7, 0xd4, 0x33, 0x79, 0x8d, 0x86, 0x4f, 0x15, 0x29, 0xf3, 0xc1,
	0x14, 0x74, 0xa9, 0x62, 0x3f, 0x52, 0xad, 0xdf, 0xd8, 0x4e, 0xb5, 0x8c, 0xe8, 0x39, 0xcf, 0x32,
	0x29, 0x32, 0x0a, 0x06, 0x6d, 0x55, 0x12, 0xc0, 0xce, 0x92, 0x27, 0xa7, 0x2b, 0x7c, 0xae, 0xba,
	0xfe, 0xb4, 0xd1, 0x31, 0x52, 0xcc, 0x7f, 0x11, 0xf4, 0x9e, 0x89, 0x84, 0xb2, 0x1e, 0x61, 0x1a,
	0x89, 0xa5, 0x26, 0x24, 0x8e, 0x10, 0x15, 0x6c, 0x35, 0xcd, 0x69, 0xc3, 0xb4, 0x9a, 0xe6, 0xf8,
	0x7e, 0x69, 0xbe, 0xa6, 0x0f, 0xcc, 0xfb, 0xa5, 0xf9, 0x1a, 0x2d, 0x67, 0xe7, 0x6b, 0x4a, 0x8d,
	0xe5, 0xec, 0x7c, 0x6d, 0x28, 0xf5, 0xff, 0x2b, 0x94, 0x0a, 0x2e, 0x29, 0xe5, 0x83, 0xab, 0x94,
	0xa4, 0x8f, 0xb4, 0x8e, 0x22, 0xe9, 0x42, 0x63, 0x9e, 0x26, 0x89, 0x98, 0x23, 0x9b, 0x8a, 0xf7,
	0x4a, 0xd0, 0x87, 0x9a, 0x6f, 0x57, 0x38, 0xdc, 0xdb, 0x20, 0x26, 0xef, 0x95, 0x60, 0xd7, 0x3c,
	0xc8, 0x1b, 0x00, 0x73, 0x28, 0xb4, 0x3f, 0xf9, 0x98, 0xaf, 0xe6, 0xf1, 0xb5, 0xef, 0x07, 0x48,
	0xac, 0x46, 0x2e, 0x38, 0xdd, 0x35, 0xb5, 0xcb, 0x05, 0xc7, 0xa9, 0xc4, 0x7c, 0x8e, 0x8f, 0xbf,
	0x67, 0xa6, 0x62, 0x34, 0x1c, 0xb1, 0xb8, 0x28, 0xe8, 0xe3, 0xff, 0x32, 0xe2, 0xf0, 0xa2, 0x60,
	0x88, 0x0e, 0x7e, 0x73, 0xc0, 0x1d, 0x88, 0x14, 0xd3, 0x2c, 0x79, 0xa1, 0xb7, 0xcc, 0x61, 0x28,
	0x6a, 0x4b, 0x9a, 0x68, 0x4a, 0xa1, 0x25, 0x4d, 0xc8, 0xd7, 0x26, 0x81, 0xb9, 0x8a, 0x9f, 0xdf,
	0xc9, 0xa1, 0x6d, 0x92, 0xe7, 0xe0, 0x86, 0x17, 0x05, 0x79, 0x0c, 0xb5, 0x02, 0x97, 0xae, 0xe0,
	0xb1, 0xb2, 0xfb, 0xbc, 0x35, 0x04, 0xdf, 0x19, 0x90, 0x0f, 0x2e, 0xd7, 0x07, 0x4e, 0xf7, 0xcb,
	0x65, 0x84, 0x4c, 0xe0, 0x68, 0xb2, 0x3b, 0x82, 0x32, 0xa2, 0x62, 0x3e, 0xb7, 0xfc, 0x46, 0x31,
	0x98, 0x80, 0x7b, 0xa4, 0x8f, 0x77, 0x25, 0x12, 0x7c, 0x99, 0xdb, 0x63, 0xff, 0xd9, 0xed, 0x57,
	0xa6, 0xdd, 0x17, 0x7c, 0xc9, 0x0c, 0x3a, 0xd8, 0x83, 0x32, 0xaa, 0xd7, 0xaf, 0xeb, 0xb3, 0xbf,
	0x5c, 0xa8, 0x6b, 0xb7, 0x5c, 0xa5, 0x49, 0x2e, 0x6e, 0xb8, 0xbe, 0xd5, 0x5c, 0xf0, 0x62, 0xa6,
	0xcb, 0xbb, 0x39, 0xa1, 0xf1, 0x6c, 0x1f, 0x0b, 0x5e, 0xa0, 0x7e, 0x89, 0x47, 0x32, 0xcf, 0x64,
	0x24, 0x23, 0xdb, 0x84, 0x51, 0xb0, 0xb1, 0xf9, 0x2a, 0xd3, 0x7b, 0x5a, 0x63, 0x28, 0x06, 0x7f,
	0x96, 0xa0, 0x6a, 0x9d, 0xc9, 0x6b, 0x70, 0x67, 0x3a, 0x3f, 0xa6, 0x7a, 0x7e, 0x47, 0x2a, 0x6d,
	0x43, 0x7c, 0xf0, 0x8f, 0x03, 0x2e, 0xba, 0x5f, 0xaf, 0x7e, 0x17, 0x2a, 0x32, 0x56, 0xba, 0x76,
	0x34, 0x19, 0x05, 0xad, 0x2a, 0x33, 0xbf, 0xbf, 0x52, 0xcb, 0x61, 0x46, 0xd1, 0x53, 0xc0, 0x6a,
	0xcb, 0x76, 0x0a, 0x91, 0x99, 0x4c, 0xb2, 0xca, 0x96, 0xfa, 0x17, 0x86, 0xd7, 0x6b, 0x95, 0x2d,
	0xf5, 0xfc, 0xa2, 0x98, 0x7a, 0x3a, 0x22, 0x8a, 0xfa, 0x26, 0x20, 0x6a, 0xc7, 0xde, 0x04, 0x44,
	0xe9, 0xfd, 0xf3, 0xaf, 0xec, 0xdf, 0xc3, 0xcb, 0xfd, 0x23, 0x50, 0x9e, 0x67, 0x32, 0xb2, 0xc7,
	0x42, 0xcb, 0xfa, 0x12, 0x09, 0xbe, 0x94, 0x91, 0x5d, 0x7d, 0xab, 0xbd, 0xf8, 0x19, 0x1a, 0x57,
	0xf7, 0x8e, 0xec, 0x01, 0xe9, 0x8d, 0x47, 0xa3, 0xb0, 0x37, 0x19, 0x8e, 0x47, 0xd3, 0x1f, 0x47,
	0x3f, 0x8c, 0xc6, 0x27, 0x23, 0xff, 0x7f, 0x64, 0x07, 0xca, 0x27, 0xc3, 0xb7, 0x43, 0xbf, 0x44,
	0xea, 0x50, 0xed, 0x85, 0x87, 0x87, 0xd3, 0xfd, 0x81, 0x5f, 0xde, 0x28, 0x2f, 0x07, 0x7e, 0x65,
	0xa3, 0xbc, 0x1a, 0xf8, 0xde, 0x0b, 0x06, 0xb0, 0x5d, 0x49, 0xf2, 0x10, 0xee, 0x1f, 0x0c, 0x07,
	0x07, 0xe1, 0xa8, 0x3f, 0x3d, 0x3a, 0x18, 0x8f, 0x42, 0xbf, 0x4c, 0x00, 0xbc, 0x49, 0xa7, 0x7b,
	0x18, 0x4e, 0xfc, 0x0a, 0xd9, 0x05, 0xdf, 0x66, 0x0d, 0xfb, 0xd3, 0x7e, 0xf8, 0x6e, 0xd8, 0x0b,
	0x7d, 0x8f, 0x3c, 0x80, 0xfa, 0x71, 0x38, 0x99, 0x4e, 0xc6, 0x47, 0xd3, 0xee, 0xf8, 0x27, 0xbf,
	0xfa, 0xa2, 0x03, 0xb0, 0xfd, 0x2d, 0x61, 0xba, 0x6d, 0x7d, 0x04, 0x1a, 0x9d, 0xee, 0xf8, 0x5d,
	0x38, 0x9d, 0x1c, 0x84, 0xd3, 0xb7, 0xe3, 0xc3, 0xbe, 0xef, 0xa0, 0xad, 0x1b, 0x1e, 0x8e, 0x4f,
	0xb6, 0x36, 0xf7, 0xdf, 0x00, 0x00, 0x00, 0xff, 0xff, 0xc7, 0x90, 0x1a, 0x4e, 0x07, 0x09, 0x00,
	0x00,
}
