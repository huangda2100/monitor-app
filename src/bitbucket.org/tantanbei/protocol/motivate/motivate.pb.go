// Code generated by protoc-gen-go. DO NOT EDIT.
// source: motivate.proto

/*
Package motivate is a generated protocol buffer package.

It is generated from these files:
	motivate.proto

It has these top-level messages:
	App
	MotivateItems
*/
package motivate

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// 应用信息
type App struct {
	PackageName string `protobuf:"bytes,1,opt,name=package_name,json=packageName" json:"package_name,omitempty"`
}

func (m *App) Reset()                    { *m = App{} }
func (m *App) String() string            { return proto.CompactTextString(m) }
func (*App) ProtoMessage()               {}
func (*App) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *App) GetPackageName() string {
	if m != nil {
		return m.PackageName
	}
	return ""
}

type MotivateItems struct {
	App []*App `protobuf:"bytes,1,rep,name=app" json:"app,omitempty"`
}

func (m *MotivateItems) Reset()                    { *m = MotivateItems{} }
func (m *MotivateItems) String() string            { return proto.CompactTextString(m) }
func (*MotivateItems) ProtoMessage()               {}
func (*MotivateItems) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *MotivateItems) GetApp() []*App {
	if m != nil {
		return m.App
	}
	return nil
}

func init() {
	proto.RegisterType((*App)(nil), "motivate.App")
	proto.RegisterType((*MotivateItems)(nil), "motivate.MotivateItems")
}

func init() { proto.RegisterFile("motivate.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 122 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0xcb, 0xcd, 0x2f, 0xc9,
	0x2c, 0x4b, 0x2c, 0x49, 0xd5, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0xe2, 0x80, 0xf1, 0x95, 0x34,
	0xb8, 0x98, 0x1d, 0x0b, 0x0a, 0x84, 0x14, 0xb9, 0x78, 0x0a, 0x12, 0x93, 0xb3, 0x13, 0xd3, 0x53,
	0xe3, 0xf3, 0x12, 0x73, 0x53, 0x25, 0x18, 0x15, 0x18, 0x35, 0x38, 0x83, 0xb8, 0xa1, 0x62, 0x7e,
	0x89, 0xb9, 0xa9, 0x4a, 0x06, 0x5c, 0xbc, 0xbe, 0x50, 0x5d, 0x9e, 0x25, 0xa9, 0xb9, 0xc5, 0x42,
	0xf2, 0x5c, 0xcc, 0x89, 0x05, 0x05, 0x12, 0x8c, 0x0a, 0xcc, 0x1a, 0xdc, 0x46, 0xbc, 0x7a, 0x70,
	0x2b, 0x1c, 0x0b, 0x0a, 0x82, 0x40, 0x32, 0x49, 0x6c, 0x60, 0xcb, 0x8c, 0x01, 0x01, 0x00, 0x00,
	0xff, 0xff, 0x00, 0x4b, 0xa0, 0xad, 0x7e, 0x00, 0x00, 0x00,
}
