// Code generated by protoc-gen-go. DO NOT EDIT.
// source: mlmodel.proto

/*
Package mlmodel is a generated protocol buffer package.

It is generated from these files:
	mlmodel.proto

It has these top-level messages:
	RetrievalEmbedding
	AllAdEmbeddings
	FtrlProto
	LRModel
	IRModel
	Dict
	Pack
	ModelServer
	CalibrationConfig
	ProtoPortrait
	ProtoPortraitFile
	Snapshot
	AccessLog
	IntArray
	DnnMultiHot
	FloatArray
*/
package mlmodel

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Strategy int32

const (
	Strategy_StrategyNone        Strategy = 0
	Strategy_StrategyLR          Strategy = 1
	Strategy_StrategyXgboost     Strategy = 2
	Strategy_StrategyLRXgboost   Strategy = 3
	Strategy_StrategyXgboostLR   Strategy = 4
	Strategy_StrategyXgboostFtrl Strategy = 5
	Strategy_StrategyXgboostBS   Strategy = 6
	Strategy_StrategyDNNRawID    Strategy = 7
)

var Strategy_name = map[int32]string{
	0: "StrategyNone",
	1: "StrategyLR",
	2: "StrategyXgboost",
	3: "StrategyLRXgboost",
	4: "StrategyXgboostLR",
	5: "StrategyXgboostFtrl",
	6: "StrategyXgboostBS",
	7: "StrategyDNNRawID",
}
var Strategy_value = map[string]int32{
	"StrategyNone":        0,
	"StrategyLR":          1,
	"StrategyXgboost":     2,
	"StrategyLRXgboost":   3,
	"StrategyXgboostLR":   4,
	"StrategyXgboostFtrl": 5,
	"StrategyXgboostBS":   6,
	"StrategyDNNRawID":    7,
}

func (x Strategy) String() string {
	return proto.EnumName(Strategy_name, int32(x))
}
func (Strategy) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

type RetrievalEmbedding struct {
	Size       int32     `protobuf:"varint,1,opt,name=size" json:"size,omitempty"`
	Embeddings []float64 `protobuf:"fixed64,2,rep,packed,name=embeddings" json:"embeddings,omitempty"`
}

func (m *RetrievalEmbedding) Reset()                    { *m = RetrievalEmbedding{} }
func (m *RetrievalEmbedding) String() string            { return proto.CompactTextString(m) }
func (*RetrievalEmbedding) ProtoMessage()               {}
func (*RetrievalEmbedding) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *RetrievalEmbedding) GetSize() int32 {
	if m != nil {
		return m.Size
	}
	return 0
}

func (m *RetrievalEmbedding) GetEmbeddings() []float64 {
	if m != nil {
		return m.Embeddings
	}
	return nil
}

type AllAdEmbeddings struct {
	Version       string                        `protobuf:"bytes,1,opt,name=version" json:"version,omitempty"`
	AllEmbeddings map[int32]*RetrievalEmbedding `protobuf:"bytes,2,rep,name=allEmbeddings" json:"allEmbeddings,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"bytes,2,opt,name=value"`
}

func (m *AllAdEmbeddings) Reset()                    { *m = AllAdEmbeddings{} }
func (m *AllAdEmbeddings) String() string            { return proto.CompactTextString(m) }
func (*AllAdEmbeddings) ProtoMessage()               {}
func (*AllAdEmbeddings) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *AllAdEmbeddings) GetVersion() string {
	if m != nil {
		return m.Version
	}
	return ""
}

func (m *AllAdEmbeddings) GetAllEmbeddings() map[int32]*RetrievalEmbedding {
	if m != nil {
		return m.AllEmbeddings
	}
	return nil
}

type FtrlProto struct {
	Alpha float64           `protobuf:"fixed64,1,opt,name=alpha" json:"alpha,omitempty"`
	Beta  float64           `protobuf:"fixed64,2,opt,name=beta" json:"beta,omitempty"`
	L1    float64           `protobuf:"fixed64,3,opt,name=L1" json:"L1,omitempty"`
	L2    float64           `protobuf:"fixed64,4,opt,name=L2" json:"L2,omitempty"`
	N     map[int32]float64 `protobuf:"bytes,5,rep,name=n" json:"n,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"fixed64,2,opt,name=value"`
	Z     map[int32]float64 `protobuf:"bytes,6,rep,name=z" json:"z,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"fixed64,2,opt,name=value"`
	W     map[int32]float64 `protobuf:"bytes,7,rep,name=w" json:"w,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"fixed64,2,opt,name=value"`
}

func (m *FtrlProto) Reset()                    { *m = FtrlProto{} }
func (m *FtrlProto) String() string            { return proto.CompactTextString(m) }
func (*FtrlProto) ProtoMessage()               {}
func (*FtrlProto) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *FtrlProto) GetAlpha() float64 {
	if m != nil {
		return m.Alpha
	}
	return 0
}

func (m *FtrlProto) GetBeta() float64 {
	if m != nil {
		return m.Beta
	}
	return 0
}

func (m *FtrlProto) GetL1() float64 {
	if m != nil {
		return m.L1
	}
	return 0
}

func (m *FtrlProto) GetL2() float64 {
	if m != nil {
		return m.L2
	}
	return 0
}

func (m *FtrlProto) GetN() map[int32]float64 {
	if m != nil {
		return m.N
	}
	return nil
}

func (m *FtrlProto) GetZ() map[int32]float64 {
	if m != nil {
		return m.Z
	}
	return nil
}

func (m *FtrlProto) GetW() map[int32]float64 {
	if m != nil {
		return m.W
	}
	return nil
}

type LRModel struct {
	Weights    map[int32]float64 `protobuf:"bytes,1,rep,name=weights" json:"weights,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"fixed64,2,opt,name=value"`
	Parser     string            `protobuf:"bytes,2,opt,name=parser" json:"parser,omitempty"`
	FeatureNum int32             `protobuf:"varint,3,opt,name=feature_num,json=featureNum" json:"feature_num,omitempty"`
	AuPRC      float64           `protobuf:"fixed64,4,opt,name=auPRC" json:"auPRC,omitempty"`
	AuROC      float64           `protobuf:"fixed64,5,opt,name=auROC" json:"auROC,omitempty"`
}

func (m *LRModel) Reset()                    { *m = LRModel{} }
func (m *LRModel) String() string            { return proto.CompactTextString(m) }
func (*LRModel) ProtoMessage()               {}
func (*LRModel) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{3} }

func (m *LRModel) GetWeights() map[int32]float64 {
	if m != nil {
		return m.Weights
	}
	return nil
}

func (m *LRModel) GetParser() string {
	if m != nil {
		return m.Parser
	}
	return ""
}

func (m *LRModel) GetFeatureNum() int32 {
	if m != nil {
		return m.FeatureNum
	}
	return 0
}

func (m *LRModel) GetAuPRC() float64 {
	if m != nil {
		return m.AuPRC
	}
	return 0
}

func (m *LRModel) GetAuROC() float64 {
	if m != nil {
		return m.AuROC
	}
	return 0
}

type IRModel struct {
	Boundaries      []float64 `protobuf:"fixed64,1,rep,packed,name=boundaries" json:"boundaries,omitempty"`
	Predictions     []float64 `protobuf:"fixed64,2,rep,packed,name=predictions" json:"predictions,omitempty"`
	MeanSquareError float64   `protobuf:"fixed64,3,opt,name=mean_square_error,json=meanSquareError" json:"mean_square_error,omitempty"`
}

func (m *IRModel) Reset()                    { *m = IRModel{} }
func (m *IRModel) String() string            { return proto.CompactTextString(m) }
func (*IRModel) ProtoMessage()               {}
func (*IRModel) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{4} }

func (m *IRModel) GetBoundaries() []float64 {
	if m != nil {
		return m.Boundaries
	}
	return nil
}

func (m *IRModel) GetPredictions() []float64 {
	if m != nil {
		return m.Predictions
	}
	return nil
}

func (m *IRModel) GetMeanSquareError() float64 {
	if m != nil {
		return m.MeanSquareError
	}
	return 0
}

type Dict struct {
	Planid       map[int32]int32  `protobuf:"bytes,1,rep,name=planid" json:"planid,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"varint,2,opt,name=value"`
	Unitid       map[int32]int32  `protobuf:"bytes,2,rep,name=unitid" json:"unitid,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"varint,2,opt,name=value"`
	Ideaid       map[int32]int32  `protobuf:"bytes,3,rep,name=ideaid" json:"ideaid,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"varint,2,opt,name=value"`
	Adclass      map[int32]int32  `protobuf:"bytes,4,rep,name=adclass" json:"adclass,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"varint,2,opt,name=value"`
	Slotid       map[int32]int32  `protobuf:"bytes,5,rep,name=slotid" json:"slotid,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"varint,2,opt,name=value"`
	Cityid       map[int32]int32  `protobuf:"bytes,6,rep,name=cityid" json:"cityid,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"varint,2,opt,name=value"`
	Mediaid      map[int32]int32  `protobuf:"bytes,7,rep,name=mediaid" json:"mediaid,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"varint,2,opt,name=value"`
	Appid        map[string]int32 `protobuf:"bytes,8,rep,name=appid" json:"appid,omitempty" protobuf_key:"bytes,1,opt,name=key" protobuf_val:"varint,2,opt,name=value"`
	Advertiserid map[int32]int32  `protobuf:"bytes,9,rep,name=advertiserid" json:"advertiserid,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"varint,2,opt,name=value"`
	Stringid     map[string]int32 `protobuf:"bytes,10,rep,name=stringid" json:"stringid,omitempty" protobuf_key:"bytes,1,opt,name=key" protobuf_val:"varint,2,opt,name=value"`
}

func (m *Dict) Reset()                    { *m = Dict{} }
func (m *Dict) String() string            { return proto.CompactTextString(m) }
func (*Dict) ProtoMessage()               {}
func (*Dict) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{5} }

func (m *Dict) GetPlanid() map[int32]int32 {
	if m != nil {
		return m.Planid
	}
	return nil
}

func (m *Dict) GetUnitid() map[int32]int32 {
	if m != nil {
		return m.Unitid
	}
	return nil
}

func (m *Dict) GetIdeaid() map[int32]int32 {
	if m != nil {
		return m.Ideaid
	}
	return nil
}

func (m *Dict) GetAdclass() map[int32]int32 {
	if m != nil {
		return m.Adclass
	}
	return nil
}

func (m *Dict) GetSlotid() map[int32]int32 {
	if m != nil {
		return m.Slotid
	}
	return nil
}

func (m *Dict) GetCityid() map[int32]int32 {
	if m != nil {
		return m.Cityid
	}
	return nil
}

func (m *Dict) GetMediaid() map[int32]int32 {
	if m != nil {
		return m.Mediaid
	}
	return nil
}

func (m *Dict) GetAppid() map[string]int32 {
	if m != nil {
		return m.Appid
	}
	return nil
}

func (m *Dict) GetAdvertiserid() map[int32]int32 {
	if m != nil {
		return m.Advertiserid
	}
	return nil
}

func (m *Dict) GetStringid() map[string]int32 {
	if m != nil {
		return m.Stringid
	}
	return nil
}

type Pack struct {
	Name           string       `protobuf:"bytes,1,opt,name=name" json:"name,omitempty"`
	Lr             *LRModel     `protobuf:"bytes,2,opt,name=lr" json:"lr,omitempty"`
	Ir             *IRModel     `protobuf:"bytes,3,opt,name=ir" json:"ir,omitempty"`
	Dict           *Dict        `protobuf:"bytes,4,opt,name=dict" json:"dict,omitempty"`
	CreateTime     int64        `protobuf:"varint,5,opt,name=create_time,json=createTime" json:"create_time,omitempty"`
	GbmTreeLimit   uint32       `protobuf:"varint,6,opt,name=gbm_tree_limit,json=gbmTreeLimit" json:"gbm_tree_limit,omitempty"`
	Gbmfile        string       `protobuf:"bytes,7,opt,name=gbmfile" json:"gbmfile,omitempty"`
	Strategy       Strategy     `protobuf:"varint,8,opt,name=strategy,enum=mlmodel.Strategy" json:"strategy,omitempty"`
	GbmTreeDepth   uint32       `protobuf:"varint,9,opt,name=gbm_tree_depth,json=gbmTreeDepth" json:"gbm_tree_depth,omitempty"`
	NegSampleRatio float64      `protobuf:"fixed64,10,opt,name=negSampleRatio" json:"negSampleRatio,omitempty"`
	XgFeatureNum   int64        `protobuf:"varint,11,opt,name=xg_feature_num,json=xgFeatureNum" json:"xg_feature_num,omitempty"`
	OnnxFile       string       `protobuf:"bytes,12,opt,name=onnx_file,json=onnxFile" json:"onnx_file,omitempty"`
	DnnGraphFile   string       `protobuf:"bytes,13,opt,name=dnn_graph_file,json=dnnGraphFile" json:"dnn_graph_file,omitempty"`
	DnnIdxFile     string       `protobuf:"bytes,14,opt,name=dnn_idx_file,json=dnnIdxFile" json:"dnn_idx_file,omitempty"`
	DnnEmbedFile   string       `protobuf:"bytes,15,opt,name=dnn_embed_file,json=dnnEmbedFile" json:"dnn_embed_file,omitempty"`
	DnnEmbedWidth  uint32       `protobuf:"varint,16,opt,name=dnn_embed_width,json=dnnEmbedWidth" json:"dnn_embed_width,omitempty"`
	UseModelServer bool         `protobuf:"varint,17,opt,name=use_model_server,json=useModelServer" json:"use_model_server,omitempty"`
	ModelServer    *ModelServer `protobuf:"bytes,18,opt,name=model_server,json=modelServer" json:"model_server,omitempty"`
}

func (m *Pack) Reset()                    { *m = Pack{} }
func (m *Pack) String() string            { return proto.CompactTextString(m) }
func (*Pack) ProtoMessage()               {}
func (*Pack) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{6} }

func (m *Pack) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Pack) GetLr() *LRModel {
	if m != nil {
		return m.Lr
	}
	return nil
}

func (m *Pack) GetIr() *IRModel {
	if m != nil {
		return m.Ir
	}
	return nil
}

func (m *Pack) GetDict() *Dict {
	if m != nil {
		return m.Dict
	}
	return nil
}

func (m *Pack) GetCreateTime() int64 {
	if m != nil {
		return m.CreateTime
	}
	return 0
}

func (m *Pack) GetGbmTreeLimit() uint32 {
	if m != nil {
		return m.GbmTreeLimit
	}
	return 0
}

func (m *Pack) GetGbmfile() string {
	if m != nil {
		return m.Gbmfile
	}
	return ""
}

func (m *Pack) GetStrategy() Strategy {
	if m != nil {
		return m.Strategy
	}
	return Strategy_StrategyNone
}

func (m *Pack) GetGbmTreeDepth() uint32 {
	if m != nil {
		return m.GbmTreeDepth
	}
	return 0
}

func (m *Pack) GetNegSampleRatio() float64 {
	if m != nil {
		return m.NegSampleRatio
	}
	return 0
}

func (m *Pack) GetXgFeatureNum() int64 {
	if m != nil {
		return m.XgFeatureNum
	}
	return 0
}

func (m *Pack) GetOnnxFile() string {
	if m != nil {
		return m.OnnxFile
	}
	return ""
}

func (m *Pack) GetDnnGraphFile() string {
	if m != nil {
		return m.DnnGraphFile
	}
	return ""
}

func (m *Pack) GetDnnIdxFile() string {
	if m != nil {
		return m.DnnIdxFile
	}
	return ""
}

func (m *Pack) GetDnnEmbedFile() string {
	if m != nil {
		return m.DnnEmbedFile
	}
	return ""
}

func (m *Pack) GetDnnEmbedWidth() uint32 {
	if m != nil {
		return m.DnnEmbedWidth
	}
	return 0
}

func (m *Pack) GetUseModelServer() bool {
	if m != nil {
		return m.UseModelServer
	}
	return false
}

func (m *Pack) GetModelServer() *ModelServer {
	if m != nil {
		return m.ModelServer
	}
	return nil
}

type ModelServer struct {
	Name        string `protobuf:"bytes,1,opt,name=name" json:"name,omitempty"`
	RedisDb     int32  `protobuf:"varint,2,opt,name=redis_db,json=redisDb" json:"redis_db,omitempty"`
	DataExpires int64  `protobuf:"varint,3,opt,name=data_expires,json=dataExpires" json:"data_expires,omitempty"`
}

func (m *ModelServer) Reset()                    { *m = ModelServer{} }
func (m *ModelServer) String() string            { return proto.CompactTextString(m) }
func (*ModelServer) ProtoMessage()               {}
func (*ModelServer) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{7} }

func (m *ModelServer) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *ModelServer) GetRedisDb() int32 {
	if m != nil {
		return m.RedisDb
	}
	return 0
}

func (m *ModelServer) GetDataExpires() int64 {
	if m != nil {
		return m.DataExpires
	}
	return 0
}

type CalibrationConfig struct {
	Name      string   `protobuf:"bytes,1,opt,name=name" json:"name,omitempty"`
	Ir        *IRModel `protobuf:"bytes,2,opt,name=ir" json:"ir,omitempty"`
	Timestamp int64    `protobuf:"varint,3,opt,name=timestamp" json:"timestamp,omitempty"`
}

func (m *CalibrationConfig) Reset()                    { *m = CalibrationConfig{} }
func (m *CalibrationConfig) String() string            { return proto.CompactTextString(m) }
func (*CalibrationConfig) ProtoMessage()               {}
func (*CalibrationConfig) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{8} }

func (m *CalibrationConfig) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *CalibrationConfig) GetIr() *IRModel {
	if m != nil {
		return m.Ir
	}
	return nil
}

func (m *CalibrationConfig) GetTimestamp() int64 {
	if m != nil {
		return m.Timestamp
	}
	return 0
}

type ProtoPortrait struct {
	Id       int64                     `protobuf:"varint,1,opt,name=id" json:"id,omitempty"`
	Sid      string                    `protobuf:"bytes,2,opt,name=sid" json:"sid,omitempty"`
	ValueMap map[int64]float32         `protobuf:"bytes,3,rep,name=valueMap" json:"valueMap,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"fixed32,2,opt,name=value"`
	SubMap   map[string]*ProtoPortrait `protobuf:"bytes,4,rep,name=subMap" json:"subMap,omitempty" protobuf_key:"bytes,1,opt,name=key" protobuf_val:"bytes,2,opt,name=value"`
}

func (m *ProtoPortrait) Reset()                    { *m = ProtoPortrait{} }
func (m *ProtoPortrait) String() string            { return proto.CompactTextString(m) }
func (*ProtoPortrait) ProtoMessage()               {}
func (*ProtoPortrait) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{9} }

func (m *ProtoPortrait) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *ProtoPortrait) GetSid() string {
	if m != nil {
		return m.Sid
	}
	return ""
}

func (m *ProtoPortrait) GetValueMap() map[int64]float32 {
	if m != nil {
		return m.ValueMap
	}
	return nil
}

func (m *ProtoPortrait) GetSubMap() map[string]*ProtoPortrait {
	if m != nil {
		return m.SubMap
	}
	return nil
}

type ProtoPortraitFile struct {
	SubMap map[string]*ProtoPortrait `protobuf:"bytes,1,rep,name=subMap" json:"subMap,omitempty" protobuf_key:"bytes,1,opt,name=key" protobuf_val:"bytes,2,opt,name=value"`
}

func (m *ProtoPortraitFile) Reset()                    { *m = ProtoPortraitFile{} }
func (m *ProtoPortraitFile) String() string            { return proto.CompactTextString(m) }
func (*ProtoPortraitFile) ProtoMessage()               {}
func (*ProtoPortraitFile) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{10} }

func (m *ProtoPortraitFile) GetSubMap() map[string]*ProtoPortrait {
	if m != nil {
		return m.SubMap
	}
	return nil
}

type Snapshot struct {
	RawInt        map[string]int32  `protobuf:"bytes,1,rep,name=raw_int,json=rawInt" json:"raw_int,omitempty" protobuf_key:"bytes,1,opt,name=key" protobuf_val:"varint,2,opt,name=value"`
	RawStr        map[string]string `protobuf:"bytes,2,rep,name=raw_str,json=rawStr" json:"raw_str,omitempty" protobuf_key:"bytes,1,opt,name=key" protobuf_val:"bytes,2,opt,name=value"`
	FeatureVector map[int32]float32 `protobuf:"bytes,3,rep,name=feature_vector,json=featureVector" json:"feature_vector,omitempty" protobuf_key:"varint,1,opt,name=key" protobuf_val:"fixed32,2,opt,name=value"`
	Result        float32           `protobuf:"fixed32,4,opt,name=result" json:"result,omitempty"`
	ReqVersion    string            `protobuf:"bytes,5,opt,name=req_version,json=reqVersion" json:"req_version,omitempty"`
	ModelName     string            `protobuf:"bytes,6,opt,name=model_name,json=modelName" json:"model_name,omitempty"`
	DataType      int32             `protobuf:"varint,7,opt,name=data_type,json=dataType" json:"data_type,omitempty"`
	Strategy      Strategy          `protobuf:"varint,8,opt,name=strategy,enum=mlmodel.Strategy" json:"strategy,omitempty"`
	Calibrated    float32           `protobuf:"fixed32,9,opt,name=calibrated" json:"calibrated,omitempty"`
}

func (m *Snapshot) Reset()                    { *m = Snapshot{} }
func (m *Snapshot) String() string            { return proto.CompactTextString(m) }
func (*Snapshot) ProtoMessage()               {}
func (*Snapshot) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{11} }

func (m *Snapshot) GetRawInt() map[string]int32 {
	if m != nil {
		return m.RawInt
	}
	return nil
}

func (m *Snapshot) GetRawStr() map[string]string {
	if m != nil {
		return m.RawStr
	}
	return nil
}

func (m *Snapshot) GetFeatureVector() map[int32]float32 {
	if m != nil {
		return m.FeatureVector
	}
	return nil
}

func (m *Snapshot) GetResult() float32 {
	if m != nil {
		return m.Result
	}
	return 0
}

func (m *Snapshot) GetReqVersion() string {
	if m != nil {
		return m.ReqVersion
	}
	return ""
}

func (m *Snapshot) GetModelName() string {
	if m != nil {
		return m.ModelName
	}
	return ""
}

func (m *Snapshot) GetDataType() int32 {
	if m != nil {
		return m.DataType
	}
	return 0
}

func (m *Snapshot) GetStrategy() Strategy {
	if m != nil {
		return m.Strategy
	}
	return Strategy_StrategyNone
}

func (m *Snapshot) GetCalibrated() float32 {
	if m != nil {
		return m.Calibrated
	}
	return 0
}

type AccessLog struct {
	Searchid  string      `protobuf:"bytes,1,opt,name=searchid" json:"searchid,omitempty"`
	Snapshots []*Snapshot `protobuf:"bytes,2,rep,name=snapshots" json:"snapshots,omitempty"`
	Timestamp int64       `protobuf:"varint,3,opt,name=timestamp" json:"timestamp,omitempty"`
}

func (m *AccessLog) Reset()                    { *m = AccessLog{} }
func (m *AccessLog) String() string            { return proto.CompactTextString(m) }
func (*AccessLog) ProtoMessage()               {}
func (*AccessLog) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{12} }

func (m *AccessLog) GetSearchid() string {
	if m != nil {
		return m.Searchid
	}
	return ""
}

func (m *AccessLog) GetSnapshots() []*Snapshot {
	if m != nil {
		return m.Snapshots
	}
	return nil
}

func (m *AccessLog) GetTimestamp() int64 {
	if m != nil {
		return m.Timestamp
	}
	return 0
}

type IntArray struct {
	Value []int64 `protobuf:"varint,1,rep,packed,name=value" json:"value,omitempty"`
}

func (m *IntArray) Reset()                    { *m = IntArray{} }
func (m *IntArray) String() string            { return proto.CompactTextString(m) }
func (*IntArray) ProtoMessage()               {}
func (*IntArray) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{13} }

func (m *IntArray) GetValue() []int64 {
	if m != nil {
		return m.Value
	}
	return nil
}

type DnnMultiHot struct {
	Group    []uint32 `protobuf:"varint,1,rep,packed,name=group" json:"group,omitempty"`
	Hashcode []uint64 `protobuf:"varint,2,rep,packed,name=hashcode" json:"hashcode,omitempty"`
}

func (m *DnnMultiHot) Reset()                    { *m = DnnMultiHot{} }
func (m *DnnMultiHot) String() string            { return proto.CompactTextString(m) }
func (*DnnMultiHot) ProtoMessage()               {}
func (*DnnMultiHot) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{14} }

func (m *DnnMultiHot) GetGroup() []uint32 {
	if m != nil {
		return m.Group
	}
	return nil
}

func (m *DnnMultiHot) GetHashcode() []uint64 {
	if m != nil {
		return m.Hashcode
	}
	return nil
}

type FloatArray struct {
	Value []float32 `protobuf:"fixed32,1,rep,packed,name=value" json:"value,omitempty"`
}

func (m *FloatArray) Reset()                    { *m = FloatArray{} }
func (m *FloatArray) String() string            { return proto.CompactTextString(m) }
func (*FloatArray) ProtoMessage()               {}
func (*FloatArray) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{15} }

func (m *FloatArray) GetValue() []float32 {
	if m != nil {
		return m.Value
	}
	return nil
}

func init() {
	proto.RegisterType((*RetrievalEmbedding)(nil), "mlmodel.RetrievalEmbedding")
	proto.RegisterType((*AllAdEmbeddings)(nil), "mlmodel.AllAdEmbeddings")
	proto.RegisterType((*FtrlProto)(nil), "mlmodel.FtrlProto")
	proto.RegisterType((*LRModel)(nil), "mlmodel.LRModel")
	proto.RegisterType((*IRModel)(nil), "mlmodel.IRModel")
	proto.RegisterType((*Dict)(nil), "mlmodel.Dict")
	proto.RegisterType((*Pack)(nil), "mlmodel.Pack")
	proto.RegisterType((*ModelServer)(nil), "mlmodel.ModelServer")
	proto.RegisterType((*CalibrationConfig)(nil), "mlmodel.CalibrationConfig")
	proto.RegisterType((*ProtoPortrait)(nil), "mlmodel.ProtoPortrait")
	proto.RegisterType((*ProtoPortraitFile)(nil), "mlmodel.ProtoPortraitFile")
	proto.RegisterType((*Snapshot)(nil), "mlmodel.Snapshot")
	proto.RegisterType((*AccessLog)(nil), "mlmodel.AccessLog")
	proto.RegisterType((*IntArray)(nil), "mlmodel.IntArray")
	proto.RegisterType((*DnnMultiHot)(nil), "mlmodel.DnnMultiHot")
	proto.RegisterType((*FloatArray)(nil), "mlmodel.FloatArray")
	proto.RegisterEnum("mlmodel.Strategy", Strategy_name, Strategy_value)
}

func init() { proto.RegisterFile("mlmodel.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 1638 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xb4, 0x58, 0x4b, 0x8f, 0x1b, 0xc7,
	0x11, 0xce, 0xf0, 0xcd, 0xe2, 0x63, 0xc9, 0xb6, 0xe2, 0x8c, 0x57, 0xb1, 0x4c, 0x13, 0x82, 0xb2,
	0x70, 0x12, 0x06, 0xda, 0x18, 0x91, 0xad, 0x00, 0xb1, 0x37, 0xbb, 0x52, 0xbc, 0xc8, 0x6a, 0xbd,
	0x6e, 0x2a, 0x52, 0x60, 0x20, 0x18, 0x34, 0x39, 0xad, 0x61, 0x43, 0xf3, 0x52, 0x4f, 0x73, 0x1f,
	0xfa, 0x2b, 0xb9, 0xe7, 0x1f, 0xe4, 0x9c, 0x43, 0x7e, 0x47, 0x4e, 0xf9, 0x07, 0x39, 0xe4, 0x1c,
	0x74, 0x75, 0x0f, 0x39, 0x7c, 0x2c, 0x16, 0x0c, 0xe0, 0x1b, 0xab, 0xea, 0xfb, 0xaa, 0xab, 0xba,
	0xab, 0xaa, 0x7b, 0x08, 0x9d, 0x28, 0x8c, 0x12, 0x9f, 0x87, 0xa3, 0x54, 0x26, 0x2a, 0x21, 0x75,
	0x2b, 0x0e, 0xbf, 0x01, 0x42, 0xb9, 0x92, 0x82, 0x5f, 0xb2, 0xf0, 0x59, 0x34, 0xe1, 0xbe, 0x2f,
	0xe2, 0x80, 0x10, 0xa8, 0x64, 0xe2, 0x3d, 0x77, 0x9d, 0x81, 0x73, 0x50, 0xa5, 0xf8, 0x9b, 0x3c,
	0x00, 0xe0, 0x39, 0x20, 0x73, 0x4b, 0x83, 0xf2, 0x81, 0x43, 0x0b, 0x9a, 0xe1, 0xbf, 0x1c, 0xd8,
	0x3b, 0x0a, 0xc3, 0x23, 0x7f, 0xe1, 0x26, 0x23, 0x2e, 0xd4, 0x2f, 0xb9, 0xcc, 0x44, 0x12, 0xa3,
	0xab, 0x26, 0xcd, 0x45, 0xf2, 0x1d, 0x74, 0x58, 0xb8, 0x5c, 0xd1, 0x38, 0x6c, 0x1d, 0xfe, 0x7c,
	0x94, 0xc7, 0xb9, 0xe6, 0x4a, 0xcb, 0x4b, 0xe9, 0x59, 0xac, 0xe4, 0x0d, 0x5d, 0xf5, 0xb0, 0xff,
	0x17, 0x20, 0x9b, 0x20, 0xd2, 0x83, 0xf2, 0x5b, 0x7e, 0x63, 0x33, 0xd1, 0x3f, 0xc9, 0x63, 0xa8,
	0x5e, 0xb2, 0x70, 0xce, 0xdd, 0xd2, 0xc0, 0x39, 0x68, 0x1d, 0xde, 0x5f, 0x2c, 0xb9, 0xb9, 0x11,
	0xd4, 0x20, 0x9f, 0x96, 0xbe, 0x70, 0x86, 0xff, 0x29, 0x41, 0xf3, 0xb9, 0x92, 0xe1, 0x05, 0x6e,
	0xe0, 0x3d, 0xa8, 0xb2, 0x30, 0x9d, 0x31, 0x74, 0xec, 0x50, 0x23, 0xe8, 0x7d, 0x9b, 0x70, 0xc5,
	0xd0, 0xb3, 0x43, 0xf1, 0x37, 0xe9, 0x42, 0xe9, 0xec, 0xb1, 0x5b, 0x46, 0x4d, 0xe9, 0xec, 0x31,
	0xca, 0x87, 0x6e, 0xc5, 0xca, 0x87, 0xe4, 0x67, 0xe0, 0xc4, 0x6e, 0x15, 0xb3, 0xff, 0x68, 0x11,
	0xca, 0x62, 0xa1, 0xd1, 0xb9, 0xc9, 0xd5, 0x89, 0x35, 0xf0, 0xbd, 0x5b, 0xbb, 0x15, 0xf8, 0xbd,
	0x05, 0xbe, 0xd7, 0xc0, 0x2b, 0xb7, 0x7e, 0x2b, 0xf0, 0xb5, 0x05, 0x5e, 0xed, 0x7f, 0x0e, 0xb5,
	0xf3, 0xdb, 0x76, 0xe9, 0x5e, 0x71, 0x97, 0x9c, 0xc2, 0x46, 0x68, 0xd6, 0xf7, 0xff, 0x17, 0xeb,
	0xf5, 0xce, 0xac, 0xe1, 0xbf, 0x1d, 0xa8, 0x9f, 0xd1, 0x17, 0x3a, 0x03, 0xf2, 0x04, 0xea, 0x57,
	0x5c, 0x04, 0x33, 0x95, 0xb9, 0x0e, 0x26, 0xf7, 0xf1, 0x22, 0x39, 0x0b, 0x19, 0xbd, 0x36, 0x76,
	0x93, 0x60, 0x8e, 0x26, 0x1f, 0x42, 0x2d, 0x65, 0x32, 0xe3, 0x12, 0xfd, 0x37, 0xa9, 0x95, 0xc8,
	0x27, 0xd0, 0x7a, 0xc3, 0x99, 0x9a, 0x4b, 0xee, 0xc5, 0xf3, 0x08, 0x8f, 0xa8, 0x4a, 0xc1, 0xaa,
	0xce, 0xe7, 0x11, 0x1e, 0xf2, 0xfc, 0x82, 0x1e, 0xdb, 0xd3, 0x32, 0x82, 0xd1, 0xd2, 0x6f, 0x8f,
	0xdd, 0x6a, 0xae, 0xa5, 0xdf, 0x1e, 0xef, 0x3f, 0x85, 0x76, 0x71, 0xf5, 0x9d, 0xb2, 0xbc, 0x82,
	0xfa, 0xa9, 0x4d, 0xf2, 0x01, 0xc0, 0x24, 0x99, 0xc7, 0x3e, 0x93, 0x82, 0x9b, 0x3c, 0x1d, 0x5a,
	0xd0, 0x90, 0x01, 0xb4, 0x52, 0xc9, 0x7d, 0x31, 0x55, 0x22, 0x89, 0xf3, 0x36, 0x2c, 0xaa, 0xc8,
	0x67, 0xd0, 0x8f, 0x38, 0x8b, 0xbd, 0xec, 0xdd, 0x9c, 0x49, 0xee, 0x71, 0x29, 0x13, 0x69, 0xcb,
	0x6f, 0x4f, 0x1b, 0xc6, 0xa8, 0x7f, 0xa6, 0xd5, 0xc3, 0xff, 0x36, 0xa0, 0x72, 0x22, 0xa6, 0x8a,
	0x3c, 0x86, 0x5a, 0x1a, 0xb2, 0x58, 0xf8, 0x76, 0x6b, 0x97, 0x75, 0xa3, 0xcd, 0xa3, 0x0b, 0xb4,
	0x99, 0x6d, 0xb5, 0x40, 0x4d, 0x99, 0xc7, 0x42, 0x09, 0xdf, 0xb6, 0xee, 0x1a, 0xe5, 0x4f, 0x68,
	0xb3, 0x14, 0x03, 0xd4, 0x14, 0xe1, 0x73, 0x26, 0x7c, 0xb7, 0xbc, 0x8d, 0x72, 0x8a, 0x36, 0x4b,
	0x31, 0x40, 0xf2, 0x39, 0xd4, 0x99, 0x3f, 0x0d, 0x59, 0x96, 0xb9, 0x15, 0xe4, 0xec, 0xaf, 0x72,
	0x8e, 0x8c, 0xd1, 0x9e, 0xb8, 0x85, 0xea, 0x85, 0xb2, 0x30, 0xd1, 0xb1, 0x55, 0xb7, 0x2d, 0x34,
	0x46, 0x9b, 0x5d, 0xc8, 0x00, 0x35, 0x65, 0x2a, 0xd4, 0x8d, 0xf0, 0x37, 0x5a, 0x0c, 0x29, 0xc7,
	0x68, 0xb3, 0x14, 0x03, 0xd4, 0xb1, 0x45, 0xdc, 0x17, 0x3a, 0x9f, 0xfa, 0xb6, 0xd8, 0x5e, 0x18,
	0xa3, 0x8d, 0xcd, 0x42, 0xc9, 0x08, 0xaa, 0x2c, 0x4d, 0x85, 0xef, 0x36, 0x90, 0xe3, 0xae, 0xe5,
	0xa3, 0x4d, 0x86, 0x61, 0x60, 0xe4, 0x18, 0xda, 0xcc, 0xbf, 0xe4, 0x52, 0x89, 0x8c, 0x4b, 0xe1,
	0xbb, 0x4d, 0xa4, 0x7d, 0xb2, 0xbe, 0x0d, 0x4b, 0x84, 0x61, 0xaf, 0x90, 0xc8, 0x13, 0x68, 0x64,
	0x4a, 0x8a, 0x38, 0x10, 0xbe, 0x0b, 0xe8, 0xe0, 0xfe, 0xda, 0x96, 0x58, 0xab, 0x21, 0x2f, 0xc0,
	0xfb, 0x5f, 0x42, 0xab, 0x70, 0xf8, 0x77, 0x55, 0x75, 0xb5, 0xd8, 0xf1, 0x5f, 0x42, 0xab, 0x50,
	0x04, 0xbb, 0x52, 0x0b, 0xc5, 0xb0, 0x13, 0xf5, 0x29, 0xb4, 0x8b, 0x35, 0xb1, 0xeb, 0xb2, 0x85,
	0xd2, 0xd8, 0x95, 0x5a, 0x28, 0x91, 0x5d, 0x23, 0x2e, 0x56, 0xca, 0x4e, 0xdc, 0x2f, 0x00, 0x96,
	0x15, 0x53, 0x64, 0x36, 0xef, 0x62, 0x7e, 0x05, 0xfd, 0x8d, 0xa2, 0xd9, 0x69, 0xe9, 0xdf, 0x42,
	0x67, 0xa5, 0x68, 0x76, 0x59, 0x7d, 0xf8, 0xd7, 0x2a, 0x54, 0x2e, 0xd8, 0xf4, 0xad, 0xbe, 0x31,
	0x63, 0x16, 0x71, 0xcb, 0xc2, 0xdf, 0x64, 0x00, 0xa5, 0x50, 0xda, 0xdb, 0xb9, 0xb7, 0x3e, 0xe3,
	0x69, 0x29, 0x94, 0x1a, 0x21, 0xcc, 0x50, 0x2b, 0x22, 0x4e, 0x73, 0x84, 0x90, 0xe4, 0x53, 0xa8,
	0xe8, 0x89, 0x88, 0x93, 0xbb, 0x75, 0xd8, 0x59, 0x29, 0x76, 0x8a, 0x26, 0x3d, 0xfe, 0xa7, 0x92,
	0x33, 0xc5, 0x3d, 0x25, 0x22, 0x8e, 0xd3, 0xbc, 0x4c, 0xc1, 0xa8, 0x5e, 0x8a, 0x88, 0x93, 0x87,
	0xd0, 0x0d, 0x26, 0x91, 0xa7, 0x24, 0xe7, 0x5e, 0x28, 0x22, 0xa1, 0xdc, 0xda, 0xc0, 0x39, 0xe8,
	0xd0, 0x76, 0x30, 0x89, 0x5e, 0x4a, 0xce, 0xcf, 0xb4, 0x4e, 0xbf, 0x71, 0x82, 0x49, 0xf4, 0x46,
	0x84, 0xdc, 0xad, 0x9b, 0x37, 0x8e, 0x15, 0xc9, 0x2f, 0xb1, 0xe9, 0x98, 0xe2, 0xc1, 0x8d, 0xdb,
	0x18, 0x38, 0x07, 0xdd, 0xc3, 0xfe, 0x22, 0x8e, 0xb1, 0x35, 0xd0, 0x05, 0x64, 0x65, 0x39, 0x9f,
	0xa7, 0x6a, 0xe6, 0x36, 0x57, 0x96, 0x3b, 0xd1, 0x3a, 0xf2, 0x08, 0xba, 0x31, 0x0f, 0xc6, 0x2c,
	0x4a, 0x43, 0x4e, 0x99, 0x12, 0x89, 0x0b, 0x38, 0xdb, 0xd7, 0xb4, 0xda, 0xdb, 0x75, 0xe0, 0x15,
	0xef, 0xb7, 0x16, 0x26, 0xd8, 0xbe, 0x0e, 0x9e, 0x2f, 0x6f, 0xb8, 0xfb, 0xd0, 0x4c, 0xe2, 0xf8,
	0xda, 0xc3, 0xf0, 0xdb, 0x18, 0x7e, 0x43, 0x2b, 0x9e, 0xeb, 0xf8, 0x1f, 0x42, 0xd7, 0x8f, 0x63,
	0x2f, 0x90, 0x2c, 0x9d, 0x19, 0x44, 0x07, 0x11, 0x6d, 0x3f, 0x8e, 0xff, 0xa0, 0x95, 0x88, 0x1a,
	0x80, 0x96, 0x3d, 0xe1, 0x5b, 0x2f, 0x5d, 0xc4, 0x80, 0x1f, 0xc7, 0xa7, 0xfe, 0x8a, 0x1f, 0x7c,
	0x2b, 0x1a, 0xcc, 0xde, 0xc2, 0x0f, 0x3e, 0xb5, 0x10, 0xf5, 0x08, 0xf6, 0x96, 0xa8, 0x2b, 0xe1,
	0xab, 0x99, 0xdb, 0xc3, 0xfc, 0x3b, 0x39, 0xec, 0xb5, 0x56, 0x92, 0x03, 0xe8, 0xcd, 0x33, 0xee,
	0xe1, 0x36, 0x7a, 0x19, 0x97, 0x97, 0x5c, 0xba, 0xfd, 0x81, 0x73, 0xd0, 0xa0, 0xdd, 0x79, 0xc6,
	0xb1, 0x02, 0xc6, 0xa8, 0x25, 0x4f, 0xa0, 0xbd, 0x82, 0x22, 0x58, 0x0b, 0xf7, 0x16, 0x67, 0x50,
	0xc0, 0xd2, 0x56, 0xb4, 0x14, 0x86, 0x1e, 0xb4, 0x8a, 0x7e, 0xb6, 0xd5, 0xe8, 0x47, 0xd0, 0xd0,
	0x77, 0x6e, 0xe6, 0xf9, 0x13, 0x5b, 0xdd, 0x75, 0x94, 0x4f, 0x26, 0xe4, 0x53, 0x68, 0xfb, 0x4c,
	0x31, 0x8f, 0x5f, 0xa7, 0x42, 0xf2, 0x0c, 0xcb, 0xb4, 0x4c, 0x5b, 0x5a, 0xf7, 0xcc, 0xa8, 0x86,
	0x01, 0xf4, 0x8f, 0x59, 0x28, 0x26, 0x52, 0x1f, 0x55, 0x7c, 0x9c, 0xc4, 0x6f, 0x44, 0x70, 0x5b,
	0x2b, 0x88, 0xcd, 0x56, 0x28, 0x16, 0xfa, 0x4f, 0xa1, 0xa9, 0xcb, 0x37, 0x53, 0x2c, 0x4a, 0xed,
	0x52, 0x4b, 0xc5, 0xf0, 0x1f, 0x25, 0xe8, 0xe0, 0xab, 0xef, 0x22, 0x91, 0x4a, 0x32, 0xa1, 0xf4,
	0xf3, 0x13, 0x6f, 0x79, 0x0d, 0x2c, 0x09, 0x5f, 0x77, 0x6d, 0x86, 0x77, 0x38, 0x76, 0x6d, 0x26,
	0x7c, 0xf2, 0x35, 0x34, 0xb0, 0x51, 0x5f, 0xb0, 0xd4, 0xde, 0xd3, 0x0f, 0x17, 0x2b, 0xaf, 0xf8,
	0x1a, 0xbd, 0xb2, 0x30, 0x7b, 0x69, 0xe4, 0x2c, 0xf2, 0x14, 0x6a, 0xd9, 0x7c, 0xa2, 0xf9, 0xe6,
	0xce, 0x1e, 0xde, 0xc2, 0x1f, 0x23, 0x28, 0xbf, 0x87, 0x51, 0xd0, 0x63, 0x65, 0xc5, 0x6d, 0x71,
	0xac, 0x94, 0xb7, 0x8c, 0x95, 0x52, 0x71, 0x26, 0x7d, 0x07, 0xad, 0x82, 0xcf, 0x2d, 0x13, 0xe9,
	0x17, 0xab, 0x6f, 0xff, 0x0f, 0xb7, 0x07, 0x56, 0x9c, 0x54, 0x7f, 0x73, 0xa0, 0xbf, 0x62, 0xc4,
	0x62, 0xfd, 0xdd, 0x22, 0x43, 0xf3, 0x5e, 0x7a, 0xb4, 0xdd, 0x91, 0xc6, 0x6e, 0xcd, 0xf2, 0x07,
	0x08, 0xf4, 0x9f, 0x15, 0x68, 0x8c, 0x63, 0x96, 0x66, 0xb3, 0x44, 0x91, 0xdf, 0x40, 0x5d, 0xb2,
	0x2b, 0x4f, 0xc4, 0x6a, 0xe3, 0xad, 0x9c, 0x63, 0x46, 0x94, 0x5d, 0x9d, 0xc6, 0xca, 0xc6, 0x25,
	0x51, 0xc8, 0x79, 0x99, 0x92, 0xf6, 0x55, 0xb7, 0x9d, 0x37, 0x56, 0x72, 0xc9, 0x1b, 0x2b, 0x49,
	0xfe, 0x08, 0xdd, 0x7c, 0xd4, 0x5c, 0xf2, 0xa9, 0xc2, 0x17, 0xe7, 0x6a, 0xe5, 0x2c, 0xe8, 0x76,
	0xfa, 0xbc, 0x42, 0x98, 0xfd, 0x90, 0x7b, 0x53, 0xd4, 0xe9, 0xf7, 0xba, 0xe4, 0xd9, 0x3c, 0x34,
	0xd3, 0xbb, 0x44, 0xad, 0xa4, 0x07, 0xb6, 0xe4, 0xef, 0xbc, 0xfc, 0x8b, 0xb2, 0x6a, 0x06, 0x8d,
	0xe4, 0xef, 0x5e, 0xd9, 0x8f, 0xca, 0x8f, 0x01, 0x4c, 0xc3, 0x63, 0x1f, 0xd5, 0xd0, 0xde, 0x44,
	0xcd, 0xb9, 0x6e, 0xa6, 0xfb, 0xd0, 0xc4, 0xc6, 0x54, 0x37, 0xa9, 0x99, 0xd5, 0x55, 0xda, 0xd0,
	0x8a, 0x97, 0x37, 0xe9, 0xce, 0xc3, 0xfa, 0x01, 0xc0, 0xd4, 0x76, 0x30, 0xf7, 0x71, 0x50, 0x97,
	0x68, 0x41, 0xa3, 0xdf, 0x03, 0x85, 0xfd, 0xdd, 0xe9, 0x66, 0x36, 0xd4, 0x7c, 0x8b, 0xef, 0xa2,
	0x36, 0x8b, 0xd4, 0xaf, 0x81, 0x6c, 0x6e, 0xef, 0x5d, 0xb7, 0x7a, 0xb1, 0x83, 0x86, 0x97, 0xd0,
	0x3c, 0x9a, 0x4e, 0x79, 0x96, 0x9d, 0x25, 0x01, 0xd9, 0x87, 0x46, 0xc6, 0x99, 0x9c, 0xce, 0xec,
	0xc4, 0x68, 0xd2, 0x85, 0x4c, 0x7e, 0x05, 0xcd, 0xcc, 0x1e, 0x69, 0xfe, 0xf1, 0xde, 0xdf, 0x38,
	0x6c, 0xba, 0xc4, 0xdc, 0x31, 0xa8, 0x06, 0xd0, 0x38, 0x8d, 0xd5, 0x91, 0x94, 0xac, 0x10, 0x9d,
	0x2e, 0xdd, 0xb2, 0x8d, 0x6e, 0xf8, 0x15, 0xb4, 0x4e, 0xe2, 0xf8, 0xc5, 0x3c, 0x54, 0xe2, 0x9b,
	0x44, 0x69, 0x50, 0x20, 0x93, 0xb9, 0x69, 0xc0, 0x0e, 0x35, 0x82, 0x8e, 0x78, 0xc6, 0xb2, 0xd9,
	0x34, 0xf1, 0x39, 0x06, 0x55, 0xa1, 0x0b, 0x79, 0x38, 0x04, 0x78, 0x1e, 0x26, 0x6c, 0xdb, 0x22,
	0xf9, 0x16, 0x7c, 0xf6, 0x77, 0x07, 0x1a, 0xf9, 0x69, 0x93, 0x1e, 0xb4, 0xf3, 0xdf, 0xe7, 0x49,
	0xcc, 0x7b, 0x3f, 0x22, 0x5d, 0x80, 0x5c, 0x73, 0x46, 0x7b, 0x0e, 0xf9, 0x00, 0xf6, 0x72, 0xf9,
	0xcf, 0xc1, 0x24, 0x49, 0x32, 0xd5, 0x2b, 0x91, 0x1f, 0x43, 0x7f, 0x09, 0xca, 0xd5, 0xe5, 0xa2,
	0xda, 0x2a, 0xcf, 0x68, 0xaf, 0x42, 0x7e, 0x02, 0x1f, 0xac, 0xa9, 0xf5, 0x97, 0x7a, 0xaf, 0xba,
	0x05, 0xff, 0xfb, 0x71, 0xaf, 0x46, 0xee, 0x41, 0x2f, 0x57, 0x9f, 0x9c, 0x9f, 0xeb, 0x1a, 0x3b,
	0xe9, 0xd5, 0x27, 0x35, 0xfc, 0x5b, 0xe7, 0xd7, 0xff, 0x0b, 0x00, 0x00, 0xff, 0xff, 0x5a, 0x1a,
	0xe2, 0x3c, 0xe7, 0x11, 0x00, 0x00,
}
