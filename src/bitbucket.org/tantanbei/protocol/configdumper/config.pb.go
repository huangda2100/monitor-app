// Code generated by protoc-gen-go. DO NOT EDIT.
// source: config.proto

package config

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type AllConfig struct {
	TypeConfig           map[int32]*TypeConfig `protobuf:"bytes,1,rep,name=type_config,json=typeConfig,proto3" json:"type_config,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}              `json:"-"`
	XXX_unrecognized     []byte                `json:"-"`
	XXX_sizecache        int32                 `json:"-"`
}

func (m *AllConfig) Reset()         { *m = AllConfig{} }
func (m *AllConfig) String() string { return proto.CompactTextString(m) }
func (*AllConfig) ProtoMessage()    {}
func (*AllConfig) Descriptor() ([]byte, []int) {
	return fileDescriptor_3eaf2c85e69e9ea4, []int{0}
}

func (m *AllConfig) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AllConfig.Unmarshal(m, b)
}
func (m *AllConfig) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AllConfig.Marshal(b, m, deterministic)
}
func (m *AllConfig) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AllConfig.Merge(m, src)
}
func (m *AllConfig) XXX_Size() int {
	return xxx_messageInfo_AllConfig.Size(m)
}
func (m *AllConfig) XXX_DiscardUnknown() {
	xxx_messageInfo_AllConfig.DiscardUnknown(m)
}

var xxx_messageInfo_AllConfig proto.InternalMessageInfo

func (m *AllConfig) GetTypeConfig() map[int32]*TypeConfig {
	if m != nil {
		return m.TypeConfig
	}
	return nil
}

type TypeConfig struct {
	Config               map[string]string `protobuf:"bytes,1,rep,name=config,proto3" json:"config,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}          `json:"-"`
	XXX_unrecognized     []byte            `json:"-"`
	XXX_sizecache        int32             `json:"-"`
}

func (m *TypeConfig) Reset()         { *m = TypeConfig{} }
func (m *TypeConfig) String() string { return proto.CompactTextString(m) }
func (*TypeConfig) ProtoMessage()    {}
func (*TypeConfig) Descriptor() ([]byte, []int) {
	return fileDescriptor_3eaf2c85e69e9ea4, []int{1}
}

func (m *TypeConfig) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TypeConfig.Unmarshal(m, b)
}
func (m *TypeConfig) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TypeConfig.Marshal(b, m, deterministic)
}
func (m *TypeConfig) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TypeConfig.Merge(m, src)
}
func (m *TypeConfig) XXX_Size() int {
	return xxx_messageInfo_TypeConfig.Size(m)
}
func (m *TypeConfig) XXX_DiscardUnknown() {
	xxx_messageInfo_TypeConfig.DiscardUnknown(m)
}

var xxx_messageInfo_TypeConfig proto.InternalMessageInfo

func (m *TypeConfig) GetConfig() map[string]string {
	if m != nil {
		return m.Config
	}
	return nil
}

func init() {
	proto.RegisterType((*AllConfig)(nil), "config.all_config")
	proto.RegisterMapType((map[int32]*TypeConfig)(nil), "config.all_config.TypeConfigEntry")
	proto.RegisterType((*TypeConfig)(nil), "config.type_config")
	proto.RegisterMapType((map[string]string)(nil), "config.type_config.ConfigEntry")
}

func init() { proto.RegisterFile("config.proto", fileDescriptor_3eaf2c85e69e9ea4) }

var fileDescriptor_3eaf2c85e69e9ea4 = []byte{
	// 185 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x49, 0xce, 0xcf, 0x4b,
	0xcb, 0x4c, 0xd7, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x83, 0xf0, 0x94, 0x96, 0x32, 0x72,
	0x71, 0x25, 0xe6, 0xe4, 0xc4, 0x43, 0xb8, 0x42, 0xce, 0x5c, 0xdc, 0x25, 0x95, 0x05, 0xa9, 0x50,
	0xae, 0x04, 0xa3, 0x02, 0xb3, 0x06, 0xb7, 0x91, 0x92, 0x1e, 0x54, 0x2b, 0x42, 0xa1, 0x5e, 0x48,
	0x65, 0x41, 0xaa, 0x33, 0x98, 0xe9, 0x9a, 0x57, 0x52, 0x54, 0x19, 0xc4, 0x55, 0x02, 0x17, 0x90,
	0x0a, 0xe2, 0xe2, 0x47, 0x93, 0x16, 0x12, 0xe0, 0x62, 0xce, 0x4e, 0xad, 0x94, 0x60, 0x54, 0x60,
	0xd4, 0x60, 0x0d, 0x02, 0x31, 0x85, 0x34, 0xb9, 0x58, 0xcb, 0x12, 0x73, 0x4a, 0x53, 0x25, 0x98,
	0x14, 0x18, 0x35, 0xb8, 0x8d, 0x84, 0x61, 0x76, 0x20, 0x59, 0x1f, 0x04, 0x51, 0x61, 0xc5, 0x64,
	0xc1, 0xa8, 0xd4, 0xc8, 0x88, 0xe2, 0x32, 0x21, 0x73, 0x2e, 0x36, 0x14, 0x37, 0xca, 0x63, 0xd1,
	0xaf, 0x87, 0xec, 0x40, 0xa8, 0x72, 0x29, 0x4b, 0x2e, 0x6e, 0x1c, 0x0e, 0xe3, 0x84, 0x38, 0x4c,
	0x04, 0xd9, 0x61, 0x9c, 0x48, 0x6e, 0x48, 0x62, 0x03, 0x07, 0x9d, 0x31, 0x20, 0x00, 0x00, 0xff,
	0xff, 0x2f, 0x9a, 0x53, 0x92, 0x4a, 0x01, 0x00, 0x00,
}
