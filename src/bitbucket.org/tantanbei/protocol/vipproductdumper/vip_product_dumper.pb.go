// Code generated by protoc-gen-go. DO NOT EDIT.
// source: vip_product_dumper.proto

/*
Package vipproductdumper is a generated protocol buffer package.

It is generated from these files:
	vip_product_dumper.proto

It has these top-level messages:
	VipProduct
	VipProductDump
*/
package vipproductdumper

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type VipProduct struct {
	FirstCategory       int32   `protobuf:"varint,1,opt,name=first_category,json=firstCategory" json:"first_category,omitempty"`
	Title               string  `protobuf:"bytes,2,opt,name=title" json:"title,omitempty"`
	Image               string  `protobuf:"bytes,3,opt,name=image" json:"image,omitempty"`
	Image1              string  `protobuf:"bytes,4,opt,name=image1" json:"image1,omitempty"`
	Image2              string  `protobuf:"bytes,5,opt,name=image2" json:"image2,omitempty"`
	Image3              string  `protobuf:"bytes,6,opt,name=image3" json:"image3,omitempty"`
	Value               int64   `protobuf:"varint,7,opt,name=value" json:"value,omitempty"`
	Price               int64   `protobuf:"varint,8,opt,name=price" json:"price,omitempty"`
	Discount            float32 `protobuf:"fixed32,9,opt,name=discount" json:"discount,omitempty"`
	TargetUrl           string  `protobuf:"bytes,10,opt,name=target_url,json=targetUrl" json:"target_url,omitempty"`
	TargetUrlAndroidApp string  `protobuf:"bytes,11,opt,name=target_url_android_app,json=targetUrlAndroidApp" json:"target_url_android_app,omitempty"`
	TargetUrlIosApp     string  `protobuf:"bytes,12,opt,name=target_url_ios_app,json=targetUrlIosApp" json:"target_url_ios_app,omitempty"`
	BrandName           string  `protobuf:"bytes,13,opt,name=brand_name,json=brandName" json:"brand_name,omitempty"`
	Category            string  `protobuf:"bytes,14,opt,name=category" json:"category,omitempty"`
	OuterId             int64   `protobuf:"varint,15,opt,name=outer_id,json=outerId" json:"outer_id,omitempty"`
	OnlineTime          int64   `protobuf:"varint,16,opt,name=online_time,json=onlineTime" json:"online_time,omitempty"`
	OfflineTime         int64   `protobuf:"varint,17,opt,name=offline_time,json=offlineTime" json:"offline_time,omitempty"`
}

func (m *VipProduct) Reset()                    { *m = VipProduct{} }
func (m *VipProduct) String() string            { return proto.CompactTextString(m) }
func (*VipProduct) ProtoMessage()               {}
func (*VipProduct) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *VipProduct) GetFirstCategory() int32 {
	if m != nil {
		return m.FirstCategory
	}
	return 0
}

func (m *VipProduct) GetTitle() string {
	if m != nil {
		return m.Title
	}
	return ""
}

func (m *VipProduct) GetImage() string {
	if m != nil {
		return m.Image
	}
	return ""
}

func (m *VipProduct) GetImage1() string {
	if m != nil {
		return m.Image1
	}
	return ""
}

func (m *VipProduct) GetImage2() string {
	if m != nil {
		return m.Image2
	}
	return ""
}

func (m *VipProduct) GetImage3() string {
	if m != nil {
		return m.Image3
	}
	return ""
}

func (m *VipProduct) GetValue() int64 {
	if m != nil {
		return m.Value
	}
	return 0
}

func (m *VipProduct) GetPrice() int64 {
	if m != nil {
		return m.Price
	}
	return 0
}

func (m *VipProduct) GetDiscount() float32 {
	if m != nil {
		return m.Discount
	}
	return 0
}

func (m *VipProduct) GetTargetUrl() string {
	if m != nil {
		return m.TargetUrl
	}
	return ""
}

func (m *VipProduct) GetTargetUrlAndroidApp() string {
	if m != nil {
		return m.TargetUrlAndroidApp
	}
	return ""
}

func (m *VipProduct) GetTargetUrlIosApp() string {
	if m != nil {
		return m.TargetUrlIosApp
	}
	return ""
}

func (m *VipProduct) GetBrandName() string {
	if m != nil {
		return m.BrandName
	}
	return ""
}

func (m *VipProduct) GetCategory() string {
	if m != nil {
		return m.Category
	}
	return ""
}

func (m *VipProduct) GetOuterId() int64 {
	if m != nil {
		return m.OuterId
	}
	return 0
}

func (m *VipProduct) GetOnlineTime() int64 {
	if m != nil {
		return m.OnlineTime
	}
	return 0
}

func (m *VipProduct) GetOfflineTime() int64 {
	if m != nil {
		return m.OfflineTime
	}
	return 0
}

type VipProductDump struct {
	Products []*VipProduct `protobuf:"bytes,1,rep,name=products" json:"products,omitempty"`
}

func (m *VipProductDump) Reset()                    { *m = VipProductDump{} }
func (m *VipProductDump) String() string            { return proto.CompactTextString(m) }
func (*VipProductDump) ProtoMessage()               {}
func (*VipProductDump) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *VipProductDump) GetProducts() []*VipProduct {
	if m != nil {
		return m.Products
	}
	return nil
}

func init() {
	proto.RegisterType((*VipProduct)(nil), "vipproductdumper.VipProduct")
	proto.RegisterType((*VipProductDump)(nil), "vipproductdumper.VipProductDump")
}

func init() { proto.RegisterFile("vip_product_dumper.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 379 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x5c, 0x92, 0xc1, 0x8b, 0xd4, 0x30,
	0x18, 0xc5, 0xe9, 0xce, 0xce, 0x6c, 0xe7, 0xeb, 0xee, 0xec, 0x1a, 0x65, 0xf9, 0x14, 0x17, 0xeb,
	0x82, 0x50, 0x10, 0x06, 0x9c, 0x5e, 0xbc, 0x2e, 0x7a, 0x59, 0x0f, 0x22, 0x45, 0xbd, 0x86, 0x4c,
	0x93, 0x19, 0x02, 0x6d, 0x13, 0xd2, 0x74, 0xc0, 0x7f, 0xc1, 0xbf, 0x5a, 0xfa, 0xa5, 0xa4, 0x83,
	0xb7, 0xbe, 0xdf, 0x7b, 0xaf, 0x4d, 0xc3, 0x03, 0x3c, 0x69, 0xcb, 0xad, 0x33, 0x72, 0xa8, 0x3d,
	0x97, 0x43, 0x6b, 0x95, 0xdb, 0x5a, 0x67, 0xbc, 0x61, 0x77, 0x27, 0x6d, 0x27, 0x23, 0xf0, 0xc7,
	0xbf, 0x97, 0x00, 0xbf, 0xb5, 0xfd, 0x11, 0x20, 0xfb, 0x00, 0x9b, 0x83, 0x76, 0xbd, 0xe7, 0xb5,
	0xf0, 0xea, 0x68, 0xdc, 0x1f, 0x4c, 0xf2, 0xa4, 0x58, 0x56, 0x37, 0x44, 0xbf, 0x4c, 0x90, 0xbd,
	0x82, 0xa5, 0xd7, 0xbe, 0x51, 0x78, 0x91, 0x27, 0xc5, 0xba, 0x0a, 0x62, 0xa4, 0xba, 0x15, 0x47,
	0x85, 0x8b, 0x40, 0x49, 0xb0, 0x7b, 0x58, 0xd1, 0xc3, 0x27, 0xbc, 0x24, 0x3c, 0xa9, 0xc8, 0x77,
	0xb8, 0x3c, 0xe3, 0xbb, 0xc8, 0x4b, 0x5c, 0x9d, 0xf1, 0x72, 0x7c, 0xfb, 0x49, 0x34, 0x83, 0xc2,
	0xab, 0x3c, 0x29, 0x16, 0x55, 0x10, 0x23, 0xb5, 0x4e, 0xd7, 0x0a, 0xd3, 0x40, 0x49, 0xb0, 0x37,
	0x90, 0x4a, 0xdd, 0xd7, 0x66, 0xe8, 0x3c, 0xae, 0xf3, 0xa4, 0xb8, 0xa8, 0xa2, 0x66, 0x0f, 0x00,
	0x5e, 0xb8, 0xa3, 0xf2, 0x7c, 0x70, 0x0d, 0x02, 0x7d, 0x63, 0x1d, 0xc8, 0x2f, 0xd7, 0xb0, 0x12,
	0xee, 0x67, 0x9b, 0x8b, 0x4e, 0x3a, 0xa3, 0x25, 0x17, 0xd6, 0x62, 0x46, 0xd1, 0x97, 0x31, 0xfa,
	0x14, 0xbc, 0x27, 0x6b, 0xd9, 0x47, 0x60, 0x67, 0x25, 0x6d, 0x7a, 0x2a, 0x5c, 0x53, 0xe1, 0x36,
	0x16, 0x9e, 0x4d, 0x3f, 0x86, 0x1f, 0x00, 0xf6, 0x4e, 0x74, 0x92, 0x77, 0xa2, 0x55, 0x78, 0x13,
	0x0e, 0x40, 0xe4, 0xbb, 0x68, 0xe9, 0xec, 0xf1, 0xf2, 0x37, 0x64, 0x46, 0xcd, 0x5e, 0x43, 0x6a,
	0x06, 0xaf, 0x1c, 0xd7, 0x12, 0x6f, 0xe9, 0x87, 0xaf, 0x48, 0x3f, 0x4b, 0xf6, 0x0e, 0x32, 0xd3,
	0x35, 0xba, 0x53, 0xdc, 0xeb, 0x56, 0xe1, 0x1d, 0xb9, 0x10, 0xd0, 0x4f, 0xdd, 0x2a, 0xf6, 0x1e,
	0xae, 0xcd, 0xe1, 0x30, 0x27, 0x5e, 0x50, 0x22, 0x9b, 0xd8, 0x18, 0x79, 0xfc, 0x06, 0x9b, 0x79,
	0x0b, 0x5f, 0x87, 0xd6, 0xb2, 0xcf, 0x90, 0x4e, 0x7b, 0xe9, 0x31, 0xc9, 0x17, 0x45, 0xb6, 0x7b,
	0xbb, 0xfd, 0x7f, 0x43, 0xdb, 0xb9, 0x53, 0xc5, 0xf4, 0x7e, 0x45, 0x8b, 0x2b, 0xff, 0x05, 0x00,
	0x00, 0xff, 0xff, 0x51, 0x23, 0x9f, 0x68, 0x8d, 0x02, 0x00, 0x00,
}
