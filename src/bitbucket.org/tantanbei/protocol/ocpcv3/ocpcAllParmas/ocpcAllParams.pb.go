// Code generated by protoc-gen-go. DO NOT EDIT.
// source: ocpcAllParams.proto

/*
Package ocpcAllParams is a generated protocol buffer package.

It is generated from these files:
	ocpcAllParams.proto

It has these top-level messages:
	SingleItem
	OcpcAllParamsList
*/
package ocpcAllParams

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type SingleItem struct {
	Key          string  `protobuf:"bytes,1,opt,name=key" json:"key,omitempty"`
	CvrCalFactor float64 `protobuf:"fixed64,2,opt,name=cvrCalFactor" json:"cvrCalFactor,omitempty"`
	JfbFactor    float64 `protobuf:"fixed64,3,opt,name=jfbFactor" json:"jfbFactor,omitempty"`
	MinCtr       float64 `protobuf:"fixed64,4,opt,name=minCtr" json:"minCtr,omitempty"`
	MinCvr       float64 `protobuf:"fixed64,5,opt,name=minCvr" json:"minCvr,omitempty"`
}

func (m *SingleItem) Reset()                    { *m = SingleItem{} }
func (m *SingleItem) String() string            { return proto.CompactTextString(m) }
func (*SingleItem) ProtoMessage()               {}
func (*SingleItem) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *SingleItem) GetKey() string {
	if m != nil {
		return m.Key
	}
	return ""
}

func (m *SingleItem) GetCvrCalFactor() float64 {
	if m != nil {
		return m.CvrCalFactor
	}
	return 0
}

func (m *SingleItem) GetJfbFactor() float64 {
	if m != nil {
		return m.JfbFactor
	}
	return 0
}

func (m *SingleItem) GetMinCtr() float64 {
	if m != nil {
		return m.MinCtr
	}
	return 0
}

func (m *SingleItem) GetMinCvr() float64 {
	if m != nil {
		return m.MinCvr
	}
	return 0
}

type OcpcAllParamsList struct {
	Records []*SingleItem `protobuf:"bytes,1,rep,name=records" json:"records,omitempty"`
}

func (m *OcpcAllParamsList) Reset()                    { *m = OcpcAllParamsList{} }
func (m *OcpcAllParamsList) String() string            { return proto.CompactTextString(m) }
func (*OcpcAllParamsList) ProtoMessage()               {}
func (*OcpcAllParamsList) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *OcpcAllParamsList) GetRecords() []*SingleItem {
	if m != nil {
		return m.Records
	}
	return nil
}

func init() {
	proto.RegisterType((*SingleItem)(nil), "ocpcAllParams.SingleItem")
	proto.RegisterType((*OcpcAllParamsList)(nil), "ocpcAllParams.OcpcAllParamsList")
}

func init() { proto.RegisterFile("ocpcAllParams.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 188 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0xce, 0x4f, 0x2e, 0x48,
	0x76, 0xcc, 0xc9, 0x09, 0x48, 0x2c, 0x4a, 0xcc, 0x2d, 0xd6, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17,
	0xe2, 0x45, 0x11, 0x54, 0x9a, 0xc0, 0xc8, 0xc5, 0x15, 0x9c, 0x99, 0x97, 0x9e, 0x93, 0xea, 0x59,
	0x92, 0x9a, 0x2b, 0x24, 0xc0, 0xc5, 0x9c, 0x9d, 0x5a, 0x29, 0xc1, 0xa8, 0xc0, 0xa8, 0xc1, 0x19,
	0x04, 0x62, 0x0a, 0x29, 0x71, 0xf1, 0x24, 0x97, 0x15, 0x39, 0x27, 0xe6, 0xb8, 0x25, 0x26, 0x97,
	0xe4, 0x17, 0x49, 0x30, 0x29, 0x30, 0x6a, 0x30, 0x06, 0xa1, 0x88, 0x09, 0xc9, 0x70, 0x71, 0x66,
	0xa5, 0x25, 0x41, 0x15, 0x30, 0x83, 0x15, 0x20, 0x04, 0x84, 0xc4, 0xb8, 0xd8, 0x72, 0x33, 0xf3,
	0x9c, 0x4b, 0x8a, 0x24, 0x58, 0xc0, 0x52, 0x50, 0x1e, 0x4c, 0xbc, 0xac, 0x48, 0x82, 0x15, 0x21,
	0x5e, 0x56, 0xa4, 0xe4, 0xc1, 0x25, 0xe8, 0x8f, 0xec, 0x46, 0x9f, 0xcc, 0xe2, 0x12, 0x21, 0x63,
	0x2e, 0xf6, 0xa2, 0xd4, 0xe4, 0xfc, 0xa2, 0x94, 0x62, 0x09, 0x46, 0x05, 0x66, 0x0d, 0x6e, 0x23,
	0x49, 0x3d, 0x54, 0xdf, 0x21, 0x3c, 0x11, 0x04, 0x53, 0x99, 0xc4, 0x06, 0xf6, 0xb2, 0x31, 0x20,
	0x00, 0x00, 0xff, 0xff, 0x28, 0x72, 0xa1, 0xb6, 0x09, 0x01, 0x00, 0x00,
}
