// Code generated by protoc-gen-go. DO NOT EDIT.
// source: ocpcnovel.proto

/*
Package ocpcnovel is a generated protocol buffer package.

It is generated from these files:
	ocpcnovel.proto

It has these top-level messages:
	SingleUnit
	OcpcNovelList
*/
package ocpcnovel

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type SingleUnit struct {
	Unitid         string  `protobuf:"bytes,1,opt,name=unitid" json:"unitid,omitempty"`
	Kvalue         float64 `protobuf:"fixed64,2,opt,name=kvalue" json:"kvalue,omitempty"`
	CpaHistory     float64 `protobuf:"fixed64,3,opt,name=cpaHistory" json:"cpaHistory,omitempty"`
	Cvr2Cnt        int64   `protobuf:"varint,4,opt,name=cvr2Cnt" json:"cvr2Cnt,omitempty"`
	Cvr3Cnt        int64   `protobuf:"varint,5,opt,name=cvr3Cnt" json:"cvr3Cnt,omitempty"`
	Cpa3History    float64 `protobuf:"fixed64,6,opt,name=cpa3History" json:"cpa3History,omitempty"`
	Conversiongoal int32   `protobuf:"varint,7,opt,name=conversiongoal" json:"conversiongoal,omitempty"`
	Flag           string  `protobuf:"bytes,8,opt,name=flag" json:"flag,omitempty"`
	Postcvr2       float64 `protobuf:"fixed64,9,opt,name=postcvr2" json:"postcvr2,omitempty"`
	Postcvr3       float64 `protobuf:"fixed64,10,opt,name=postcvr3" json:"postcvr3,omitempty"`
	Cvrfactor      float64 `protobuf:"fixed64,11,opt,name=cvrfactor" json:"cvrfactor,omitempty"`
	Avgbid         float64 `protobuf:"fixed64,12,opt,name=avgbid" json:"avgbid,omitempty"`
	Maxbid         float64 `protobuf:"fixed64,13,opt,name=maxbid" json:"maxbid,omitempty"`
}

func (m *SingleUnit) Reset()                    { *m = SingleUnit{} }
func (m *SingleUnit) String() string            { return proto.CompactTextString(m) }
func (*SingleUnit) ProtoMessage()               {}
func (*SingleUnit) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *SingleUnit) GetUnitid() string {
	if m != nil {
		return m.Unitid
	}
	return ""
}

func (m *SingleUnit) GetKvalue() float64 {
	if m != nil {
		return m.Kvalue
	}
	return 0
}

func (m *SingleUnit) GetCpaHistory() float64 {
	if m != nil {
		return m.CpaHistory
	}
	return 0
}

func (m *SingleUnit) GetCvr2Cnt() int64 {
	if m != nil {
		return m.Cvr2Cnt
	}
	return 0
}

func (m *SingleUnit) GetCvr3Cnt() int64 {
	if m != nil {
		return m.Cvr3Cnt
	}
	return 0
}

func (m *SingleUnit) GetCpa3History() float64 {
	if m != nil {
		return m.Cpa3History
	}
	return 0
}

func (m *SingleUnit) GetConversiongoal() int32 {
	if m != nil {
		return m.Conversiongoal
	}
	return 0
}

func (m *SingleUnit) GetFlag() string {
	if m != nil {
		return m.Flag
	}
	return ""
}

func (m *SingleUnit) GetPostcvr2() float64 {
	if m != nil {
		return m.Postcvr2
	}
	return 0
}

func (m *SingleUnit) GetPostcvr3() float64 {
	if m != nil {
		return m.Postcvr3
	}
	return 0
}

func (m *SingleUnit) GetCvrfactor() float64 {
	if m != nil {
		return m.Cvrfactor
	}
	return 0
}

func (m *SingleUnit) GetAvgbid() float64 {
	if m != nil {
		return m.Avgbid
	}
	return 0
}

func (m *SingleUnit) GetMaxbid() float64 {
	if m != nil {
		return m.Maxbid
	}
	return 0
}

type OcpcNovelList struct {
	Adunit []*SingleUnit `protobuf:"bytes,1,rep,name=adunit" json:"adunit,omitempty"`
}

func (m *OcpcNovelList) Reset()                    { *m = OcpcNovelList{} }
func (m *OcpcNovelList) String() string            { return proto.CompactTextString(m) }
func (*OcpcNovelList) ProtoMessage()               {}
func (*OcpcNovelList) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *OcpcNovelList) GetAdunit() []*SingleUnit {
	if m != nil {
		return m.Adunit
	}
	return nil
}

func init() {
	proto.RegisterType((*SingleUnit)(nil), "ocpcnovel.SingleUnit")
	proto.RegisterType((*OcpcNovelList)(nil), "ocpcnovel.OcpcNovelList")
}

func init() { proto.RegisterFile("ocpcnovel.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 294 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x5c, 0x91, 0x4f, 0x4b, 0xc3, 0x30,
	0x18, 0xc6, 0xc9, 0xfe, 0x74, 0xeb, 0x3b, 0xa7, 0x10, 0x50, 0x5e, 0x44, 0x24, 0xec, 0x20, 0xbd,
	0xb8, 0xc3, 0x7a, 0xf7, 0xe2, 0xc5, 0x83, 0x28, 0x54, 0xfc, 0x00, 0x59, 0xd6, 0x95, 0x60, 0x4d,
	0x42, 0x9a, 0x05, 0xfd, 0xde, 0x7e, 0x00, 0x49, 0xda, 0xad, 0xd5, 0x5b, 0x9e, 0xdf, 0x2f, 0xbc,
	0x24, 0xef, 0x03, 0x17, 0x5a, 0x18, 0xa1, 0xb4, 0x2f, 0xeb, 0xb5, 0xb1, 0xda, 0x69, 0x9a, 0x9e,
	0xc0, 0xea, 0x67, 0x04, 0xf0, 0x26, 0x55, 0x55, 0x97, 0xef, 0x4a, 0x3a, 0x7a, 0x05, 0xc9, 0x41,
	0x49, 0x27, 0x77, 0x48, 0x18, 0xc9, 0xd2, 0xa2, 0x4b, 0x81, 0x7f, 0x78, 0x5e, 0x1f, 0x4a, 0x1c,
	0x31, 0x92, 0x91, 0xa2, 0x4b, 0xf4, 0x16, 0x40, 0x18, 0xfe, 0x24, 0x1b, 0xa7, 0xed, 0x37, 0x8e,
	0xa3, 0x1b, 0x10, 0x8a, 0x30, 0x13, 0xde, 0x6e, 0x1e, 0x95, 0xc3, 0x09, 0x23, 0xd9, 0xb8, 0x38,
	0xc6, 0xce, 0xe4, 0xc1, 0x4c, 0x4f, 0x26, 0x44, 0xca, 0x60, 0x21, 0x0c, 0xcf, 0x8f, 0x43, 0x93,
	0x38, 0x74, 0x88, 0xe8, 0x1d, 0x9c, 0x0b, 0xad, 0x7c, 0x69, 0x1b, 0xa9, 0x55, 0xa5, 0x79, 0x8d,
	0x33, 0x46, 0xb2, 0x69, 0xf1, 0x8f, 0x52, 0x0a, 0x93, 0x7d, 0xcd, 0x2b, 0x9c, 0xc7, 0xbf, 0xc4,
	0x33, 0xbd, 0x86, 0xb9, 0xd1, 0x8d, 0x0b, 0xcf, 0xc0, 0x34, 0x8e, 0x3e, 0xe5, 0x81, 0xcb, 0x11,
	0xfe, 0xb8, 0x9c, 0xde, 0x40, 0x2a, 0xbc, 0xdd, 0x73, 0xe1, 0xb4, 0xc5, 0x45, 0x94, 0x3d, 0x08,
	0xfb, 0xe1, 0xbe, 0xda, 0xca, 0x1d, 0x9e, 0xb5, 0xfb, 0x69, 0x53, 0xe0, 0x9f, 0xfc, 0x2b, 0xf0,
	0x65, 0xcb, 0xdb, 0xb4, 0x7a, 0x80, 0xe5, 0xab, 0x30, 0xe2, 0x25, 0x74, 0xf0, 0x2c, 0x1b, 0x47,
	0xef, 0x21, 0xe1, 0xbb, 0xb0, 0x6c, 0x24, 0x6c, 0x9c, 0x2d, 0x36, 0x97, 0xeb, 0xbe, 0xb4, 0xbe,
	0x9f, 0xa2, 0xbb, 0xb4, 0x4d, 0x62, 0x91, 0xf9, 0x6f, 0x00, 0x00, 0x00, 0xff, 0xff, 0xdd, 0x84,
	0x7b, 0x2f, 0xdb, 0x01, 0x00, 0x00,
}
