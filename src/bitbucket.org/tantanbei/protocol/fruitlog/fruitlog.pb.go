// Code generated by protoc-gen-go. DO NOT EDIT.
// source: fruitlog.proto

package fruitlog

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type FruitLog struct {
	UserId               int32    `protobuf:"varint,1,opt,name=userId,proto3" json:"userId,omitempty"`
	ApiName              string   `protobuf:"bytes,2,opt,name=apiName,proto3" json:"apiName,omitempty"`
	CoinIncrease         int32    `protobuf:"varint,3,opt,name=coinIncrease,proto3" json:"coinIncrease,omitempty"`
	WaterIncrease        int32    `protobuf:"varint,4,opt,name=waterIncrease,proto3" json:"waterIncrease,omitempty"`
	SpriteIncrease       int32    `protobuf:"varint,5,opt,name=spriteIncrease,proto3" json:"spriteIncrease,omitempty"`
	Timestamp            int32    `protobuf:"varint,6,opt,name=timestamp,proto3" json:"timestamp,omitempty"`
	Ip                   string   `protobuf:"bytes,7,opt,name=ip,proto3" json:"ip,omitempty"`
	WithdrawAmount       float32  `protobuf:"fixed32,8,opt,name=withdrawAmount,proto3" json:"withdrawAmount,omitempty"`
	CurrentCoin          int32    `protobuf:"varint,9,opt,name=currentCoin,proto3" json:"currentCoin,omitempty"`
	Openid               string   `protobuf:"bytes,10,opt,name=openid,proto3" json:"openid,omitempty"`
	Unionid              string   `protobuf:"bytes,11,opt,name=unionid,proto3" json:"unionid,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *FruitLog) Reset()         { *m = FruitLog{} }
func (m *FruitLog) String() string { return proto.CompactTextString(m) }
func (*FruitLog) ProtoMessage()    {}
func (*FruitLog) Descriptor() ([]byte, []int) {
	return fileDescriptor_2e6c36a75ef6e9ad, []int{0}
}

func (m *FruitLog) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FruitLog.Unmarshal(m, b)
}
func (m *FruitLog) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FruitLog.Marshal(b, m, deterministic)
}
func (m *FruitLog) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FruitLog.Merge(m, src)
}
func (m *FruitLog) XXX_Size() int {
	return xxx_messageInfo_FruitLog.Size(m)
}
func (m *FruitLog) XXX_DiscardUnknown() {
	xxx_messageInfo_FruitLog.DiscardUnknown(m)
}

var xxx_messageInfo_FruitLog proto.InternalMessageInfo

func (m *FruitLog) GetUserId() int32 {
	if m != nil {
		return m.UserId
	}
	return 0
}

func (m *FruitLog) GetApiName() string {
	if m != nil {
		return m.ApiName
	}
	return ""
}

func (m *FruitLog) GetCoinIncrease() int32 {
	if m != nil {
		return m.CoinIncrease
	}
	return 0
}

func (m *FruitLog) GetWaterIncrease() int32 {
	if m != nil {
		return m.WaterIncrease
	}
	return 0
}

func (m *FruitLog) GetSpriteIncrease() int32 {
	if m != nil {
		return m.SpriteIncrease
	}
	return 0
}

func (m *FruitLog) GetTimestamp() int32 {
	if m != nil {
		return m.Timestamp
	}
	return 0
}

func (m *FruitLog) GetIp() string {
	if m != nil {
		return m.Ip
	}
	return ""
}

func (m *FruitLog) GetWithdrawAmount() float32 {
	if m != nil {
		return m.WithdrawAmount
	}
	return 0
}

func (m *FruitLog) GetCurrentCoin() int32 {
	if m != nil {
		return m.CurrentCoin
	}
	return 0
}

func (m *FruitLog) GetOpenid() string {
	if m != nil {
		return m.Openid
	}
	return ""
}

func (m *FruitLog) GetUnionid() string {
	if m != nil {
		return m.Unionid
	}
	return ""
}

func init() {
	proto.RegisterType((*FruitLog)(nil), "fruitlog.FruitLog")
}

func init() { proto.RegisterFile("fruitlog.proto", fileDescriptor_2e6c36a75ef6e9ad) }

var fileDescriptor_2e6c36a75ef6e9ad = []byte{
	// 245 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x5c, 0x90, 0xc1, 0x4a, 0xc4, 0x30,
	0x10, 0x86, 0x69, 0x75, 0xbb, 0xed, 0xac, 0xf6, 0x90, 0x83, 0xe4, 0xe0, 0xa1, 0x2c, 0x22, 0x3d,
	0x79, 0xf1, 0x09, 0x44, 0x10, 0x16, 0xc4, 0x43, 0xdf, 0x20, 0xb6, 0x71, 0x1d, 0xb0, 0x99, 0x30,
	0x9d, 0xd0, 0x57, 0xf5, 0x71, 0x24, 0xb1, 0xbb, 0xba, 0x7b, 0xfc, 0xbe, 0xfc, 0xf9, 0x19, 0x7e,
	0xa8, 0x3f, 0x38, 0xa0, 0x7c, 0xd1, 0xfe, 0xc1, 0x33, 0x09, 0xa9, 0xf2, 0xc0, 0xdb, 0xef, 0x1c,
	0xca, 0x97, 0x08, 0xaf, 0xb4, 0x57, 0x37, 0x50, 0x84, 0xc9, 0xf2, 0x6e, 0xd0, 0x59, 0x93, 0xb5,
	0xab, 0x6e, 0x21, 0xa5, 0x61, 0x6d, 0x3c, 0xbe, 0x99, 0xd1, 0xea, 0xbc, 0xc9, 0xda, 0xaa, 0x3b,
	0xa0, 0xda, 0xc2, 0x55, 0x4f, 0xe8, 0x76, 0xae, 0x67, 0x6b, 0x26, 0xab, 0x2f, 0xd2, 0xbf, 0x13,
	0xa7, 0xee, 0xe0, 0x7a, 0x36, 0x62, 0xf9, 0x18, 0xba, 0x4c, 0xa1, 0x53, 0xa9, 0xee, 0xa1, 0x9e,
	0x3c, 0xa3, 0xd8, 0x63, 0x6c, 0x95, 0x62, 0x67, 0x56, 0xdd, 0x42, 0x25, 0x38, 0xda, 0x49, 0xcc,
	0xe8, 0x75, 0x91, 0x22, 0x7f, 0x42, 0xd5, 0x90, 0xa3, 0xd7, 0xeb, 0x74, 0x64, 0x8e, 0x3e, 0xb6,
	0xce, 0x28, 0x9f, 0x03, 0x9b, 0xf9, 0x69, 0xa4, 0xe0, 0x44, 0x97, 0x4d, 0xd6, 0xe6, 0xdd, 0x99,
	0x55, 0x0d, 0x6c, 0xfa, 0xc0, 0x6c, 0x9d, 0x3c, 0x13, 0x3a, 0x5d, 0xa5, 0xde, 0xff, 0x2a, 0x6e,
	0x43, 0xde, 0x3a, 0x1c, 0x34, 0xa4, 0xf6, 0x85, 0xe2, 0x36, 0xc1, 0x21, 0xc5, 0x87, 0xcd, 0xef,
	0x36, 0x0b, 0xbe, 0x17, 0x69, 0xeb, 0xc7, 0x9f, 0x00, 0x00, 0x00, 0xff, 0xff, 0x01, 0x7d, 0x12,
	0x6b, 0x7d, 0x01, 0x00, 0x00,
}
