// Code generated by protoc-gen-go.
// source: feature.proto
// DO NOT EDIT!

package userprofile

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type FeatureValueType struct {
	FloatValue               float32   `protobuf:"fixed32,1,opt,name=FloatValue" json:"FloatValue,omitempty"`
	StringValue              string    `protobuf:"bytes,2,opt,name=StringValue" json:"StringValue,omitempty"`
	FloatArrayValue          []float32 `protobuf:"fixed32,3,rep,packed,name=FloatArrayValue" json:"FloatArrayValue,omitempty"`
	StringArrayValue         []string  `protobuf:"bytes,4,rep,name=StringArrayValue" json:"StringArrayValue,omitempty"`
	FloatArrayWeight         []float32 `protobuf:"fixed32,5,rep,packed,name=FloatArrayWeight" json:"FloatArrayWeight,omitempty"`
	FloatArrayTime           []float32 `protobuf:"fixed32,6,rep,packed,name=FloatArrayTime" json:"FloatArrayTime,omitempty"`
	FloatArrayRecall         []string  `protobuf:"bytes,7,rep,name=FloatArrayRecall" json:"FloatArrayRecall,omitempty"`
	FloatArrayFrom           []string  `protobuf:"bytes,8,rep,name=FloatArrayFrom" json:"FloatArrayFrom,omitempty"`
	FloatArrayStringExtend_1 []string  `protobuf:"bytes,9,rep,name=FloatArrayStringExtend_1,json=FloatArrayStringExtend1" json:"FloatArrayStringExtend_1,omitempty"`
	FloatArrayFloatExtend_2  []float32 `protobuf:"fixed32,10,rep,packed,name=FloatArrayFloatExtend_2,json=FloatArrayFloatExtend2" json:"FloatArrayFloatExtend_2,omitempty"`
	FloatArrayFloatExtend_3  []float32 `protobuf:"fixed32,11,rep,packed,name=FloatArrayFloatExtend_3,json=FloatArrayFloatExtend3" json:"FloatArrayFloatExtend_3,omitempty"`
}

func (m *FeatureValueType) Reset()                    { *m = FeatureValueType{} }
func (m *FeatureValueType) String() string            { return proto.CompactTextString(m) }
func (*FeatureValueType) ProtoMessage()               {}
func (*FeatureValueType) Descriptor() ([]byte, []int) { return fileDescriptor3, []int{0} }

func (m *FeatureValueType) GetFloatValue() float32 {
	if m != nil {
		return m.FloatValue
	}
	return 0
}

func (m *FeatureValueType) GetStringValue() string {
	if m != nil {
		return m.StringValue
	}
	return ""
}

func (m *FeatureValueType) GetFloatArrayValue() []float32 {
	if m != nil {
		return m.FloatArrayValue
	}
	return nil
}

func (m *FeatureValueType) GetStringArrayValue() []string {
	if m != nil {
		return m.StringArrayValue
	}
	return nil
}

func (m *FeatureValueType) GetFloatArrayWeight() []float32 {
	if m != nil {
		return m.FloatArrayWeight
	}
	return nil
}

func (m *FeatureValueType) GetFloatArrayTime() []float32 {
	if m != nil {
		return m.FloatArrayTime
	}
	return nil
}

func (m *FeatureValueType) GetFloatArrayRecall() []string {
	if m != nil {
		return m.FloatArrayRecall
	}
	return nil
}

func (m *FeatureValueType) GetFloatArrayFrom() []string {
	if m != nil {
		return m.FloatArrayFrom
	}
	return nil
}

func (m *FeatureValueType) GetFloatArrayStringExtend_1() []string {
	if m != nil {
		return m.FloatArrayStringExtend_1
	}
	return nil
}

func (m *FeatureValueType) GetFloatArrayFloatExtend_2() []float32 {
	if m != nil {
		return m.FloatArrayFloatExtend_2
	}
	return nil
}

func (m *FeatureValueType) GetFloatArrayFloatExtend_3() []float32 {
	if m != nil {
		return m.FloatArrayFloatExtend_3
	}
	return nil
}

type FeatureParam struct {
	FeatureName  string            `protobuf:"bytes,1,opt,name=FeatureName" json:"FeatureName,omitempty"`
	FeatureValue *FeatureValueType `protobuf:"bytes,2,opt,name=FeatureValue" json:"FeatureValue,omitempty"`
}

func (m *FeatureParam) Reset()                    { *m = FeatureParam{} }
func (m *FeatureParam) String() string            { return proto.CompactTextString(m) }
func (*FeatureParam) ProtoMessage()               {}
func (*FeatureParam) Descriptor() ([]byte, []int) { return fileDescriptor3, []int{1} }

func (m *FeatureParam) GetFeatureName() string {
	if m != nil {
		return m.FeatureName
	}
	return ""
}

func (m *FeatureParam) GetFeatureValue() *FeatureValueType {
	if m != nil {
		return m.FeatureValue
	}
	return nil
}

type FeatureData struct {
	Uid     string          `protobuf:"bytes,2,opt,name=Uid" json:"Uid,omitempty"`
	DocId   int32           `protobuf:"varint,3,opt,name=DocId" json:"DocId,omitempty"`
	Feature []*FeatureParam `protobuf:"bytes,1,rep,name=Feature" json:"Feature,omitempty"`
}

func (m *FeatureData) Reset()                    { *m = FeatureData{} }
func (m *FeatureData) String() string            { return proto.CompactTextString(m) }
func (*FeatureData) ProtoMessage()               {}
func (*FeatureData) Descriptor() ([]byte, []int) { return fileDescriptor3, []int{2} }

func (m *FeatureData) GetUid() string {
	if m != nil {
		return m.Uid
	}
	return ""
}

func (m *FeatureData) GetDocId() int32 {
	if m != nil {
		return m.DocId
	}
	return 0
}

func (m *FeatureData) GetFeature() []*FeatureParam {
	if m != nil {
		return m.Feature
	}
	return nil
}

type ReqFtrlUserFeature struct {
	GroupId int32    `protobuf:"varint,2,opt,name=GroupId" json:"GroupId,omitempty"`
	Uid     []string `protobuf:"bytes,1,rep,name=Uid" json:"Uid,omitempty"`
	ReqId   string   `protobuf:"bytes,3,opt,name=ReqId" json:"ReqId,omitempty"`
}

func (m *ReqFtrlUserFeature) Reset()                    { *m = ReqFtrlUserFeature{} }
func (m *ReqFtrlUserFeature) String() string            { return proto.CompactTextString(m) }
func (*ReqFtrlUserFeature) ProtoMessage()               {}
func (*ReqFtrlUserFeature) Descriptor() ([]byte, []int) { return fileDescriptor3, []int{3} }

func (m *ReqFtrlUserFeature) GetGroupId() int32 {
	if m != nil {
		return m.GroupId
	}
	return 0
}

func (m *ReqFtrlUserFeature) GetUid() []string {
	if m != nil {
		return m.Uid
	}
	return nil
}

func (m *ReqFtrlUserFeature) GetReqId() string {
	if m != nil {
		return m.ReqId
	}
	return ""
}

type RespFtrlUserFeature struct {
	Errcode   int32          `protobuf:"varint,1,opt,name=Errcode" json:"Errcode,omitempty"`
	Errdesc   string         `protobuf:"bytes,2,opt,name=Errdesc" json:"Errdesc,omitempty"`
	Features  []*FeatureData `protobuf:"bytes,3,rep,name=Features" json:"Features,omitempty"`
	StartTime int64          `protobuf:"varint,4,opt,name=StartTime" json:"StartTime,omitempty"`
	EndTime   int64          `protobuf:"varint,5,opt,name=EndTime" json:"EndTime,omitempty"`
}

func (m *RespFtrlUserFeature) Reset()                    { *m = RespFtrlUserFeature{} }
func (m *RespFtrlUserFeature) String() string            { return proto.CompactTextString(m) }
func (*RespFtrlUserFeature) ProtoMessage()               {}
func (*RespFtrlUserFeature) Descriptor() ([]byte, []int) { return fileDescriptor3, []int{4} }

func (m *RespFtrlUserFeature) GetErrcode() int32 {
	if m != nil {
		return m.Errcode
	}
	return 0
}

func (m *RespFtrlUserFeature) GetErrdesc() string {
	if m != nil {
		return m.Errdesc
	}
	return ""
}

func (m *RespFtrlUserFeature) GetFeatures() []*FeatureData {
	if m != nil {
		return m.Features
	}
	return nil
}

func (m *RespFtrlUserFeature) GetStartTime() int64 {
	if m != nil {
		return m.StartTime
	}
	return 0
}

func (m *RespFtrlUserFeature) GetEndTime() int64 {
	if m != nil {
		return m.EndTime
	}
	return 0
}

type ReqFtrlDocFeature struct {
	GroupId int32   `protobuf:"varint,2,opt,name=GroupId" json:"GroupId,omitempty"`
	DocId   []int32 `protobuf:"varint,1,rep,packed,name=DocId" json:"DocId,omitempty"`
	ReqId   string  `protobuf:"bytes,3,opt,name=ReqId" json:"ReqId,omitempty"`
}

func (m *ReqFtrlDocFeature) Reset()                    { *m = ReqFtrlDocFeature{} }
func (m *ReqFtrlDocFeature) String() string            { return proto.CompactTextString(m) }
func (*ReqFtrlDocFeature) ProtoMessage()               {}
func (*ReqFtrlDocFeature) Descriptor() ([]byte, []int) { return fileDescriptor3, []int{5} }

func (m *ReqFtrlDocFeature) GetGroupId() int32 {
	if m != nil {
		return m.GroupId
	}
	return 0
}

func (m *ReqFtrlDocFeature) GetDocId() []int32 {
	if m != nil {
		return m.DocId
	}
	return nil
}

func (m *ReqFtrlDocFeature) GetReqId() string {
	if m != nil {
		return m.ReqId
	}
	return ""
}

type RespFtrlDocFeature struct {
	Errcode   int32          `protobuf:"varint,1,opt,name=Errcode" json:"Errcode,omitempty"`
	Errdesc   string         `protobuf:"bytes,2,opt,name=Errdesc" json:"Errdesc,omitempty"`
	Features  []*FeatureData `protobuf:"bytes,3,rep,name=Features" json:"Features,omitempty"`
	StartTime int64          `protobuf:"varint,4,opt,name=StartTime" json:"StartTime,omitempty"`
	EndTime   int64          `protobuf:"varint,5,opt,name=EndTime" json:"EndTime,omitempty"`
}

func (m *RespFtrlDocFeature) Reset()                    { *m = RespFtrlDocFeature{} }
func (m *RespFtrlDocFeature) String() string            { return proto.CompactTextString(m) }
func (*RespFtrlDocFeature) ProtoMessage()               {}
func (*RespFtrlDocFeature) Descriptor() ([]byte, []int) { return fileDescriptor3, []int{6} }

func (m *RespFtrlDocFeature) GetErrcode() int32 {
	if m != nil {
		return m.Errcode
	}
	return 0
}

func (m *RespFtrlDocFeature) GetErrdesc() string {
	if m != nil {
		return m.Errdesc
	}
	return ""
}

func (m *RespFtrlDocFeature) GetFeatures() []*FeatureData {
	if m != nil {
		return m.Features
	}
	return nil
}

func (m *RespFtrlDocFeature) GetStartTime() int64 {
	if m != nil {
		return m.StartTime
	}
	return 0
}

func (m *RespFtrlDocFeature) GetEndTime() int64 {
	if m != nil {
		return m.EndTime
	}
	return 0
}

type RespFtrlFeature struct {
	Errcode   int32          `protobuf:"varint,1,opt,name=Errcode" json:"Errcode,omitempty"`
	Errdesc   string         `protobuf:"bytes,2,opt,name=Errdesc" json:"Errdesc,omitempty"`
	Features  []*FeatureData `protobuf:"bytes,3,rep,name=Features" json:"Features,omitempty"`
	StartTime int64          `protobuf:"varint,4,opt,name=StartTime" json:"StartTime,omitempty"`
	EndTime   int64          `protobuf:"varint,5,opt,name=EndTime" json:"EndTime,omitempty"`
}

func (m *RespFtrlFeature) Reset()                    { *m = RespFtrlFeature{} }
func (m *RespFtrlFeature) String() string            { return proto.CompactTextString(m) }
func (*RespFtrlFeature) ProtoMessage()               {}
func (*RespFtrlFeature) Descriptor() ([]byte, []int) { return fileDescriptor3, []int{7} }

func (m *RespFtrlFeature) GetErrcode() int32 {
	if m != nil {
		return m.Errcode
	}
	return 0
}

func (m *RespFtrlFeature) GetErrdesc() string {
	if m != nil {
		return m.Errdesc
	}
	return ""
}

func (m *RespFtrlFeature) GetFeatures() []*FeatureData {
	if m != nil {
		return m.Features
	}
	return nil
}

func (m *RespFtrlFeature) GetStartTime() int64 {
	if m != nil {
		return m.StartTime
	}
	return 0
}

func (m *RespFtrlFeature) GetEndTime() int64 {
	if m != nil {
		return m.EndTime
	}
	return 0
}

type ReportFeatureData struct {
	Uid     string        `protobuf:"bytes,2,opt,name=Uid" json:"Uid,omitempty"`
	DocId   int32         `protobuf:"varint,3,opt,name=DocId" json:"DocId,omitempty"`
	Feature *FeatureParam `protobuf:"bytes,1,opt,name=Feature" json:"Feature,omitempty"`
}

func (m *ReportFeatureData) Reset()                    { *m = ReportFeatureData{} }
func (m *ReportFeatureData) String() string            { return proto.CompactTextString(m) }
func (*ReportFeatureData) ProtoMessage()               {}
func (*ReportFeatureData) Descriptor() ([]byte, []int) { return fileDescriptor3, []int{8} }

func (m *ReportFeatureData) GetUid() string {
	if m != nil {
		return m.Uid
	}
	return ""
}

func (m *ReportFeatureData) GetDocId() int32 {
	if m != nil {
		return m.DocId
	}
	return 0
}

func (m *ReportFeatureData) GetFeature() *FeatureParam {
	if m != nil {
		return m.Feature
	}
	return nil
}

func init() {
	proto.RegisterType((*FeatureValueType)(nil), "userprofile.FeatureValueType")
	proto.RegisterType((*FeatureParam)(nil), "userprofile.FeatureParam")
	proto.RegisterType((*FeatureData)(nil), "userprofile.FeatureData")
	proto.RegisterType((*ReqFtrlUserFeature)(nil), "userprofile.ReqFtrlUserFeature")
	proto.RegisterType((*RespFtrlUserFeature)(nil), "userprofile.RespFtrlUserFeature")
	proto.RegisterType((*ReqFtrlDocFeature)(nil), "userprofile.ReqFtrlDocFeature")
	proto.RegisterType((*RespFtrlDocFeature)(nil), "userprofile.RespFtrlDocFeature")
	proto.RegisterType((*RespFtrlFeature)(nil), "userprofile.RespFtrlFeature")
	proto.RegisterType((*ReportFeatureData)(nil), "userprofile.ReportFeatureData")
}

func init() { proto.RegisterFile("feature.proto", fileDescriptor3) }

var fileDescriptor3 = []byte{
	// 518 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xd4, 0x55, 0x5d, 0x8b, 0xd4, 0x30,
	0x14, 0x25, 0xd3, 0xed, 0xce, 0xf6, 0x56, 0xdd, 0x31, 0x8a, 0x46, 0x50, 0x29, 0x7d, 0x90, 0xe2,
	0xc3, 0xc0, 0x76, 0x04, 0xf1, 0x71, 0x61, 0x76, 0x64, 0x5f, 0x44, 0xb2, 0x1f, 0xe2, 0x93, 0xc4,
	0x36, 0xbb, 0x56, 0x3a, 0x93, 0x6e, 0x9a, 0x01, 0xf7, 0x77, 0x09, 0xfa, 0xf3, 0x94, 0x26, 0xe9,
	0xc7, 0x74, 0xea, 0x07, 0xf8, 0xe2, 0xbe, 0xf5, 0x9e, 0x7b, 0x4e, 0xee, 0xcd, 0xb9, 0x37, 0x14,
	0x6e, 0x5f, 0x70, 0xa6, 0xd6, 0x92, 0x4f, 0x0b, 0x29, 0x94, 0xc0, 0xfe, 0xba, 0xe4, 0xb2, 0x90,
	0xe2, 0x22, 0xcb, 0x79, 0xf8, 0xc3, 0x81, 0xc9, 0xc2, 0xa4, 0xcf, 0x59, 0xbe, 0xe6, 0xa7, 0xd7,
	0x05, 0xc7, 0x4f, 0x01, 0x16, 0xb9, 0x60, 0x4a, 0x23, 0x04, 0x05, 0x28, 0x1a, 0xd1, 0x0e, 0x82,
	0x03, 0xf0, 0x4f, 0x94, 0xcc, 0x56, 0x97, 0x86, 0x30, 0x0a, 0x50, 0xe4, 0xd1, 0x2e, 0x84, 0x23,
	0xd8, 0xd7, 0xfc, 0x43, 0x29, 0xd9, 0xb5, 0x61, 0x39, 0x81, 0x13, 0x8d, 0x68, 0x1f, 0xc6, 0xcf,
	0x61, 0x62, 0x84, 0x1d, 0xea, 0x4e, 0xe0, 0x44, 0x1e, 0xdd, 0xc2, 0x2b, 0x6e, 0x2b, 0x7f, 0xc7,
	0xb3, 0xcb, 0x4f, 0x8a, 0xb8, 0xfa, 0xd8, 0x2d, 0x1c, 0x3f, 0x83, 0x3b, 0x2d, 0x76, 0x9a, 0x2d,
	0x39, 0xd9, 0xd5, 0xcc, 0x1e, 0xba, 0x79, 0x26, 0xe5, 0x09, 0xcb, 0x73, 0x32, 0x36, 0xf5, 0xfb,
	0xf8, 0xe6, 0x99, 0x0b, 0x29, 0x96, 0x64, 0x4f, 0x33, 0x7b, 0x28, 0x7e, 0x05, 0xa4, 0x45, 0xcc,
	0x2d, 0x8e, 0xbe, 0x28, 0xbe, 0x4a, 0x3f, 0x1c, 0x10, 0x4f, 0x2b, 0x1e, 0x0e, 0xe7, 0x0f, 0xf0,
	0x4b, 0xe8, 0xa4, 0xf4, 0x97, 0x55, 0xc6, 0x04, 0x74, 0xff, 0x0f, 0x06, 0xd3, 0xf1, 0xaf, 0x85,
	0x33, 0xe2, 0xff, 0x46, 0x38, 0x0b, 0x4b, 0xb8, 0x65, 0x17, 0xe0, 0x2d, 0x93, 0x6c, 0x59, 0x0d,
	0xd7, 0xc6, 0x6f, 0xd8, 0xd2, 0x4c, 0xdf, 0xa3, 0x5d, 0x08, 0x1f, 0x36, 0x8a, 0x76, 0xfe, 0x7e,
	0xfc, 0x64, 0xda, 0xd9, 0xab, 0x69, 0x7f, 0xa7, 0xe8, 0x86, 0x24, 0xfc, 0xdc, 0x14, 0x99, 0x33,
	0xc5, 0xf0, 0x04, 0x9c, 0xb3, 0x2c, 0xb5, 0x8b, 0x54, 0x7d, 0xe2, 0xfb, 0xe0, 0xce, 0x45, 0x72,
	0x9c, 0x12, 0x27, 0x40, 0x91, 0x4b, 0x4d, 0x80, 0x67, 0x30, 0xb6, 0x32, 0x82, 0x02, 0x27, 0xf2,
	0xe3, 0x47, 0x43, 0x45, 0xf5, 0x3d, 0x68, 0xcd, 0x0c, 0xcf, 0x01, 0x53, 0x7e, 0xb5, 0x50, 0x32,
	0x3f, 0x2b, 0xb9, 0xb4, 0x28, 0x26, 0x30, 0x7e, 0x2d, 0xc5, 0xba, 0x38, 0x36, 0x65, 0x5d, 0x5a,
	0x87, 0x75, 0x33, 0x48, 0x0f, 0xaa, 0x6e, 0x86, 0xf2, 0x2b, 0xdb, 0x8c, 0x47, 0x4d, 0x10, 0x7e,
	0x47, 0x70, 0x8f, 0xf2, 0xb2, 0x18, 0x38, 0xf9, 0x48, 0xca, 0x44, 0xa4, 0xc6, 0x3c, 0x97, 0xd6,
	0xa1, 0xcd, 0xa4, 0xbc, 0x4c, 0xec, 0x55, 0xeb, 0x10, 0xbf, 0x80, 0x3d, 0x2b, 0x2f, 0xf5, 0x43,
	0xf1, 0x63, 0x32, 0x74, 0xb3, 0xca, 0x2c, 0xda, 0x30, 0xf1, 0x63, 0xf0, 0x4e, 0x14, 0x93, 0x4a,
	0xaf, 0xf7, 0x4e, 0x80, 0x22, 0x87, 0xb6, 0x80, 0xae, 0xb6, 0x4a, 0x75, 0xce, 0xd5, 0xb9, 0x3a,
	0x0c, 0xdf, 0xc3, 0x5d, 0xeb, 0xc8, 0x5c, 0x24, 0x7f, 0x36, 0xa4, 0x99, 0x45, 0x65, 0x49, 0x33,
	0x8b, 0x61, 0x53, 0xbe, 0xa1, 0xca, 0x6d, 0x63, 0xca, 0xe6, 0xe1, 0xff, 0xb9, 0x27, 0x5f, 0x11,
	0xec, 0xd7, 0x8d, 0xdf, 0x9c, 0xae, 0x8b, 0x6a, 0x92, 0x85, 0x90, 0xea, 0x9f, 0x5f, 0x13, 0xfa,
	0xbb, 0xd7, 0xf4, 0x71, 0x57, 0xff, 0x44, 0x66, 0x3f, 0x03, 0x00, 0x00, 0xff, 0xff, 0x70, 0x67,
	0x96, 0x73, 0x55, 0x06, 0x00, 0x00,
}
