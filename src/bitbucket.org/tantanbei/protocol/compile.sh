#!/usr/bin/env bash

set -e

GOPATH_DIR=$(echo $(go env GOPATH) | awk -F':' '{ print $1}')

function init() {
    if [[ $(uname -s) == "Darwin"* ]]; then
        download_file=protoc-3.5.1-osx-x86_64.zip
    elif [[ $(uname -s) == "Linux"* ]]; then
        download_file=protoc-3.5.1-linux-x86_64.zip
    else
        echo "你的系统不是mac也不是linux，本脚本不支持"
        exit
    fi
    wget -O protoc.zip https://github.com/protocolbuffers/protobuf/releases/download/v3.5.1/${download_file}
    unzip -j protoc.zip bin/protoc && rm protoc.zip
    mv protoc ${GOPATH_DIR}/bin/

    rm -rf ${GOPATH_DIR}/src/github.com/golang/protobuf
    go get -v -d -u github.com/golang/protobuf/protoc-gen-go
    git -C ${GOPATH_DIR}/src/github.com/golang/protobuf checkout "v1.1.0"
    go install github.com/golang/protobuf/protoc-gen-go
}

if [[ "$1" == "init" ]]; then
    init
else
    PATH=$PATH:${GOPATH_DIR}/bin protoc --go_out=plugins=grpc,paths=source_relative:. --python_out=plugins=grpc:. $1
fi
